<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;



class CreateTableEmpresas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas_nc', function (Blueprint $table) {
            $table->bigIncrements('id_rgt')->unsigned();
            $table->integer('id_empresa')->unsigned();
            $table->string('razon_social',100)->nullable(false);
            $table->string('razon_comercial',100);
            $table->string('ruc',13)->nullable(false);
            $table->string('correo',100);
            $table->string('direccion',100);
            $table->integer('iva');
            $table->char('estado',2)->default('A');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('empresas_nc', function (Blueprint $table) {
        //     //
        // });
    }
}
