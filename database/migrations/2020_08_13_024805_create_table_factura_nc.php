<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableFacturaNc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura_nc', function (Blueprint $table) {
            $table->bigIncrements('id_fact')->unsigned();
            $table->unsignedBigInteger('id_empresa')->unsigned();//
            $table->string('tipo_doc',10)->nullable(false);
            $table->unsignedBigInteger('id_rgt_conf')->unsigned();//
            $table->integer('id_establecimiento')->unsigned();
            $table->string('id_punto_emsion',8);
            $table->date('fecha_emision');
            $table->integer('secuencia')->unsigned();
            $table->unsignedBigInteger('id_cliente')->unsigned();//
            $table->string('comentario',255);
            $table->string('forma_de_pago',255);
            $table->decimal('subtotal', 8, 2);
            $table->decimal('iva', 8, 2);
            $table->decimal('descuento', 8, 2);
            $table->decimal('total', 8, 2);

            $table->foreign('id_empresa')->references('id_rgt')->on('empresas_nc');
            $table->foreign('id_rgt_conf')->references('id_rgt')->on('configuracion_nc');
            $table->foreign('id_cliente')->references('id_cliente')->on('clientes_nc');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('factura_nc');
        // create::table('factura_nc', function (Blueprint $table) {
        //     //
        // });
    }
}
