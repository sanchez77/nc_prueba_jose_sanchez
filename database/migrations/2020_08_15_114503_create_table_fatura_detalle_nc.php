<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableFaturaDetalleNc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('factura_detalle_nc', function (Blueprint $table) {
            $table->bigIncrements('id_det_fact')->unsigned();
            $table->integer('id_empresa')->unsigned();
            $table->unsignedBigInteger('id_product')->unsigned(); //
            $table->string('cod_prod', 255)->nullable(false);
            $table->integer('cantidad')->unsigned();
            $table->decimal('precio_unitario', 10, 2);
            $table->decimal('subtotal_prod', 10, 2);
            $table->decimal('desct_prod_porcent', 10, 2);
            $table->decimal('desct_prod_valor', 10, 2);
            $table->decimal('total_prod', 10, 2);
            $table->string('tipo_doc', 10)->nullable(false);
            $table->date('fecha_emision');
            $table->unsignedBigInteger('id_fact')->unsigned(); //
            $table->integer('secuencia')->unsigned();
            $table->integer('id_establecimiento')->unsigned();
            $table->string('id_punto_emsion', 8);

            $table->foreign('id_fact')->references('id_fact')->on('factura_nc');
            $table->foreign('id_product')->references('id_product')->on('productos_nc');


            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('factura_detalle_nc');
        // Schema::table('factura_detalle_nc', function (Blueprint $table) {
        //     //
        // });
    }
}
