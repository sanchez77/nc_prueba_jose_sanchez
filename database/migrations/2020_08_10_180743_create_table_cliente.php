<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes_nc', function (Blueprint $table) {
            $table->bigIncrements('id_cliente')->unsigned();
            $table->string('id_tip_doc',5)->nullable(false);
            $table->string('num_doc',50)->nullable(false);
            $table->string('nombres',255)->nullable(false);
            $table->string('apellidos',255)->nullable(false);
            $table->string('razon_comercial',100);
            $table->string('correo',100);
            $table->date('fecha_nacimiento');
            $table->integer('edad')->unsigned();
            $table->string('telefono',15);
            $table->string('celular',15);
            $table->string('direccion',100);
            $table->char('estado',2)->default('A');
            $table->integer('id_empresa')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::create('cliente_nc', function (Blueprint $table) {

        // });
    }
}
