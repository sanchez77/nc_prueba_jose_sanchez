<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableConfiguracionNc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuracion_nc', function (Blueprint $table) {
            $table->bigIncrements('id_rgt')->unsigned();
            $table->integer('id_empresa')->unsigned();
            $table->integer('id_establecimiento')->unsigned();
            $table->string('d_establecimiento',100)->nullable(false);
            $table->string('id_punto_emsion',5);
            $table->string('d_punto_emsion',100)->nullable(false);
            $table->integer('secuencia')->unsigned();
            $table->char('estado',2)->default('A');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::create('configuracion_nc', function (Blueprint $table) {
        //     //
        // });
    }
}
