<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableProductosNc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos_nc', function (Blueprint $table) {
            $table->bigIncrements('id_product')->unsigned();
            $table->string('cod_prod',255)->nullable(false);
            $table->string('descripcion',255)->nullable(false);
            $table->integer('stock')->unsigned();
            $table->decimal('precio_unitario', 8, 2);
            $table->integer('id_empresa')->unsigned();
            $table->char('estado',2)->default('A');


            //$table->foreignId('id_empresa'))->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::create('productos_nc', function (Blueprint $table) {
        //     //
        // });
    }
}
