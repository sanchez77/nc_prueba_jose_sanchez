<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Cliente;
use Faker\Generator as Faker;

$factory->define(Cliente::class, function (Faker $faker) {
    return [
        'id_tip_doc' =>$faker->randomElement($array = array ('04','05','06')),
        'num_doc' => $faker->isbn10,
        'nombres' => $faker->name,
        'apellidos' => $faker->name,
        'razon_comercial' => $faker->company,
        'correo' => $faker->freeEmail,
        'fecha_nacimiento' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'edad' => $faker->numberBetween($min = 10, $max = 80),
        'telefono' => $faker->tollFreePhoneNumber,
        'celular' => $faker->tollFreePhoneNumber,
        'direccion' => $faker->address,
        'estado' => $faker->randomElement($array = array ('A','I')),
        'id_empresa' => 1
    ];
});
