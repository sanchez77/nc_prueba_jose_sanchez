/*
Navicat MySQL Data Transfer

Source Server         : laragonE
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : nc_josesanchez

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2020-08-15 19:51:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for clientes_nc
-- ----------------------------
DROP TABLE IF EXISTS `clientes_nc`;
CREATE TABLE `clientes_nc` (
  `id_cliente` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_tip_doc` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_doc` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombres` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `razon_comercial` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `edad` int(10) unsigned NOT NULL,
  `telefono` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` char(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `id_empresa` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of clientes_nc
-- ----------------------------
INSERT INTO `clientes_nc` VALUES ('1', '05', '0931147284', 'jose', 'sanchez', 'PRUEBAS COMERCIAL', 'sanchezbaquejoseluis@gmail.com', '2020-08-10', '23', '0981767327', '0520527272', 'Bastion Popular BL-1A Mz- 483 SL-37 , AV.38E N-O y 9NO PSE. 23A, 9NO PSE. 23A N-O\n9NO PSE. 23A N-O', 'A', '0', '2020-08-10 20:02:08', '2020-08-10 20:02:08');
INSERT INTO `clientes_nc` VALUES ('2', '05', '0936589452', 'jose', 'sanchez', 'PRUEBAS COMERCIAL', 'sanchezbaquejoseluis@gmail.com', '2020-08-10', '25', '0981767327', '0520527272', 'Bastion Popular BL-1A Mz- 483 SL-37 , AV.38E N-O y 9NO PSE. 23A, 9NO PSE. 23A N-O\n9NO PSE. 23A N-O1', 'A', '0', '2020-08-10 20:03:18', '2020-08-10 20:03:18');
INSERT INTO `clientes_nc` VALUES ('5', '05', '0986325678', 'ANDRES', 'SALCEDO', 'FARMACIA', 'andres@gmail.com', '2020-08-10', '12', '025689656', '0986523586', 'norte de gye', 'A', '0', '2020-08-10 20:25:04', '2020-08-10 20:25:04');
INSERT INTO `clientes_nc` VALUES ('6', '05', '0963589745', 'karla', 'veliz', 'karla veliz', 'karla@gmail.com', '1996-05-01', '23', '02563589', '09856321456', 'mucho lotes', 'A', '0', '2020-08-10 20:25:52', '2020-08-10 20:25:52');
INSERT INTO `clientes_nc` VALUES ('7', '05', '0931147589', 'ana', 'choez', 'NULL', 'NULL', '2000-02-17', '12', '0933434', '0687783', 'miñago', 'I', '0', '2020-08-10 20:30:31', '2020-08-10 20:30:31');

-- ----------------------------
-- Table structure for configuracion_nc
-- ----------------------------
DROP TABLE IF EXISTS `configuracion_nc`;
CREATE TABLE `configuracion_nc` (
  `id_rgt` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) unsigned NOT NULL,
  `id_establecimiento` int(10) unsigned NOT NULL,
  `d_establecimiento` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_punto_emsion` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `d_punto_emsion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secuencia` int(10) unsigned NOT NULL,
  `estado` char(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_rgt`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of configuracion_nc
-- ----------------------------
INSERT INTO `configuracion_nc` VALUES ('7', '1', '1', 'PRINCIPAL', '001', 'CENTRO', '5', 'A', '2020-08-10 09:35:27', '2020-08-13 04:38:32');
INSERT INTO `configuracion_nc` VALUES ('8', '1', '2', 'MATRIZ', '001', 'NORTE', '6', 'A', '2020-08-15 19:44:24', '2020-08-15 19:44:24');

-- ----------------------------
-- Table structure for empresas_nc
-- ----------------------------
DROP TABLE IF EXISTS `empresas_nc`;
CREATE TABLE `empresas_nc` (
  `id_rgt` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) unsigned NOT NULL,
  `razon_social` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `razon_comercial` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruc` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iva` int(11) NOT NULL,
  `estado` char(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_rgt`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of empresas_nc
-- ----------------------------
INSERT INTO `empresas_nc` VALUES ('1', '1', 'REBBOT', 'Juan carpio', '0935568924001', 'juancarpio1@gmail.com', 'urdesa central 105', '12', 'A', '2020-08-13 03:57:28', '2020-08-13 03:57:28');
INSERT INTO `empresas_nc` VALUES ('2', '2', 'ARTESANO LAS', 'ANDY ANDINO', '0965886321001', 'artasano@gmail.com', '10 agosto 115 guayaquil', '12', 'A', '2020-08-13 04:00:46', '2020-08-13 04:00:46');
INSERT INTO `empresas_nc` VALUES ('3', '3', 'heladria marco', 'marcos tapia guaman', '0931147285001', 'marcohelado@hotmail.com', 'florida norte', '12', 'I', '2020-08-13 04:31:36', '2020-08-13 05:38:11');

-- ----------------------------
-- Table structure for factura_detalle_nc
-- ----------------------------
DROP TABLE IF EXISTS `factura_detalle_nc`;
CREATE TABLE `factura_detalle_nc` (
  `id_det_fact` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) unsigned NOT NULL,
  `id_product` bigint(20) unsigned NOT NULL,
  `cod_prod` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cantidad` int(10) unsigned NOT NULL,
  `precio_unitario` decimal(10,2) NOT NULL,
  `subtotal_prod` decimal(10,2) NOT NULL,
  `desct_prod_porcent` decimal(10,2) NOT NULL,
  `desct_prod_valor` decimal(10,2) NOT NULL,
  `total_prod` decimal(10,2) NOT NULL,
  `tipo_doc` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_emision` date NOT NULL,
  `id_fact` bigint(20) unsigned NOT NULL,
  `secuencia` int(10) unsigned NOT NULL,
  `id_establecimiento` int(10) unsigned NOT NULL,
  `id_punto_emsion` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_det_fact`),
  KEY `factura_detalle_nc_id_fact_foreign` (`id_fact`),
  KEY `factura_detalle_nc_id_product_foreign` (`id_product`),
  CONSTRAINT `factura_detalle_nc_id_fact_foreign` FOREIGN KEY (`id_fact`) REFERENCES `factura_nc` (`id_fact`),
  CONSTRAINT `factura_detalle_nc_id_product_foreign` FOREIGN KEY (`id_product`) REFERENCES `productos_nc` (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of factura_detalle_nc
-- ----------------------------
INSERT INTO `factura_detalle_nc` VALUES ('1', '1', '2', 'tell002', '3', '100.00', '300.00', '10.00', '120.00', '250.00', 'FACT', '2020-08-15', '1', '4', '1', '001', '2020-08-15 19:48:57', '2020-08-15 19:48:57');
INSERT INTO `factura_detalle_nc` VALUES ('2', '1', '1', 'cell001', '2', '152.36', '456.00', '3.00', '23.00', '2563.00', 'FACT', '2020-08-15', '1', '4', '1', '001', '2020-08-15 19:48:57', '2020-08-15 19:48:57');

-- ----------------------------
-- Table structure for factura_nc
-- ----------------------------
DROP TABLE IF EXISTS `factura_nc`;
CREATE TABLE `factura_nc` (
  `id_fact` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_empresa` bigint(20) unsigned NOT NULL,
  `tipo_doc` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_rgt_conf` bigint(20) unsigned NOT NULL,
  `id_establecimiento` int(10) unsigned NOT NULL,
  `id_punto_emsion` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_emision` date NOT NULL,
  `secuencia` int(10) unsigned NOT NULL,
  `id_cliente` bigint(20) unsigned NOT NULL,
  `comentario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `forma_de_pago` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtotal` decimal(8,2) NOT NULL,
  `iva` decimal(8,2) NOT NULL,
  `descuento` decimal(8,2) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_fact`),
  KEY `factura_nc_id_empresa_foreign` (`id_empresa`),
  KEY `factura_nc_id_rgt_conf_foreign` (`id_rgt_conf`),
  KEY `factura_nc_id_cliente_foreign` (`id_cliente`),
  CONSTRAINT `factura_nc_id_cliente_foreign` FOREIGN KEY (`id_cliente`) REFERENCES `clientes_nc` (`id_cliente`),
  CONSTRAINT `factura_nc_id_empresa_foreign` FOREIGN KEY (`id_empresa`) REFERENCES `empresas_nc` (`id_rgt`),
  CONSTRAINT `factura_nc_id_rgt_conf_foreign` FOREIGN KEY (`id_rgt_conf`) REFERENCES `configuracion_nc` (`id_rgt`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of factura_nc
-- ----------------------------
INSERT INTO `factura_nc` VALUES ('1', '1', 'FACT', '7', '1', '001', '2020-08-15', '4', '1', 'preubas de fact', 'SIN UTILIZACIÓN DEL SISTEMA FINANCIERO', '0.00', '0.00', '0.00', '0.00', '2020-08-15 19:48:57', '2020-08-15 19:48:57');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2019_08_19_000000_create_failed_jobs_table', '1');
INSERT INTO `migrations` VALUES ('2', '2020_08_08_202943_create_table_empresas', '1');
INSERT INTO `migrations` VALUES ('3', '2020_08_09_024913_create_table_configuracion_nc', '1');
INSERT INTO `migrations` VALUES ('4', '2020_08_10_180743_create_table_cliente', '1');
INSERT INTO `migrations` VALUES ('5', '2020_08_11_150937_create_table_productos_nc', '1');
INSERT INTO `migrations` VALUES ('6', '2020_08_13_024805_create_table_factura_nc', '1');
INSERT INTO `migrations` VALUES ('7', '2020_08_15_114503_create_table_fatura_detalle_nc', '1');

-- ----------------------------
-- Table structure for productos_nc
-- ----------------------------
DROP TABLE IF EXISTS `productos_nc`;
CREATE TABLE `productos_nc` (
  `id_product` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cod_prod` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock` int(10) unsigned NOT NULL,
  `precio_unitario` decimal(8,2) NOT NULL,
  `id_empresa` int(10) unsigned NOT NULL,
  `estado` char(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of productos_nc
-- ----------------------------
INSERT INTO `productos_nc` VALUES ('1', 'cell001', 'alcatel x100', '12', '152.36', '0', 'A', '2020-08-11 18:04:29', '2020-08-13 09:22:26');
INSERT INTO `productos_nc` VALUES ('2', 'tell002', 'prueba', '25', '100.00', '0', 'A', '2020-08-11 18:11:33', '2020-08-11 18:41:23');
INSERT INTO `productos_nc` VALUES ('3', 'papel003', 'hojas papel ministro', '56', '100.00', '0', 'I', '2020-08-13 09:01:45', '2020-08-13 09:23:17');
INSERT INTO `productos_nc` VALUES ('4', 'mes001', 'mesa de madera pequeña', '25', '256.00', '0', 'A', '2020-08-15 19:47:08', '2020-08-15 19:47:08');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'Administrador');
INSERT INTO `roles` VALUES ('2', 'Usuario Estandar');

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `roles_id` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('1', 'jose', 'jose@gmail.com', '$2y$10$U34Ol9G2g.kinwrmfAWvZOnv5vj3H2kmVKqo/PmQMVe3YgmlbYS4q', null, '1', '2020-08-08 11:06:11', '2020-08-08 11:06:11');

-- ----------------------------
-- Procedure structure for sp_con_cliente_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_con_cliente_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_con_cliente_nc`(IN p_in_opcion text, 
IN p_in_num_doc varchar(50))
BEGIN
case p_in_opcion
	 when 'AA' THEN  
			SELECT 
				 id_cliente ,
				id_tip_doc ,
				num_doc ,
				nombres ,
				apellidos ,
				razon_comercial, 
				correo ,
				fecha_nacimiento , 
				edad ,
				telefono ,
				celular,
				direccion ,
				estado 
			FROM clientes_nc;
		
	 when 'AB' THEN  
		SELECT 
			 id_cliente ,
			id_tip_doc ,
			num_doc ,
			nombres ,
			apellidos ,
			razon_comercial, 
			correo ,
			fecha_nacimiento , 
			edad ,
			telefono ,
			celular,
			direccion ,
			estado 
		FROM clientes_nc
        where (num_doc LIKE concat('%',p_in_num_doc,'%')) 
        or (nombres LIKE concat('%',p_in_num_doc,'%')) 
        or (apellidos LIKE concat('%',p_in_num_doc,'%')) ;
       
        #'%'+p_in_num_doc+'%';
		#ORDER BY id_rgt DESC;
		#DESCRIBE EMPRESAS_NC
	    
	end case;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_con_cliente_nc_busq
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_con_cliente_nc_busq`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_con_cliente_nc_busq`(IN p_in_opcion text, IN p_in_dato_busqueda varchar(50))
BEGIN
case p_in_opcion
	 when 'AC' THEN  
		SELECT 
				 id_cliente ,
				id_tip_doc ,
				num_doc ,
				nombres ,
				apellidos ,
				razon_comercial, 
				correo ,
				fecha_nacimiento , 
				edad ,
				telefono ,
				celular,
				direccion ,
				estado 
			FROM clientes_nc
            where estado = 'A'
		ORDER BY id_cliente DESC;

        
         when 'AD' THEN  
				SELECT 
					 id_cliente ,
					id_tip_doc ,
					num_doc ,
					nombres ,
					apellidos ,
					razon_comercial, 
					correo ,
					fecha_nacimiento , 
					edad ,
					telefono ,
					celular,
					direccion ,
					estado 
				FROM clientes_nc
				where estado = 'A' and (num_doc LIKE concat('%',p_in_dato_busqueda,'%')) 
				or (nombres LIKE concat('%',p_in_dato_busqueda,'%')) 
				or (apellidos LIKE concat('%',p_in_dato_busqueda,'%'))
                ORDER BY id_cliente DESC;
   
	end case;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_con_configuracion_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_con_configuracion_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_con_configuracion_nc`(IN p_in_opcion VARCHAR(2), IN p_in_id_empresa INT)
BEGIN
IF p_in_opcion = 'AA' THEN
	SELECT id_rgt, id_empresa,id_establecimiento,d_establecimiento,id_punto_emsion,d_punto_emsion,secuencia,estado 
	FROM configuracion_nc ORDER BY id_rgt DESC;
	##WHERE p_in_id_empresa = id_empresa;

end if;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_con_empresas_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_con_empresas_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_con_empresas_nc`(IN p_in_opcion text, IN p_id_empresa varchar(255))
BEGIN
case p_in_opcion
	 when 'AA' THEN  
		SELECT 
			id_rgt,
			id_empresa,
			razon_social,
			razon_comercial,
			ruc,
			correo,
			direccion,
			iva,
			estado
		FROM empresas_nc
		ORDER BY id_rgt DESC;
		#DESCRIBE EMPRESAS_NC
        
         when 'AB' THEN  
		SELECT 
			id_rgt,
			id_empresa,
			razon_social,
			razon_comercial,
			ruc,
			correo,
			direccion,
			iva,
			estado
		FROM empresas_nc
        where ((ruc LIKE concat('%',p_id_empresa,'%'))
        or (razon_social LIKE concat('%',p_id_empresa,'%'))
        or (razon_comercial LIKE concat('%',p_id_empresa,'%')))
        AND (estado = 'A')
		ORDER BY id_rgt DESC;
        
        
         when 'AC' THEN  
		SELECT 
			id_rgt,
			id_empresa,
			razon_social,
			razon_comercial,
			ruc,
			correo,
			direccion,
			iva,
			estado
		FROM empresas_nc
        Where estado = 'A'
		ORDER BY id_rgt DESC;
		#DESCRIBE EMPRESAS_NC
        
	   
	end case;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_con_productos_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_con_productos_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_con_productos_nc`(
IN p_in_opcion text, 
IN p_in_cod_prod varchar(50))
BEGIN
case p_in_opcion
 when 'AA' THEN  
		SELECT 
			id_product ,
			cod_prod ,
			descripcion ,
			stock,
			precio_unitario,
			id_empresa ,
			estado
		FROM productos_nc;
	
	 when 'AB' THEN  
		SELECT 
			id_product ,
			cod_prod ,
			descripcion ,
			stock,
			precio_unitario,
			id_empresa ,
			estado
		FROM productos_nc
        where (cod_prod LIKE concat('%',p_in_cod_prod,'%'));
       
        #'%'+p_in_num_doc+'%';
		#ORDER BY id_rgt DESC;
		#DESCRIBE EMPRESAS_NC
	   when 'AC' THEN  
		SELECT 
			id_product ,
			cod_prod ,
			descripcion ,
			stock,
			precio_unitario,
			id_empresa ,
			estado
		FROM productos_nc
        where estado = 'A' and stock > 0;
	
	end case;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_con_puntos_emision_establecimineto_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_con_puntos_emision_establecimineto_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_con_puntos_emision_establecimineto_nc`(
IN p_in_opcion text, 
IN p_in_id_empresa int,
in p_in_otro varchar(50))
BEGIN
case p_in_opcion
 when 'AA' THEN  
	SELECT id_rgt, id_empresa,id_establecimiento,d_establecimiento,id_punto_emsion,d_punto_emsion,secuencia,estado 
	FROM configuracion_nc where id_empresa = p_in_id_empresa;
end case;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trx_cliente_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trx_cliente_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trx_cliente_nc`(
						IN opcion text,
						IN p_in_id_cliente int,                        
						IN p_in_id_tip_doc VARCHAR(5),
						IN p_in_num_doc VARCHAR(50),
						IN p_in_nombres VARCHAR(255),
						IN p_in_apellidos VARCHAR(255),
						IN p_in_razon_comercial VARCHAR(100),
						IN p_in_correo VARCHAR(250),
						IN p_in_fecha_nacimiento VARCHAR(250),
						IN p_in_edad int,
						IN p_in_telefono VARCHAR(15),
						IN p_in_celular VARCHAR(15),
						IN p_in_direccion VARCHAR(100),
						IN p_in_estado char(10),
                        IN p_in_id_empresa int,
						OUT out_cod INT,
                        OUT out_msj VARCHAR(500),
						OUT out_id_cliente INT,
                        OUT out_num_doc VARCHAR(500))
BEGIN
declare _v_cod_cliente int;
case opcion  
  when 'AA' then 
	set _v_cod_cliente = (SELECT count(*) FROM clientes_nc where  num_doc = p_in_num_doc );
	IF _v_cod_cliente >= 1 THEN
			SET out_cod = 6;
			SET out_num_doc = p_in_num_doc;
			SET out_msj = 'YA EXISTE UN CLIENTE CON ESE NUMERO DE DOCUMENTO. POR FAVOR REVISE!';
			SET  out_id_cliente = (SELECT id_cliente FROM clientes_nc where  num_doc = p_in_num_doc);
SELECT out_id_cliente AS id_cliente, out_cod, out_msj, out_num_doc;
	ELSE 
		INSERT INTO clientes_nc(
								id_tip_doc, 
								num_doc,
								nombres,
								apellidos ,
								razon_comercial,
								correo,
								fecha_nacimiento, 
								edad,
								telefono,
								celular, 
								direccion, 
								estado,
								id_empresa, 
								created_at, 
								updated_at)
		VALUES(		       
							p_in_id_tip_doc,
							p_in_num_doc,
							p_in_nombres,
							p_in_apellidos,
							p_in_razon_comercial,
							p_in_correo,
							p_in_fecha_nacimiento,
							p_in_edad,
							p_in_telefono,
							p_in_celular,
							p_in_direccion,
							p_in_estado,
							p_in_id_empresa,
							now(),
							now());
							
				SET out_cod = 7;
				SET out_num_doc = p_in_num_doc;
				SET out_msj = 'SE HA CREADO CLIENTE CORRECTAMENTE';
				SELECT 
    LAST_INSERT_ID() AS id_cliente,
    out_cod,
    out_msj,
    out_num_doc;
END IF;
when 'AB' then
UPDATE clientes_nc set 
		id_tip_doc =	p_in_id_tip_doc,
		nombres =	p_in_nombres,
		apellidos =	p_in_apellidos,
		razon_comercial =	p_in_razon_comercial,
		correo =	p_in_correo,
		fecha_nacimiento =	p_in_fecha_nacimiento,
		edad =	p_in_edad,
		telefono =	p_in_telefono,
		celular =	p_in_celular,
		direccion =	p_in_direccion,
		estado =	p_in_estado
	where id_cliente = p_in_id_cliente and num_doc =	p_in_num_doc;
	 SET out_cod = 7;
				SET out_num_doc = p_in_num_doc;
				SET out_msj = 'SE HA MODIFICADO EL CLIENTE CORRECTAMENTE';
				SELECT 
    p_in_id_cliente as id_cliente,
    out_cod,
    out_msj,
    out_num_doc;
  
ELSE BEGIN END;  
end case;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trx_configuracion_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trx_configuracion_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trx_configuracion_nc`(
						IN opcion text,
	IN p_in_id_rgt int,
	IN p_id_empresa int,
	IN p_id_establecimiento int,
	IN p_d_establecimiento VARCHAR(100),
	IN p_id_punto_emsion VARCHAR(5),
	IN p_d_punto_emsion VARCHAR(100),
	IN p_in_secuencia int,
	IN p_estado char(10),
	OUT out_cod INT,
	OUT out_id_rgt INT,
	OUT out_msj VARCHAR(500),
	OUT out_id_establecimiento INT,
	OUT out_id_punto_emsion VARCHAR(50))
BEGIN
declare _v_rgt int;
case opcion  
  when 'AA' then 
	set _v_rgt = (SELECT count(*) FROM configuracion_nc where id_empresa = p_id_empresa and  id_establecimiento = p_id_establecimiento and  id_punto_emsion = p_id_punto_emsion);
	IF _v_rgt >= 1 THEN
			SET out_cod = 6;
			SET out_id_rgt = p_in_id_rgt;
			SET out_msj = 'YA EXISTE UN ESTABLECIMIENTO O PUNTO DE EMISION CON ESA NUMERACION. POR FAVOR REVISE!';
			SET  out_id_establecimiento = p_id_establecimiento;
			SET  out_id_punto_emsion = p_id_punto_emsion;
			SELECT 
    out_id_rgt AS ID_REG,
    out_cod,
    out_msj,
    out_id_establecimiento,
    out_id_punto_emsion;
		ELSE
			set _v_rgt = 0;
		END IF;
	
IF _v_rgt = 0 then

	
	INSERT INTO configuracion_nc(id_empresa,
												id_establecimiento,
												d_establecimiento,
												id_punto_emsion,
												d_punto_emsion,
												secuencia,
												estado,
												created_at,updated_at)
	VALUES(									p_id_empresa,
													p_id_establecimiento,
													p_d_establecimiento,
													p_id_punto_emsion,
													p_d_punto_emsion,
													p_in_secuencia,
													p_estado,
													now(),
													now());
	SET out_cod = 7;
	SET out_msj = 'SE HA CREADO LA CONFIGURACION DE EMPRESA CORRECTAMENTE';
	SET  out_id_establecimiento = p_id_establecimiento;
	SET  out_id_punto_emsion = p_id_punto_emsion;
	SELECT 
    LAST_INSERT_ID() AS ID_REG,
    out_cod,
    out_msj,
    out_id_establecimiento,
    out_id_punto_emsion;

END IF;



  when 'AB' then 
	UPDATE configuracion_nc set 
		secuencia= p_in_secuencia,
		estado = p_estado,
		updated_at = now()
	where id_rgt = p_in_id_rgt and id_empresa = p_id_empresa;
	
    SET out_cod = 7;
	SET out_msj = 'SE HA MODIFICADO LA CONFIGURACION DE EMPRESA CORRECTAMENTE';
	SET  out_id_establecimiento = p_id_establecimiento;
	SET  out_id_punto_emsion = p_id_punto_emsion;
SELECT 
    p_in_id_rgt AS ID_REG,
    out_cod,
    out_msj,
    out_id_establecimiento,
    out_id_punto_emsion;
ELSE BEGIN END;  
end case;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trx_empresas_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trx_empresas_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trx_empresas_nc`(
	IN `opcion` VARCHAR(2),
	IN `p_in_id_rgt` int,
	IN `p_in_id_empresa` int,
	IN `p_in_razon_social` VARCHAR(100),
	IN `p_in_razon_comercial` VARCHAR(100),
	IN `p_in_ruc` VARCHAR(13),
	IN `p_in_correo` VARCHAR(100),
	IN `p_in_direccion` VARCHAR(100),
	IN `p_in_iva` int,
	IN `p_in_estado` char(2)
,
	OUT `out_cod` INT,
	OUT `out_id_reg` VARCHAR(50)
)
BEGIN
declare v_id_reg varchar(20) ;
declare v_rgt int;
declare v_regt int;
declare _count_emp int;
declare _id_emp int;

IF opcion = 'AB' THEN
UPDATE empresas_nc set 

												correo =p_in_correo,
												direccion = p_in_direccion,
												iva = p_in_iva,
												estado = p_in_estado,
												updated_at = now()							
	where id_rgt=p_in_id_rgt and id_empresa = p_in_id_empresa;
	
	SET out_cod = 7;
	SET out_id_reg = 'SE HA MOFICADO LA EMPRESA CORRECTAMENTE';
	
	SELECT p_in_id_rgt AS ID_REG, out_cod, out_id_reg, p_in_ruc;
	

END IF;

set v_rgt = (SELECT count(*) FROM empresas_nc where id_empresa = p_in_id_empresa and ruc = p_in_ruc);

IF v_rgt >= 1 THEN
	SET out_cod = 6;
	SET out_id_reg = 'RUC DE EMPRESA YA EXITE';
	SET  v_regt = (SELECT id_rgt FROM empresas_nc where id_empresa = p_in_id_empresa and ruc = p_in_ruc);
	
	SELECT v_regt AS ID_REG, out_cod, out_id_reg, p_in_ruc;

	
ELSE
	set v_rgt = 0;
END IF;

IF opcion = 'AA' AND v_rgt = 0 then
	
    set _count_emp = ( SELECT count(*) FROM empresas_nc ) ;
    IF( _count_emp = 0) then
		set _id_emp = 1;
    else
		set _id_emp = (SELECT (MAX(id_empresa)+1) as id_empresa FROM empresas_nc order by id_rgt desc limit 1);
	END IF;
	INSERT INTO empresas_nc(id_empresa,
												razon_social,
												razon_comercial,
												ruc,
												correo,
												direccion,
												iva,
												estado,
												created_at,updated_at)
	VALUES(									_id_emp,#p_in_id_empresa,
													p_in_razon_social,
													p_in_razon_comercial,
													p_in_ruc,
													p_in_correo,
													p_in_direccion,
													p_in_iva,
													p_in_estado,
													now(),
													now());
													
													
													
	
	
	
	SET out_cod = 7;
	SET out_id_reg = 'SE HA CREADO LA EMPRESA CORRECTAMENTE';
	
	SELECT LAST_INSERT_ID() AS ID_REG, out_cod, out_id_reg, p_in_ruc;
	

ELSE
	SELECT LAST_INSERT_ID() AS ID_REG;
END IF;



END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trx_factura_detalle_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trx_factura_detalle_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trx_factura_detalle_nc`(
						IN opcion text,
                        IN p_in_id_det_fact int,
						IN p_in_id_empresa int,
						IN p_in_id_product int,
						IN p_in_cod_prod  varchar(255),
						IN p_in_cantidad int,
						IN p_in_precio_unitario decimal(10,2) ,
						IN p_in_subtotal_prod decimal(10,2) ,
						IN p_in_desct_prod_porcent decimal(10,2) ,
						IN p_in_desct_prod_valor decimal(10,2) ,
						IN p_in_total_prod decimal(10,2) ,
						IN p_in_tipo_doc varchar(10) ,
						IN p_in_fecha_emision  varchar(50) ,
						IN p_in_id_fact int,
                        IN p_in_id_secuencia int,
						IN p_in_id_establecimiento int,
						IN p_in_id_punto_emsion varchar(8),
                        
						OUT out_cod INT,
                        OUT out_msj VARCHAR(500),
						OUT out_id_fact INT,
                        OUT out_secuencia INT)
BEGIN

case opcion  
  when 'AA' then 
		INSERT INTO factura_detalle_nc(
								id_empresa ,
								id_product,
								cod_prod,
								cantidad,
								precio_unitario,
								subtotal_prod,
								desct_prod_porcent,
								desct_prod_valor,
								total_prod,
								tipo_doc,
								fecha_emision,
								id_fact,
                                secuencia,
								id_establecimiento,
								id_punto_emsion,
								created_at, 
								updated_at)
		VALUES(		  	
						p_in_id_empresa,
						p_in_id_product,
						p_in_cod_prod,
						p_in_cantidad,
						p_in_precio_unitario,
						p_in_subtotal_prod,
						p_in_desct_prod_porcent,
						p_in_desct_prod_valor,
						p_in_total_prod,
						p_in_tipo_doc,
						p_in_fecha_emision,
						p_in_id_fact,
                        p_in_id_secuencia,
						p_in_id_establecimiento,
						p_in_id_punto_emsion,
						now(),
						now());
							
				SET out_cod = 7;
				SET out_secuencia = p_in_id_secuencia;
				SET out_msj = 'SE HA CREADO DET FACT CORRECTAMENTE';
				SELECT p_in_id_fact AS out_id_fact, out_cod, out_msj, out_secuencia;

ELSE BEGIN END;  
end case;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trx_factura_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trx_factura_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trx_factura_nc`(
						IN opcion text,
                        IN p_in_id_fact int,
						IN p_in_id_empresa int,
						IN p_in_tipo_doc varchar(10) ,
						IN p_in_id_rgt_conf int,
						IN p_in_id_establecimiento int,
						IN p_in_id_punto_emsion varchar(8) ,
						IN p_in_fecha_emision  varchar(50) ,
						IN p_in_secuencia int,
						IN p_in_id_cliente int,
						IN p_in_comentario varchar(255) ,
						IN p_in_forma_de_pago varchar(255) ,
						IN p_in_subtotal decimal(8,2) ,
						IN p_in_iva decimal(8,2) ,
						IN p_in_descuento decimal(8,2) ,
						IN p_in_total decimal(8,2),
                   
						OUT out_cod INT,
                        OUT out_msj VARCHAR(500),
						OUT out_id_fact INT,
                        OUT out_secuencia INT)
BEGIN
declare _v_secuencia int;
declare _v_sec_auto int;
case opcion  
  when 'AA' then 
	set _v_secuencia = (SELECT count(*) FROM configuracion_nc where id_empresa = p_in_id_empresa and id_establecimiento = p_in_id_establecimiento and  id_punto_emsion = p_in_id_punto_emsion);
	IF _v_secuencia <= 0 THEN
			SET out_cod = 6;
			SET out_secuencia = out_secuencia;
			SET out_msj = 'NO EXISTE SECUENCIA. POR FAVOR REVISE!';
			SET  out_secuencia = out_secuencia;
SELECT 
    LAST_INSERT_ID() AS out_id_fact,
    out_cod,
    out_msj,
    out_secuencia;
	ELSE 
		set _v_sec_auto = (SELECT (MAX(secuencia)+1)
        FROM configuracion_nc 
        where id_empresa = p_in_id_empresa 
        and id_establecimiento = p_in_id_establecimiento 
        and  id_punto_emsion = p_in_id_punto_emsion);
        
        UPDATE configuracion_nc
        set secuencia = _v_sec_auto
        where id_empresa = p_in_id_empresa 
        and id_establecimiento = p_in_id_establecimiento 
        and  id_punto_emsion = p_in_id_punto_emsion;
    
    
		INSERT INTO factura_nc(
								id_empresa ,
								tipo_doc,
								id_rgt_conf,
								id_establecimiento,
								id_punto_emsion,
								fecha_emision ,
								secuencia,
								id_cliente,
								comentario ,
								forma_de_pago ,
								subtotal, 
								iva,
								descuento,
								total,
								created_at, 
								updated_at)
		VALUES(		       
							 
						p_in_id_empresa,
						p_in_tipo_doc,
						p_in_id_rgt_conf ,
						 p_in_id_establecimiento ,
						 p_in_id_punto_emsion  ,
						 p_in_fecha_emision  ,
						 p_in_secuencia ,
                         #_v_sec_auto,
						 p_in_id_cliente ,
						 p_in_comentario  ,
						 p_in_forma_de_pago  ,
						 p_in_subtotal  ,
						 p_in_iva ,
						 p_in_descuento ,
						 p_in_total ,
							now(),
							now());
							
				SET out_cod = 7;
				SET out_secuencia = p_in_secuencia;
				SET out_msj = 'SE HA CREADO FACT CAB CORRECTAMENTE';
				SELECT 
    LAST_INSERT_ID() AS out_id_fact,
    out_cod,
    out_msj,
    out_secuencia;
END IF;

ELSE BEGIN END;  
end case;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trx_productos_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trx_productos_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trx_productos_nc`(
						IN opcion text,
						IN p_in_id_product int,                        
						IN p_in_cod_prod VARCHAR(255),
                        IN p_in_descripcion VARCHAR(255),
                        IN p_in_stock  int, 
                        IN p_in_precio_unitario  decimal(8,2), 
                        IN p_in_id_empresa  int,
						IN p_in_estado char(10),
                   
						OUT out_cod INT,
                        OUT out_msj VARCHAR(500),
						OUT out_id_aut INT,
                        OUT out_cod_proct VARCHAR(500))
BEGIN
declare _v_cod_prod int;
case opcion  
  when 'AA' then 
	set _v_cod_prod = (SELECT count(*) FROM productos_nc where  cod_prod = p_in_cod_prod );
	IF _v_cod_prod >= 1 THEN
			SET out_cod = 6;
			SET out_cod_proct = p_in_cod_prod;
			SET out_msj = 'YA EXISTE CODIGO PRODUCTO. POR FAVOR REVISE!';
			SET  out_id_aut = (SELECT id_product FROM productos_nc where  cod_prod = p_in_cod_prod);
SELECT 
    out_id_aut,
    out_cod,
    out_msj,
    out_cod_proct;
	ELSE 
		INSERT INTO productos_nc(
								cod_prod, 
                                descripcion,
                                stock,
                                precio_unitario,
                                id_empresa,
                                estado,
								created_at, 
								updated_at)
		VALUES(		       
							p_in_cod_prod,
                            p_in_descripcion,
                            p_in_stock,
                            p_in_precio_unitario,
                            p_in_id_empresa,
                            p_in_estado,
							now(),
							now());
							
				SET out_cod = 7;
				SET out_cod_proct = p_in_cod_prod;
				SET out_msj = 'SE HA CREADO PRODUCTO CORRECTAMENTE';
				SELECT 
    LAST_INSERT_ID() AS out_id_aut,
    out_cod,
    out_msj,
    out_cod_proct;
END IF;
when 'AB' then
UPDATE productos_nc 
SET 
    descripcion = p_in_descripcion,
    stock = p_in_stock,
    precio_unitario = p_in_precio_unitario,
    id_empresa = p_in_id_empresa,
    estado = p_in_estado,
    updated_at = NOW()
WHERE
    cod_prod = p_in_cod_prod;
				SET out_cod = 7;
				SET out_cod_proct = p_in_cod_prod;				
                SET out_msj = 'SE HA MODIFICADO PRODUCTO CORRECTAMENTE';
SELECT 
    p_in_id_product as out_id_aut,
    out_cod,
    out_msj,
    out_cod_proct;
  
ELSE BEGIN END;  
end case;

END
;;
DELIMITER ;
