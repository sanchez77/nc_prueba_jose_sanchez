@extends("theme.$theme.layout")
@section('titulo')
Factura
@endsection

@section('styles')

<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/responsive.bootstrap4.min.css")}}">
<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/pages.css")}}" >


<style type="text/css">
   .loaders {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('{{ asset("assets/$theme/gif/load.gif")}}') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;
}
</style>


@endsection

@section('contenido')
<div class="pcoded-inner-content">
	<div class="main-body">
		<div class="page-wrapper">
			<div class="page-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-header">
								<h5>Revisar Facturas</h5>
								<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
							</div>
							<div class="card-block">

								<form action="{{ route('Crear.Factura_Post') }}" id="form-factura" novalidate>
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">


									<div class="row">
											<div class="col-md-4">
												<label for="inputCity">Cliente</label>
												<div class="input-group mb-0">
													<span class="d-node" id="rt-buscar-cliente" action="{{ route('Buscar.ClienteModal') }}"></span>
													<input type="text"  id="inp-buscar-cliente" cod_cliente = 0 num_documento=0 class="form-control" placeholder="Buscar cliente" >
													<div class="input-group-append">
													<button id="btn-buscar-cliente" class="btn btn-outline-secondary btn-sm" type="button"><i class="fa fa-search "></i></button>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<label for="inputState">Numero Documento</label>
												<input type="text" class="form-control input-sm" id="inp_num_doc">
											</div>
											<div class="col-md-4">

											</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="inputCity">Fecha Desde</label>
                                            <input type="date" class="form-control input-sm" id="inp_fecha_desde">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="inputState">Fecha Hasta</label>
											<input type="date" class="form-control input-sm" id="inp_fecha_hasta">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="inputState"></label>
                                            <span class="d-node" id="rt-buscar-facturas-cab" action="{{ route('consultar.Facturas_get') }}"></span>
                                            <button class="btn waves-effect waves-light btn-success" id="btn-buscar-facturas-cab" type="button"><i class="fa fa-search "></i>Buscar</button>
                                        </div>
                                </div>

                                <hr>
								<div class="row">
                                    <div class="col-sm-12">
									<div id="tb-cabecera-facturas"></div>
                                </div>
								</div>

                                <hr>
                                <div class="row">
                                    <div class="col-sm-12">
                                    <span class="d-node" id="rt-buscar-facturas-detalle" action="{{ route('consultar.Facturas_Detalle') }}"></span>
									<div id="tb-detalle-factura-view"></div>
                                </div>
								</div>



								</form>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

{{--  <span class="d-node" id="rt-buscar-producto" action="{{ route('Buscar.Producto') }}"></span> --}}
{{-- <div id="tb-producto-modal"></div> --}}

<div id="tb-personas"></div>

{{-- <div id="tb-list-empresas"></div> --}}



 @endsection


 @section('script')


 <script type="text/javascript" src="{{ asset("assets/$theme/js_sistema/revisarFactura.js?v=63")}}"></script>



 @endsection

