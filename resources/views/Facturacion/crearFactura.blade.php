@extends("theme.$theme.layout")
@section('titulo')
Factura
@endsection

@section('styles')

<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/responsive.bootstrap4.min.css")}}">
<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/pages.css")}}" >


<style type="text/css">
   .loaders {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('{{ asset("assets/$theme/gif/load.gif")}}') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;
}
</style>


@endsection

@section('contenido')
<div class="pcoded-inner-content">
	<div class="main-body">
		<div class="page-wrapper">
			<div class="page-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							{{-- <div class="card-header">
								<h5>Factura</h5>
								<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
							</div> --}}
							<div class="card-block">

								<form action="{{ route('Crear.Factura_Post') }}" id="form-factura" novalidate>
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">


									<fieldset class="border border-dark rounded p-2 col-sm-12">
									<legend class="border border-dark rounded">   Factura</legend>
										<div class="row ">
											<div class="col-md-4 ">
												<label for="inputCity">Empresa</label>
												<div class="input-group mb-0">
													<span class="d-none" id="dt-list-empresas-get"  action="{{ route('Consultar.Lista_Empresa_fact') }}"  ></span>
													<input type="text"  id="inp_empresa" cod_empresa=0 class="form-control" placeholder="Buscar Empresa" >
													<div class="input-group-append">
													<button id="btn-buscar-empresas" class="btn btn-outline-secondary btn-sm" type="button"><i class="fa fa-search "></i></button>
													</div>
												</div>
											</div>
											<div class=" col-md-4">
											<span class="d-none" id="buscar-punto-emsion-get"  action="{{ route('Consultar.PuntoEmision') }}"  ></span>
												<label for="inputState">Establecimiento</label>
												<input type="text" class="form-control input-sm" id="inp_establecimiento"  id_estab=0>
											</div>
											<div class=" col-md-4">
												<label for="inputZip">Punto Emision</label>
												<input type="text" class="form-control input-sm" id="inp_punto_emsion" id_punto_emision=0>
											</div>
										</div>

									<div class="row">
										<div class="col-md-4">
											<label for="inputState">Secuencial</label>
											<input disabled="disabled" type="text" class="form-control input-sm" id="inpt_secuencial" id_rgt=0>
										</div>

										<div class="col-md-4">
											<label for="inputState">Fecha</label>
											<input type="date" class="form-control input-sm" id="inp_fecha">
										</div>
									{{--
									<div class="form-group col-md-4">
										<label for="inputZip">Zip</label>
										<input type="text" class="form-control input-sm" id="inputZip">
									</div>
									--}}
									</div>

                                        <hr>
									<div class="row">
											<div class="col-md-4">
												<label for="inputCity">Cliente</label>
												<div class="input-group mb-0">
													<span class="d-node" id="rt-buscar-cliente" action="{{ route('Buscar.Cliente') }}"></span>
													<input type="text"  id="inp-buscar-cliente" cod_cliente = 0 class="form-control" placeholder="Buscar cliente" >
													<div class="input-group-append">
													<button id="btn-buscar-cliente" class="btn btn-outline-secondary btn-sm" type="button"><i class="fa fa-search "></i></button>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<label for="inputState">Numero Documento</label>
												<input type="text" class="form-control input-sm" id="inp_num_doc">
											</div>
											<div class="col-md-4">
												<label for="inputZip">Correo</label>
												<input type="text" class="form-control input-sm" id="inp_email">
											</div>
									</div>

									<div class="form-row">
											<div class="form-group col-md-4">
												<label for="inputCity">Direccion</label>
												<input type="text" class="form-control input-sm" id="inp_direccion">
											</div>
											<div class="form-group col-md-4">
												<label for="inputState">Nota</label>
												<input type="text" class="form-control input-sm" id="inp_nota">
											</div>
											<div class="form-group col-md-4">
												{{-- <label for="inputZip">Correo</label>
												<input type="text" class="form-control input-sm" id="inputZip"> --}}
											</div>
									</div>
								</fieldset>
								<hr>
								<div class="row">

									<div id="tb-detalle-factura"></div>

									</div>
                                <hr>
								<div class="row">
                                    <div class="col-sm-7">
                                        <div class="form-group row error-group">
                                            <label for="digito1" class="col-sm-4 control-label ">Forma de Pago</label>
                                            <div class="col-sm-8">
												<select name="select_forma_pago" id="select_tipo_doc" class="form-control form-control-info fill">
                                                    <option value="0" disabled="disabled">--SELECCIONAR DOCUMENTO--</option>
                                                    <option value="SIN UTILIZACIÓN DEL SISTEMA FINANCIERO" selected="true" >SIN UTILIZACIÓN DEL SISTEMA FINANCIERO</option>
                                                    <option value="TARJETA DE CRÉDITO" >TARJETA DE CRÉDITO</option>
                                                    <option value="TARJETA DE DÉBITO">TARJETA DE DÉBITO</option>
                                                    <option value="OTROS CON UTILIZACIÓN DEL SISTEMA FINANCIERO">OTROS CON UTILIZACIÓN DEL SISTEMA FINANCIERO</option>
                                                     </select>
                                              </div>
                                        </div>
                                        <br>
                                        <div id="tb-stock-factura"></div>
                                    </div>



                                    <div class="col-sm-5">

                                        <div class="form-group row ">
                                            <label for="fac_total_cero" class="col-sm-4 control-label ">SubTotal</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="0.00" class="form-control fac_total_cero text-right" id="inp_sub_total" name="fac_total_cero" placeholder="" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fa_descuento" class="col-sm-4 control-label ">Descuento</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="0.00" class="form-control fa_descuento text-right" id="inp_descuento" name="inp_descuento" placeholder="" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fac_iva" class="col-sm-4 control-label ">IVA</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="0.00" class="form-control fac_iva text-right" id="inp_iva" name="inp_iva" placeholder="" disabled="disabled">
                                            </div>
                                        </div>



                                        <div class="form-group row">
                                            <label for="fac_total" class="col-sm-4 control-label ">Total</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="0.00" class="form-control fac_total text-right" id="inp_total" name="inp_total" placeholder="" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>
                                </div>



								<hr>
									<div class="loaders d-none"></div>
									<div class="form-group row">
										<label class="col-sm-2"></label>
										<div class="col-sm-10">
											<button id="btn-grb-factura-form" type="button" class="btn btn-primary m-b-0">Guardar</button>
										</div>
									</div>
									<div id="load-grb" class="row m-t-30 d-none">
										<div class="col-md-12">
											<div class="btn btn-primary btn-md btn-block pull-right waves-effect text-center m-b-20 btn-disabled btn-outline-disabled disabled"><i class="feather icon-aperture rotate-refresh"></i>Procesando...</div>
										</div>
									</div>
								</form>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<span class="d-node" id="rt-buscar-producto" action="{{ route('Buscar.Producto') }}"></span>
<div id="tb-producto-modal"></div>

<div id="tb-personas"></div>

<div id="tb-list-empresas"></div>




 @endsection


 @section('script')

 {{-- <script src="{{ asset("assets/$theme/js/responsive.bootstrap4.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/underscore-min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/moment.min.js")}}"  type="text/javascript"></script>
 <script type="text/javascript" src="{{ asset("assets/$theme/js/validate.js")}}" ></script>
 <script type="9fef68816305b81b29a5d613-text/javascript" src="{{ asset("assets/$theme/js/form-validation.js")}}"></script> --}}
 {{-- <script src="{{ asset("assets/$theme/js/vertical-layout.min.js")}}"   type="text/javascript"></script> --}}
 <script type="text/javascript" src="{{ asset("assets/$theme/js_sistema/crearFactura.js?v=016")}}"></script>



 @endsection

