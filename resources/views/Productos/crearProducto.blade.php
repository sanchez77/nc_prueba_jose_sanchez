@extends("theme.$theme.layout")
@section('titulo')
Crear Producto
@endsection

@section('styles')

<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/responsive.bootstrap4.min.css")}}">
<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/pages.css")}}" >


<style type="text/css">
   .loaders {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('{{ asset("assets/$theme/gif/load.gif")}}') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;
}
</style>


@endsection

@section('contenido')
<div class="pcoded-inner-content">
	<div class="main-body">
		<div class="page-wrapper">
			<div class="page-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-header">
								<h5>Registrar Producto</h5>
								<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
							</div>
							<div class="card-block">

								<form action="{{ route('Crear.Producto_Post') }}" id="form-producto" novalidate>
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                                    <div class="row">
										<div class="col-sm-6">
											<div class="form-group row error-group">
												<label class="col-sm-4 col-form-label">Codigo Producto:</label>
												<div class="col-sm-8">
                                                    <div class="input-group mb-3">
                                                    <span class="d-node" id="rt-buscar-producto" action="{{ route('Buscar.Producto') }}"></span>
                                                    <input type="text"  id="inp_codigo_producto" cod_prod = 0 class="form-control" placeholder="Ingrese codigo" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                                    <div class="input-group-append">
                                                      <button id="btn-buscar-producto" class="btn btn-outline-secondary" type="button"><i class="fa fa-search"></i></button>
                                                    </div>
                                                  </div>
                                                </div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group row error-group">
												<label class="col-sm-4 col-form-label">Descripcion:</label>
												<div class="col-sm-8">
                                                    <textarea rows="2" cols="2" class="form-control" name="inp_descripcion" id="inp_descripcion" placeholder="Ingresar la descripcion"></textarea>
                                                    <span class="messages"></span> </div>
											</div>
										</div>
                                    </div>

                                    <div class="row">
										<div class="col-sm-6">
											<div class="form-group row error-group">
												<label class="col-sm-4 col-form-label">Stock:</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="inp_stock" id="inp_stock" placeholder="Ingresar Stock">
													<span class="messages"></span> </div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group row error-group">
												<label class="col-sm-4 col-form-label">Precio Unitario:</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="inp_precio_unitario" id="inp_precio_unitario" placeholder="Ingresar Precio Unitario">
													<span class="messages"></span> </div>
											</div>
										</div>
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-6">
											<div class="form-group row error-group">
												<label class="col-sm-4 col-form-label">Estado:</label>
												<div class="col-sm-8">
													<select name="select_estado" id="select_estado" class="form-control form-control-info fill">
                                                    <option value="0" disabled="disabled">--SELECCIONAR ESTADO--</option>
                                                    <option value="A" selected="true">ACTIVO</option>
                                                    <option value="I">INACTIVO</option>
                                                </select>
												</div>
											</div>
                                        </div>


                                        <div class="col-sm-6">
											<div class="form-group row error-group">
												{{-- <label class="col-sm-4 col-form-label">Empresa:</label>
												<div class="col-sm-8">
                                                    <select name="select_empresa" id="select_empresa" class="form-control form-control-info fill">
                                                        <option value="0" disabled="disabled">--SELECCIONAR Empresa--</option>
                                                        @foreach($empresas as $emp)
                                                            <option value="{{ $emp->id_empresa }}">{{$emp->razon_comercial}}</option>
                                                        @endforeach
                                                    </select>
                                                </select>
												</div> --}}
											</div>
										</div>

									</div>


									<div class="loaders d-none"></div>
									<div class="form-group row">
										<label class="col-sm-2"></label>
										<div class="col-sm-10">
											<button id="btn-grb-producto-form" type="button" class="btn btn-primary m-b-0">Guardar</button>
										</div>
									</div>
									<div id="load-grb" class="row m-t-30 d-none">
										<div class="col-md-12">
											<div class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20 btn-disabled btn-outline-disabled disabled"><i class="feather icon-aperture rotate-refresh"></i>Procesando...</div>
										</div>
									</div>
								</form>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="tb-producto"></div>


 @endsection

 @section('script')

 {{-- <script src="{{ asset("assets/$theme/js/responsive.bootstrap4.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/underscore-min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/moment.min.js")}}"  type="text/javascript"></script>
 <script type="text/javascript" src="{{ asset("assets/$theme/js/validate.js")}}" ></script>
 <script type="9fef68816305b81b29a5d613-text/javascript" src="{{ asset("assets/$theme/js/form-validation.js")}}"></script> --}}
 {{-- <script src="{{ asset("assets/$theme/js/vertical-layout.min.js")}}"   type="text/javascript"></script> --}}
 <script type="text/javascript" src="{{ asset("assets/$theme/js_sistema/producto.js?v=002")}}"></script>



 @endsection

