<nav class="pcoded-navbar" pcoded-header-position="relative">
    <div class="nav-list">
       <div class="pcoded-inner-navbar main-menu">
          <div class="pcoded-navigation-label">Navigation</div>
          <ul class="pcoded-item pcoded-left-item">
             <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                <span class="pcoded-mtext">Inicio</span>
                </a>
                <ul class="pcoded-submenu">
                   <li class="">
                      <a href="{{ route('PanelAdministrativo') }} " class="waves-effect waves-dark">
                      <span class="pcoded-mtext">Default</span>
                      </a>
                   </li>
                </ul>
             </li>
              {{-- inicio nuevo menu --}}
              <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                <span class="pcoded-micon"><i class="fa fa-cogs"></i></span>
                <span class="pcoded-mtext">Configuracion</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href=" {{ route('Crear.Empresa') }} " class="waves-effect waves-dark">
                        <span class="pcoded-mtext">Crear Empresa</span>
                        </a>
                    </li>
                    <li class="">
                        <a href=" {{ Route('Empresa') }} " class="waves-effect waves-dark">
                        <span class="pcoded-mtext">Lista Empresa</span>
                        </a>
                    </li>
                    <li class="">
                        <a href=" {{ Route('Crear.Configuracion_Empresa') }} " class="waves-effect waves-dark">
                        <span class="pcoded-mtext">Confg Ems & PtEm </span>
                        </a>
                    </li>

                   {{-- <li class="">
                      <a href=" {{ url('establecimiento') }} " class="waves-effect waves-dark">
                      <span class="pcoded-mtext">Establecimiento</span>
                      </a>
                   </li>
                   <li class="">
                      <a href="dashboard-crm.html" class="waves-effect waves-dark">
                      <span class="pcoded-mtext">Punto Emisión</span>
                      </a>
                   </li>
                   <li class="">
                      <a href="dashboard-analytics.html" class="waves-effect waves-dark">
                      <span class="pcoded-mtext">Secuencial Factura</span>
                      <span class="pcoded-badge label label-info ">New</span>
                      </a>
                   </li> --}}
                </ul>
             </li>

             <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                <span class="pcoded-micon"><i class="fa fa-users"></i></span>
                <span class="pcoded-mtext">General</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href=" {{ route('Crear.Cliente_view') }} " class="waves-effect waves-dark">
                        <span class="pcoded-mtext">Crear/Modificar Cliente</span>
                        </a>
                    </li>
                    <li class="">
                        <a href=" {{ Route('Lista.Cliente') }} " class="waves-effect waves-dark">
                        <span class="pcoded-mtext">Lista Clientes</span>
                        </a>
                    </li>
                </ul>
             </li>

             <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                <span class="pcoded-micon"><i class="fa fa-cube"></i></span>
                <span class="pcoded-mtext">Productos</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href=" {{ route('Crear.Producto_view') }} " class="waves-effect waves-dark">
                        <span class="pcoded-mtext">Crear/Modificar Producto</span>
                        </a>
                    </li>
                    <li class="">
                        <a href=" {{ Route('Lista.Productos') }} " class="waves-effect waves-dark">
                        <span class="pcoded-mtext">Lista Productos</span>
                        </a>
                    </li>
                </ul>
             </li>
             {{-- fin nuevo menu --}}
             {{-- active pcoded-trigger --}}


          <div class="pcoded-navigation-label">Facturacion</div>
          <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                <span class="pcoded-micon"><i class="fa fa-cube"></i></span>
                <span class="pcoded-mtext">Facturacion</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href=" {{ route('Crear.Factura_view') }} " class="waves-effect waves-dark">
                        <span class="pcoded-mtext">Factura</span>
                        </a>
                    </li>
                    <li class="">
                        <a href=" {{ Route('consultar.Facturas_view') }} " class="waves-effect waves-dark">
                        <span class="pcoded-mtext">Revision de Facturas</span>
                        </a>
                    </li>
                </ul>
             </li>


          </ul>





       </div>
    </div>
 </nav>
