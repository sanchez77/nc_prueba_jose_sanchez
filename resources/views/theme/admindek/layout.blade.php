<!DOCTYPE html>
<html lang="es">
<head>
     {{-- <!-- Mirrored from colorlib.com/polygon/admindek/default/menu-sidebar.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 12 Dec 2019 16:08:34 GMT --> --}}
   {{-- <!-- Added by HTTrack --> --}}
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <!-- /Added by HTTrack -->

    <title>@yield('titulo', 'Home')| JS</title>
    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Admindek Bootstrap admin template made using Bootstrap 4 and it has huge amount of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
    <meta name="keywords" content="bootstrap, bootstrap admin template, admin theme, admin dashboard, dashboard template, admin template, responsive" />
    <meta name="author" content="colorlib" />


    <link rel="icon" href="https://colorlib.com/polygon/admindek/files/assets/images/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:500,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{ asset("assets/$theme/css/waves.min.css")}}"  type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/feather.css")}}">
    <link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/themify-icons.css")}}">
    <link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/icofont.css")}}">
    <link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/font-awesome.min.css")}}">
    <link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/prism.css")}}" >
    <link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/component.css")}}" >
      <link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/style.css")}}">
      <link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/pages.css")}}" >

      {{-- fancygrid --}}
      <link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/fancygrid/fancy.min.css?v=001")}}" >


    @yield('styles')
</head>
<body>
  {{-- Barra de load --}}
  <div class="loader-bg">
    <div class="loader-bar"></div>
    </div>


<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>

    <div class="pcoded-container navbar-wrapper">
        {{-- inicio Navar --}}
        @include("theme/$theme/header")
        {{-- fin navar --}}

        {{-- inicio sidebar --}}
        @include("theme/$theme/sidebar")
        {{-- fin sidebar --}}


         {{-- inicio Chat_inner --}}
         @include("theme/$theme/chat_inner")
         {{-- fin Chat_inner --}}


        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                {{-- inicio  nav-list --}}
                @include("theme/$theme/nav-list")
                {{-- fin  nav-list --}}

                <div class="pcoded-content">
                    {{-- inicio migas de pan --}}
                    {{-- <div class="page-header card">
                        <div class="row align-items-end">
                           <div class="col-lg-8">
                              <div class="page-header-title">
                                 <i class="feather icon-sidebar bg-c-blue"></i>
                                 <div class="d-inline">
                                    <h5>Sidebar Fixed</h5>
                                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <div class="page-header-breadcrumb">
                                 <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                       <a href="index.html"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Page Layout</a></li>
                                    <li class="breadcrumb-item"><a href="#!">Vertical</a></li>
                                    <li class="breadcrumb-item"><a href="#!">Sidebar Fixed</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div> --}}
                    {{-- fin migas de pan --}}

                    {{-- inicio del contenido --}}
                    <div class="pcoded-inner-content">
                        <div class="main-body">
                           <div class="page-wrapper">
                              <div class="page-body">


                                {{-- @include("theme/$theme/p_inicio") --}}
                                @yield('contenido')
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- @yield('contenido') --}}



                    {{-- fin del contenido --}}
                </div>
                <div id="styleSelector"></div>
            </div>
        </div>



    </div>
</div>


<script type="text/javascript" src="{{ asset("assets/$theme/js/jquery.min.js?v=002")}}"></script>
<script type="text/javascript" src="{{ asset("assets/$theme/js/jquery-ui.min.js")}}"></script>
<script type="text/javascript" src="{{ asset("assets/$theme/js/popper.min.js")}}"></script>
<script type="text/javascript" src="{{ asset("assets/$theme/js/bootstrap.min.js")}}"></script>
<script src="{{ asset("assets/$theme/js/waves.min.js")}}" type="-text/javascript"></script>
<script type="text/javascript" src="{{ asset("assets/$theme/js/jquery.slimscroll.js")}}" ></script>
<script type="text/javascript" src="{{ asset("assets/$theme/js/modernizr.js")}}"></script>
<script type="text/javascript" src="{{ asset("assets/$theme/js/css-scrollbars.js")}}"></script>
<script type="text/javascript" src="{{ asset("assets/$theme/js/custom-prism.js")}}"></script>
<script type="d28fd8086f5eb18f81d8672a-text/javascript"  src="{{ asset("assets/$theme/js/modal.js")}}"></script>
<script src="{{ asset("assets/$theme/js/classie.js")}}" type="d28fd8086f5eb18f81d8672a-text/javascript"></script>
<script type="d28fd8086f5eb18f81d8672a-text/javascript" src="{{ asset("assets/$theme/js/modaleffects.js")}}"></script>
<script src="{{ asset("assets/$theme/js/pcoded.min.js")}}" type="text/javascript"></script>
<script src="{{ asset("assets/$theme/js/menu-sidebar-fixed.js")}}" type="text/javascript"></script>
<script src="{{ asset("assets/$theme/js/jquery.mcustomscrollbar.concat.min.js")}}"  type="text/javascript"></script>
<script type="text/javascript" src="{{ asset("assets/$theme/js/script.js")}}"></script>
{{-- <script type="text/javascript" src="{{ asset("assets/$theme/js/jquery-loading.js")}}"></script> --}}
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="text/javascript"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

{{-- fancygrid --}}
<script type="text/javascript" src="{{ asset("assets/$theme/fancygrid/fancy.min.js?v=003")}}" ></script>
<script type="text/javascript" src="{{ asset("assets/$theme/fancygrid/modules/selection.js")}}" ></script>

<script src="https://cdn.fancygrid.com/@1.7.96/modules/i18n/es.js" charset="utf-8" async=""></script>
{{-- @yield('script-adionales') --}}
<script type="text/javascript">
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-23581568-13');

</script>
<script  src="{{ asset("assets/$theme/js/rocket-loader.min.js")}}" data-cf-settings="ebf65459e4cbcc04709bcfbf-|49" defer=""></script>



{{-- <script>
history.pushState(null, null, document.title);
window.addEventListener('popstate', function() {
    history.pushState(null, null, document.title);

});
</script>  --}}

@yield('script')
</body>
</html>
