@extends("theme.$theme.layout")
@section('titulo')
Crear Cliente
@endsection

@section('styles')

<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/responsive.bootstrap4.min.css")}}">
<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/pages.css")}}" >


<style type="text/css">
   .loaders {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('{{ asset("assets/$theme/gif/load.gif")}}') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;
}
</style>


@endsection

@section('contenido')
<div class="pcoded-inner-content">
	<div class="main-body">
		<div class="page-wrapper">
			<div class="page-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-header">
								<h5>Registrar Datos del Cliente</h5>
								<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
							</div>
							<div class="card-block">

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Buscar:</label>
                                    <div class="col-sm-10">
                                        <div class="input-group mb-3">
                                            <span class="d-node" id="rt-buscar-cliente" action="{{ route('Buscar.Cliente') }}"></span>
                                            <input type="text"  id="inp-buscar-cliente" class="form-control" placeholder="Numero Documento (o) Nombre de persona" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                              <button id="btn-buscar-cliente" class="btn btn-outline-secondary" type="button"><i class="fa fa-search"></i></button>
                                            </div>
                                          </div>
                                    </div>
                                 </div>
                                 <hr>
								<form action="{{ route('Crear.Cliente_Post') }}" id="form-cliente-empresa" novalidate>
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group row error-group">
												<label for="digito1" class="col-sm-4 control-label ">Documento</label>
												<div class="col-sm-8">
													<select name="select_tipo_doc" id="select_tipo_doc" class="form-control form-control-info fill">
                                                    <option value="0" disabled="disabled">--SELECCIONAR DOCUMENTO--</option>
                                                    <option value="04" >RUC</option>
                                                    <option value="05" selected="true">CEDULA</option>
                                                    <option value="06">PASAPORTE</option>
                                                    <option value="07">CONSUMIDOR FINAL</option>
                                                    <option value="08">IDENTIFICACION DELEXTERIOR</option>

                                                </select> </div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group row error-group">
												<label class="col-sm-4 col-form-label">Numero Documento:</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="inp_num_doc" cod_cliente= 0 id="inp_num_doc" placeholder="Ingresar Numero del Documento">
													<span class="messages"></span> </div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group row error-group">
												<label class="col-sm-4 col-form-label">Nombres:</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="inp_nombre" id="inp_nombre" placeholder="Ingresar Nombre">
													<span class="messages"></span> </div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group row error-group">
												<label class="col-sm-4 col-form-label">Apelidos:</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="inp_apellidos" id="inp_apellidos" placeholder="Ingresar Apellido">
													<span class="messages"></span> </div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group row error-group">
												<label class="col-sm-4 col-form-label">Razon Comercial:</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="inp_razon_comercial" id="inp_razon_comercial" placeholder="Ingresar Razon Comercial">
													<span class="messages"></span> </div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group row error-group">
												<label class="col-sm-4 col-form-label">Email:</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="inp_email" id="inp_email" placeholder="noemail@mail.com">
													<span class="messages"></span> </div>
											</div>
										</div>
                                    </div>

                                    <div class="row">
										<div class="col-sm-6">
											<div class="form-group row error-group">
												<label class="col-sm-4 col-form-label">Fecha Nacimiento:</label>
												<div class="col-sm-8">
													<input type="date" class="form-control" name="inp_fecha_nacimiento" id="inp_fecha_nacimiento" placeholder="Ingresar Numero del Documento">
                                                    <span class="messages"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
											<div class="form-group row error-group">
                                                <label class="col-sm-4 col-form-label">Edad:</label>
                                                <div class="col-sm-8">
												<input type="text" class="form-control" name="inp_edad" id="inp_edad" placeholder="Edad">
                                                    <span class="messages"></span>
                                                </div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group row error-group">
												<label class="col-sm-4 col-form-label">Telefono:</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="inp_telefono" id="inp_telefono" placeholder="Ingresar Telefono">
													<span class="messages"></span> </div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group row error-group">
												<label class="col-sm-4 col-form-label">Celular:</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="inp_celular" id="inp_celular" placeholder="Ingresar Celular">
													<span class="messages"></span> </div>
											</div>
										</div>
									</div>

									<div class="row">
                                        <div class="col-sm-6">
											<div class="form-group row error-group">
												<label class="col-sm-4 col-form-label">ESTADO:</label>
												<div class="col-sm-8">
													<select name="select_estado" id="select_estado" class="form-control form-control-info fill">
                                                    <option value="0" disabled="disabled">--SELECCIONAR ESTADO--</option>
                                                    <option value="A" selected="true">ACTIVO</option>
                                                    <option value="I">INACTIVO</option>
                                                </select>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group row error-group">

											</div>
										</div>

									</div>

									<div class="form-group row">
										<label class="col-sm-2 col-form-label">Direccion:</label>
										<div class="col-sm-10">
											<textarea rows="5" cols="5" class="form-control" name="inp_direccion" id="inp_direccion" placeholder="Ingresar la direccion"></textarea>
										</div>
									</div>




									<div class="loaders d-none"></div>
									<div class="form-group row">
										<label class="col-sm-2"></label>
										<div class="col-sm-10">
											<button id="btn-grb-cliente-form" type="button" class="btn btn-primary m-b-0">Guardar</button>
										</div>
									</div>
									<div id="load-grb" class="row m-t-30 d-none">
										<div class="col-md-12">
											<div class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20 btn-disabled btn-outline-disabled disabled"><i class="feather icon-aperture rotate-refresh"></i>Procesando...</div>
										</div>
									</div>
								</form>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="tb-personas"></div>


{{-- <div class="md-modal md-effect-2 modal-white md-hiden" id="modal-3">
    <div class="md-content">
       <h3>Buscar Personas</h3>
       <div>
        <div class="modal-body">
            <div id="tb-personas"></div>
          </div>
          <div class="loaders d-none"></div>
          <div id="load-grb" class="row m-t-30 d-none">
            <div class="col-md-12">
                <div class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20 btn-disabled btn-outline-disabled disabled"><i class="feather icon-aperture rotate-refresh"></i>Procesando...</div>
            </div>
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="button" id="btn-grb-configuracion-form"  class="btn btn-primary">Guardar</button>
          </div>
       </div>
    </div>
 </div> --}}



 @endsection


 @section('script')

 <script src="{{ asset("assets/$theme/js/responsive.bootstrap4.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/underscore-min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/moment.min.js")}}"  type="text/javascript"></script>
 <script type="text/javascript" src="{{ asset("assets/$theme/js/validate.js")}}" ></script>
 <script type="9fef68816305b81b29a5d613-text/javascript" src="{{ asset("assets/$theme/js/form-validation.js")}}"></script>
 {{-- <script src="{{ asset("assets/$theme/js/vertical-layout.min.js")}}"   type="text/javascript"></script> --}}
 <script type="text/javascript" src="{{ asset("assets/$theme/js_sistema/cliente.js?v=005")}}"></script>



 @endsection

