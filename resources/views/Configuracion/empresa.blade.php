@extends("theme.$theme.layout")
@section('titulo')
Empresa
@endsection

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/datatables.bootstrap4.min.css")}}">
<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/buttons.datatables.min.css")}}" >
<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/responsive.bootstrap4.min.css")}}">

<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/pages.css")}}" >
@endsection

@section('contenido')

    <div class="pcoded-inner-content">
        <div class="main-body">
        <div class="page-wrapper">
            <div class="page-body">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                           <h5>Individual Column Searching (Text Inputs)</h5>
                           <span>The searching functionality that is provided by DataTables is very useful for quickly search through the information in the table - however the search is global, and you may wish to present controls to search on specific columns only. DataTables has the ability to apply searching to a specific column through the column().search() method (note that the name of the method is search not filter since filter() is used to apply a filter to a result set).</span>
                        </div>
                        <div class="card-block">
                            <button type="button" id="add-empresa" class="btn btn-primary m-b-20">+ Add Row</button>
                           <div class="dt-responsive table-responsive">
                              <table id="dt-empresas" class="table table-striped table-bordered nowrap">
                                 <thead>
                                    <tr>
                                       <th>Name</th>
                                       <th>Position</th>
                                       <th>Office</th>
                                       <th>Age</th>
                                       <th>Start date</th>
                                       <th>Salary</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>Tiger Nixon</td>
                                       <td>System Architect</td>
                                       <td>Edinburgh</td>
                                       <td>61</td>
                                       <td>2011/04/25</td>
                                       <td>$320,800</td>
                                    </tr>
                                    <tr>
                                       <td>Garrett Winters</td>
                                       <td>Accountant</td>
                                       <td>Tokyo</td>
                                       <td>63</td>
                                       <td>2011/07/25</td>
                                       <td>$170,750</td>
                                    </tr>
                                    <tr>
                                       <td>Ashton Cox</td>
                                       <td>Junior Technical Author</td>
                                       <td>San Francisco</td>
                                       <td>66</td>
                                       <td>2009/01/12</td>
                                       <td>$86,000</td>
                                    </tr>
                                    <tr>
                                       <td>Cedric Kelly</td>
                                       <td>Senior Javascript Developer</td>
                                       <td>Edinburgh</td>
                                       <td>22</td>
                                       <td>2012/03/29</td>
                                       <td>$433,060</td>
                                    </tr>
                                    <tr>
                                       <td>Airi Satou</td>
                                       <td>Accountant</td>
                                       <td>Tokyo</td>
                                       <td>33</td>
                                       <td>2008/11/28</td>
                                       <td>$162,700</td>
                                    </tr>
                                    <tr>
                                       <td>Brielle Williamson</td>
                                       <td>Integration Specialist</td>
                                       <td>New York</td>
                                       <td>61</td>
                                       <td>2012/12/02</td>
                                       <td>$372,000</td>
                                    </tr>
                                    <tr>
                                       <td>Herrod Chandler</td>
                                       <td>Sales Assistant</td>
                                       <td>San Francisco</td>
                                       <td>59</td>
                                       <td>2012/08/06</td>
                                       <td>$137,500</td>
                                    </tr>
                                    <tr>
                                       <td>Rhona Davidson</td>
                                       <td>Integration Specialist</td>
                                       <td>Tokyo</td>
                                       <td>55</td>
                                       <td>2010/10/14</td>
                                       <td>$327,900</td>
                                    </tr>
                                    <tr>
                                       <td>Colleen Hurst</td>
                                       <td>Javascript Developer</td>
                                       <td>San Francisco</td>
                                       <td>39</td>
                                       <td>2009/09/15</td>
                                       <td>$205,500</td>
                                    </tr>
                                    <tr>
                                       <td>Sonya Frost</td>
                                       <td>Software Engineer</td>
                                       <td>Edinburgh</td>
                                       <td>23</td>
                                       <td>2008/12/13</td>
                                       <td>$103,600</td>
                                    </tr>
                                    <tr>
                                       <td>Jena Gaines</td>
                                       <td>Office Manager</td>
                                       <td>London</td>
                                       <td>30</td>
                                       <td>2008/12/19</td>
                                       <td>$90,560</td>
                                    </tr>
                                    <tr>
                                       <td>Quinn Flynn</td>
                                       <td>Support Lead</td>
                                       <td>Edinburgh</td>
                                       <td>22</td>
                                       <td>2013/03/03</td>
                                       <td>$342,000</td>
                                    </tr>
                                    <tr>
                                       <td>Charde Marshall</td>
                                       <td>Regional Director</td>
                                       <td>San Francisco</td>
                                       <td>36</td>
                                       <td>2008/10/16</td>
                                       <td>$470,600</td>
                                    </tr>
                                 </tbody>
                                 <tfoot>
                                    <tr>
                                       <th>Name</th>
                                       <th>Position</th>
                                       <th>Office</th>
                                       <th>Age</th>
                                       <th>Start date</th>
                                       <th>Salary</th>
                                    </tr>
                                 </tfoot>
                              </table>
                           </div>
                        </div>
                     </div>


                </div>
            </div>
            </div>
        </div>
        </div>
    </div>



 @endsection


 @section('script')
 <script src="{{ asset("assets/$theme/js/jquery.datatables.min.js")}}" type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/datatables.buttons.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/jszip.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/pdfmake.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/vfs_fonts.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/buttons.print.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/buttons.html5.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/datatables.bootstrap4.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/datatables.responsive.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/responsive.bootstrap4.min.js")}}"  type="text/javascript"></script>

 <script src="{{ asset("assets/$theme/js/data-table-custom.js")}}"  type="text/javascript"></script>
 {{-- <script src="{{ asset("assets/$theme/js/vertical-layout.min.js")}}"   type="text/javascript"></script> --}}

 <script>
$(document).ready(function() {

    // $('#dt-empresas').DataTable( {
    //     dom: 'Bfrtip',
    //     buttons: [
    //         {
    //             text: 'Agregar',
    //             className: 'red',
    //             id: 'uno',
    //             //extend: 'copyHtml5',
    //             action: function ( e, dt, node, config ) {
    //                 alert( 'Button activated' );
    //             }
    //         }
    //     ]
    // } );

    $('#dt-empresas tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');
    });
    var table = $('#dt-empresas').DataTable();
    table.columns().every(function (){
        var that = this;
		$('input', this.footer()).on('keyup change', function () {
			if (that.search() !== this.value) {
				that.search(this.value).draw();
			}
		});
    });


    var t = $('#dt-empresas').DataTable();
    var counter = 1;

    $('#add-empresa').on( 'click', function () {
        t.row.add( [
            counter +'.1',
            counter +'.2',
            counter +'.3',
            counter +'.4',
            counter +'.5',
            counter +'.6'
        ] ).draw( false );

        counter++;
    } );

    // Automatically add a first row of data
    $('#add-empresa').click();




    // $('#footer-search').DataTable( {
    //     dom: 'Bfrtip',
    //     buttons: [
    //         {
    //             text: 'Agregar',
    //             attr:  {
    //                 title: 'Copy',
    //                 id: 'id_add'
    //             },
    //             action: function ( e, dt, node, config ) {
    //                 alert( 'Button activated' );
    //             }
    //         }
    //     ]
    // } );


} );
 </script>


 @endsection

