@extends("theme.$theme.layout")
@section('titulo')
Crear Empresa
@endsection

@section('styles')


<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/pages.css")}}" >

<style type="text/css">
   .loaders {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('{{ asset("assets/$theme/gif/load.gif")}}') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;
}
</style>


@endsection

@section('contenido')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                           <div class="card">
                              <div class="card-header">
                                 <h5>Registrar Datos de la Empresa</h5>
                                 <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                              </div>
                              <div class="card-block">
                                 <form action="{{ route('Crear.Empresa_Post') }}" id="form-guadar-empresa" novalidate>
                                   @csrf
                                    <div class="form-group row">
                                       <label class="col-sm-2 col-form-label">Razon Social:</label>
                                       <div class="col-sm-10">
                                          <input type="text" class="form-control" name="inp_razon_social" id="razon_social" placeholder="Ingresar Razon Social">
                                          <span class="messages"></span>
                                       </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Razon Comercial:</label>
                                        <div class="col-sm-10">
                                           <input type="text" class="form-control" name="inp_razon_comercial" id="razon_comercial" placeholder="Ingresar Razon Comercial">
                                           <span class="messages"></span>
                                        </div>
                                     </div>

                                     <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Ruc:</label>
                                        <div class="col-sm-10">
                                           <input type="text" class="form-control" name="inp_ruc" id="ruc" placeholder="Ingresar RUC">
                                           <span class="messages"></span>
                                        </div>
                                     </div>

                                     <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                           <input type="email" class="form-control" id="email" name="inp_email" placeholder="Ingresar email">
                                           <span class="messages"></span>
                                        </div>
                                     </div>

                                     <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Direccion:</label>
                                        <div class="col-sm-10">
                                        <textarea rows="5" cols="5" class="form-control" name="inp_direccion" placeholder="Ingresar la direccion"></textarea>
                                        </div>
                                    </div>


                                    <div class="row form-group">
                                        <label class="col-sm-2 col-form-label">IVA (%):</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="inp_iva" class="form-control autonumber fill" data-a-sign="%" data-p-sign="s" placeholder="Ingresar (%) IVA">
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label class="col-sm-2 col-form-label">Estado:</label>
                                        <div class="col-sm-10">
                                            <select disabled="disabled" name="select_estado" class="form-control form-control-info fill">
                                                <option value="0" disabled="disabled">--SELECCIONAR ESTADO--</option>
                                                <option value="A" selected="true">ACTIVO</option>
                                                <option value="I">INACTIVO</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="loaders d-none"></div>
                                    <div class="form-group row">
                                       <label class="col-sm-2"></label>
                                       <div class="col-sm-10">
                                          <button id="btn-grb-empresa-form" type="button" class="btn btn-primary m-b-0">Guardar</button>
                                       </div>
                                    </div>
                                    <div id="load-grb" class="row m-t-30 d-none">
                                        <div class="col-md-12">
                                            <div class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20 btn-disabled btn-outline-disabled disabled"><i class="feather icon-aperture rotate-refresh"></i>Procesando...</div>
                                        </div>
                                    </div>
                                 </form>
                              </div>
                           </div>

                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>



 @endsection


 @section('script')


 <script src="{{ asset("assets/$theme/js/underscore-min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/moment.min.js")}}"  type="text/javascript"></script>
 <script type="text/javascript" src="{{ asset("assets/$theme/js/validate.js")}}" ></script>
 <script type="9fef68816305b81b29a5d613-text/javascript" src="{{ asset("assets/$theme/js/form-validation.js")}}"></script>
 {{-- <script src="{{ asset("assets/$theme/js/vertical-layout.min.js")}}"   type="text/javascript"></script> --}}
 <script type="text/javascript" src="{{ asset("assets/$theme/js_sistema/empresa_nc.js")}}"></script>



 @endsection

