@extends("theme.$theme.layout")
@section('titulo')
Lista Empresas
@endsection

@section('styles')

<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/datatables.bootstrap4.min.css")}}">
<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/buttons.datatables.min.css")}}" >
<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/responsive.bootstrap4.min.css")}}">


<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/pages.css")}}" >

<style type="text/css">
   .loaders {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('{{ asset("assets/$theme/gif/load.gif")}}') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;
},

</style>


@endsection

@section('contenido')

<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-body">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Lista Empresas</h5>
                            <span>The searching functionality that is provided by DataTables is very useful for quickly search through the information in the table - however the search is global, and you may wish to present controls to search on specific columns only. DataTables has the ability to apply searching to a specific column through the column().search() method (note that the name of the method is search not filter since filter() is used to apply a filter to a result set).</span>
                         </div>
                         <span class="d-none" id="dt-list-empresas-get"  action="{{ route('Consultar.Lista_Empresa') }}"  ></span>
                         {{-- <span class="d-none" id="dt-configuracion-post"  action="{{ route('Configuracion.Empresa_Post') }}"  ></span> --}}
                         <form action="{{ route('Modificar.Empresa_Post') }}" id="dt-empresa-post">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <div class="loaders d-none"></div>
                         </form>

                         <div class="card-block">
                            <div id="tb-list-empresas"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


 @endsection


 @section('script')

 <script src="{{ asset("assets/$theme/js/jquery.datatables.min.js")}}" type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/datatables.buttons.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/jszip.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/pdfmake.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/vfs_fonts.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/buttons.print.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/buttons.html5.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/datatables.bootstrap4.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/datatables.responsive.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/responsive.bootstrap4.min.js")}}"  type="text/javascript"></script>

 <script src="{{ asset("assets/$theme/js/data-table-custom.js")}}"  type="text/javascript"></script>

 <script type="text/javascript" src="{{ asset("assets/$theme/js_sistema/empresa_nc.js?v=002")}}"></script>


 @endsection

