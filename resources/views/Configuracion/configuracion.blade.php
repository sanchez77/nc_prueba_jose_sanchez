@extends("theme.$theme.layout")
@section('titulo')
Configuracion Empresa
@endsection

@section('styles')

<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/datatables.bootstrap4.min.css")}}">
<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/buttons.datatables.min.css")}}" >
<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/responsive.bootstrap4.min.css")}}">


<link rel="stylesheet" type="text/css" href="{{ asset("assets/$theme/css/pages.css")}}" >

<style type="text/css">
   .loaders {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('{{ asset("assets/$theme/gif/load.gif")}}') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;
},
.modal-blue .modal-backdrop {
background-color: #0000ff;
}

.modal-white .modal-backdrop {
background-color: #ffffff;
}
</style>


@endsection

@section('contenido')

<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-body">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Configuracion</h5>
                            <span>The searching functionality that is provided by DataTables is very useful for quickly search through the information in the table - however the search is global, and you may wish to present controls to search on specific columns only. DataTables has the ability to apply searching to a specific column through the column().search() method (note that the name of the method is search not filter since filter() is used to apply a filter to a result set).</span>
                         </div>
                         <span class="d-none" id="dt-configuracion-get"  action="{{ route('Consultar.Configuracion_Empresa') }}"  ></span>
                         {{-- <span class="d-none" id="dt-configuracion-post"  action="{{ route('Configuracion.Empresa_Post') }}"  ></span> --}}
                         <form action="{{ route('Configuracion.Empresa_Post') }}" id="dt-configuracion-post">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <div class="loaders d-none"></div>
                         </form>

                         <div class="card-block">
                            <div id="tb-configuracion"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    {{-- <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                               <h5>Configuracion</h5>
                               <span>The searching functionality that is provided by DataTables is very useful for quickly search through the information in the table - however the search is global, and you may wish to present controls to search on specific columns only. DataTables has the ability to apply searching to a specific column through the column().search() method (note that the name of the method is search not filter since filter() is used to apply a filter to a result set).</span>
                            </div>
                            <div class="card-block">
                                <button type="button" id="add-empresa" class="btn btn-primary m-b-20">+ Add Row</button>
                               <div class="dt-responsive table-responsive">
                                  <table id="dt-configuracion"  action="{{ route('Consultar.Configuracion_Empresa') }}"  class="table table-striped table-bordered nowrap">
                                     <thead>
                                        <tr>
                                            <th>Cod</th>
                                           <th>Cod Empresa</th>
                                           <th>Cod Establecimiento</th>
                                           <th>Establecimiento</th>
                                           <th>Cod Punto Emsion</th>
                                           <th>Punto Emsion</th>
                                           <th>Secuencia</th>
                                           <th>Estado</th>
                                           <th>Acciones</th>
                                        </tr>
                                     </thead>
                                     <tbody>

                                     </tbody>
                                     <tfoot>
                                        <tr>
                                            <th>Cod</th>
                                            <th>Cod Empresa</th>
                                            <th>Cod Establecimiento</th>
                                            <th>Establecimiento</th>
                                            <th>Cod Punto Emsion</th>
                                            <th>Punto Emsion</th>
                                            <th>Secuencia</th>
                                            <th>Estado</th>

                                        </tr>
                                     </tfoot>
                                  </table>
                               </div>
                            </div>
                         </div>

                    </div>
                </div>
            </div>
        </div>
    </div> --}}



    <div class="md-modal md-effect-2 modal-white md-hiden" id="modal-2">
        <div class="md-content">
           <h3>Configuracion</h3>
           <div>
            <div class="modal-body">
                <form action="{{ route('Configuracion.Empresa_Post') }}" id="form-guadar-configuracion">
                    @csrf
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label class="col-form-label">ID_REG:</label>
                            <input disabled="disabled"  type="text" class="form-control" name="id_reg" id="id_reg" placeholder="Cod Establecimiento:">
                            <span class="messages"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="col-form-label">Cog Empresa</label>
                            <input disabled="disabled" type="text" class="form-control" name="id_empresa" id="id_empresa" placeholder="Establecimiento:">
                            <span class="messages"></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label class="col-form-label">Cod Establecim:</label>
                            <input type="text" class="form-control" name="inp_cod_establecimiento" id="cod_establecimiento" placeholder="Cod Establecimiento:">
                            <span class="messages"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="col-form-label">Establecimiento:</label>
                            <input type="text" class="form-control" name="inp_establecimiento" id="establecimiento" placeholder="Establecimiento:">
                            <span class="messages"></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label class="col-form-label">Cod Put. Ems:</label>
                            <input type="text" class="form-control" name="inp_cod_put_ems" id="cod_put_ems" placeholder="Cod Put. Ems">
                            <span class="messages"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="col-form-label">Punto Emision:</label>
                            <input type="text" class="form-control" name="inp_punto_emision" id="punto_emision" placeholder="Punto Emision">
                            <span class="messages"></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label class="col-form-label">Secuencia:</label>
                            <input type="text" class="form-control" name="inp_secuencia" id="secuencia" placeholder="Cod Put. Ems">
                            <span class="messages"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="col-form-label">Estado:</label>
                            <select name="select_estado" class="form-control form-control-info fill">
                                <option value="0" disabled="disabled">--SELECCIONAR ESTADO--</option>
                                <option value="A" >ACTIVO</option>
                                <option value="I">INACTIVO</option>
                            </select>
                            <span class="messages"></span>
                        </div>
                    </div>


                </form>
              </div>
              <div class="loaders d-none"></div>
              <div id="load-grb" class="row m-t-30 d-none">
                <div class="col-md-12">
                    <div class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20 btn-disabled btn-outline-disabled disabled"><i class="feather icon-aperture rotate-refresh"></i>Procesando...</div>
                </div>
                </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-grb-configuracion-form"  class="btn btn-primary">Guardar</button>
              </div>
           </div>
        </div>
     </div>




 @endsection


 @section('script')

 <script src="{{ asset("assets/$theme/js/jquery.datatables.min.js")}}" type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/datatables.buttons.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/jszip.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/pdfmake.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/vfs_fonts.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/buttons.print.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/buttons.html5.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/datatables.bootstrap4.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/datatables.responsive.min.js")}}"  type="text/javascript"></script>
 <script src="{{ asset("assets/$theme/js/responsive.bootstrap4.min.js")}}"  type="text/javascript"></script>

 <script src="{{ asset("assets/$theme/js/data-table-custom.js")}}"  type="text/javascript"></script>
 {{-- <script src="{{ asset("assets/$theme/js/vertical-layout.min.js")}}"   type="text/javascript"></script> --}}

 {{-- <script>
$(document).ready(function() {


    // $('#dt-configuracion tfoot th').each(function () {
    //     var title = $(this).text();
    //     $(this).html('<input type="text" class="form-control" placeholder="Buscar ' + title + '" />');
    // });
    // var table = $('#dt-configuracion').DataTable();
    // table.columns().every(function (){
    //     var that = this;
	// 	$('input', this.footer()).on('keyup change', function () {
	// 		if (that.search() !== this.value) {
	// 			that.search(this.value).draw();
	// 		}
	// 	});
    // });




    // var t = $('#dt-empresas').DataTable();
    // var counter = 1;

    // $('#add-empresa').on( 'click', function () {
    //     t.row.add( [
    //         counter +'.1',
    //         counter +'.2',
    //         counter +'.3',
    //         counter +'.4',
    //         counter +'.5',
    //         counter +'.6'
    //     ] ).draw( false );

    //     counter++;
    // } );

    // $('#add-empresa').click();



} );
 </script> --}}
 <script type="text/javascript" src="{{ asset("assets/$theme/js_sistema/configuracion.js")}}"></script>


 @endsection

