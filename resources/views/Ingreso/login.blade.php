@extends("theme.$theme.auth")
@section('title')
Login
@endsection
{{--
@section('styles')
/* Copia de aquí */
.loader,
.loader:before,
.loader:after {
  background: #ffffff;
  -webkit-animation: load1 1s infinite ease-in-out;
  animation: load1 1s infinite ease-in-out;
  width: 1em;
  height: 4em;
}
.loader {
  color: #ffffff;
  text-indent: -9999em;
  margin: 0 11px;
  position: relative;
  font-size: 3px;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
  -webkit-animation-delay: -0.16s;
  animation-delay: -0.16s;
  display: inline-block;
}
.loader:before,
.loader:after {
  position: absolute;
  top: 0;
  content: '';
}
.loader:before {
  left: -1.5em;
  -webkit-animation-delay: -0.32s;
  animation-delay: -0.32s;
}
.loader:after {
  left: 1.5em;
}
@-webkit-keyframes load1 {
  0%,
  80%,
  100% {
    box-shadow: 0 0;
    height: 4em;
  }
  40% {
    box-shadow: 0 -2em;
    height: 5em;
  }
}
@keyframes load1 {
  0%,
  80%,
  100% {
    box-shadow: 0 0;
    height: 4em;
  }
  40% {
    box-shadow: 0 -2em;
    height: 5em;
  }
}
/* Hasta aquí */

/* Ignora esta clase*/
.btn-primary {
  background-color: blue;
}

@endsection --}}

@section('contenido_plantilla')
<div class="row">
    <div class="col-sm-12">
       <form class="md-float-material form-material" action="{{ route('IniciarSesion') }}" id="form-inicar-sesion">
           @csrf
          <div class="text-center">
             <img  src="{{ asset("assets/$theme/png/logo.png")}}" alt="logo.png">
          </div>
          <div class="auth-box card">
             <div class="card-block">
                <div class="row m-b-20">
                   <div class="col-md-12">
                      <h3 class="text-center txt-primary">Registrarse</h3>
                   </div>
                </div>
                {{-- <div class="row m-b-20">
                   <div class="col-md-6">
                      <button class="btn btn-facebook m-b-20 btn-block"><i class="icofont icofont-social-facebook"></i>facebook</button>
                   </div>
                   <div class="col-md-6">
                      <button class="btn btn-twitter m-b-20 btn-block"><i class="icofont icofont-social-twitter"></i>twitter</button>
                   </div>
                </div> --}}
                <p class="text-muted text-center p-b-5">Inicie sesión con su cuenta habitual</p>
                <div class="form-group form-primary">
                   <input type="text" name="email" class="form-control" required="">
                   <span class="form-bar"></span>
                   <label class="float-label">Nombre de usuario</label>
                </div>
                <div class="form-group form-primary">
                   <input type="password" name="password" class="form-control" required="">
                   <span class="form-bar"></span>
                   <label class="float-label">Contraseña</label>
                </div>
                {{-- <div class="row m-t-25 text-left">
                   <div class="col-12">
                      <div class="checkbox-fade fade-in-primary">
                         <label>
                         <input type="checkbox" value="">
                         <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                         <span class="text-inverse">Remember me</span>
                         </label>
                      </div>
                      <div class="forgot-phone text-right float-right">
                         <a href="auth-reset-password.html" class="text-right f-w-600"> Forgot Password?</a>
                      </div>
                   </div>
                </div> --}}
                <div class="row m-t-30" id="btn-login">
                   <div class="col-md-12">
                      <button id="btn-enviar-form" type="button" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">INICIAR SESIÓN</button>
                   </div>
                </div>
                <div id="load-login" class="row m-t-30 d-none">
                    <div class="col-md-12">
                        <div class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20 btn-disabled btn-outline-disabled disabled"><i class="feather icon-aperture rotate-refresh"></i>Procesando...</div>
                    </div>
                </div>

            <p class="text-inverse text-left">¿No tienes una cuenta? <a  href="{{ Route('Registar') }} "> <b>Regístrese aquí </b></a>gratis!</p>
             </div>
          </div>
       </form>
    </div>
 </div>
 @endsection

 @section('scripts')

 {{-- <script src="{{ asset("assets/$theme/js/rocket-loader.min.js")}}" data-cf-settings="4878d7dfa7bc22a8dfa99416-|49" defer=""></script> --}}

 <script type="4878d7dfa7bc22a8dfa99416-text/javascript" src="{{ asset("assets/$theme/js/auth/auth.js")}}"></script>

 @endsection
