<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/', function () {
//     return view('welcome');
// });

//Route::view('/', 'welcome');

// Route::view('/inicio', 'inicio_pt');

// Route::view('/prueba', 'index');

/*Route::get('/usuario', function () {
    return 'Usuario';
});*/

//Route::get('/practica', 'practicaController@index')->name('practicaHome');
//Route::get('/practica/create', 'practicaController@create')->name('practica.crear');

//Route::get('/establecimiento','practicaController@estable')->name('Establecimiento');


Route::get('/bienvenida', 'Admin\PantillaController@home')->name('Home');

//Route::view('/login_1','theme/admindek/auth-sign');
Route::get('/registar', 'Admin\PantillaController@registar')->name('Registar');


/**
 * Lista Empresa

Route::get('/empresa', 'Configuracion\EmpresaController@indexEmpresa')->name('Empresa');
Route::get('/con_json_empresas', 'Configuracion\EmpresaController@datatableEmpresasJson')->name('Consultar.Lista_Empresa');
 */

/**
 * Crear Empresas

Route::get('/crear_empresa', 'Configuracion\EmpresaController@crearEmpresa')->name('Crear.Empresa');
Route::post('/crear_empresa_post', 'Configuracion\EmpresaController@graba_empresa')->name('Crear.Empresa_Post');
Route::post('/modificar_empresa_post', 'Configuracion\EmpresaController@modificar_empresa')->name('Modificar.Empresa_Post');
 */
/**
 * Configuracion Emsy Punto de Emsion

Route::get('/configuracion_empresa', 'Configuracion\EmpresaController@configuracionEmpresa')->name('Crear.Configuracion_Empresa');
Route::get('/configuracion_empresa_con', 'Configuracion\EmpresaController@datatableConfiguracion')->name('Consultar.Configuracion_Empresa');
Route::post('/configuracion_empresa_post', 'Configuracion\EmpresaController@graba_configuracion')->name('Configuracion.Empresa_Post');
 */ /**
 * Configuracion Emsy Punto de Emsion

Route::get('/configuracion_empresa', 'Configuracion\EmpresaController@configuracionEmpresa')->name('Crear.Configuracion_Empresa');
Route::get('/configuracion_empresa_con', 'Configuracion\EmpresaController@datatableConfiguracion')->name('Consultar.Configuracion_Empresa');
Route::post('/configuracion_empresa_post', 'Configuracion\EmpresaController@graba_configuracion')->name('Configuracion.Empresa_Post');
 */
/**
 * Crear Cliente

Route::get('/crear_cliente', 'Generales\ClienteController@indexCliente')->name('Crear.Cliente_view');
Route::post('/crear_cliente_post', 'Generales\ClienteController@trx_cliente')->name('Crear.Cliente_Post');
Route::get('/buscar_cliente', 'Generales\ClienteController@buscarCliente')->name('Buscar.Cliente');
Route::get('/lista_clientes', 'Generales\ClienteController@ListaCliente')->name('Lista.Cliente');
 */
/**
 * Crear Producto

Route::get('/crear_producto', 'Productos\ProductosController@indexProducto')->name('Crear.Producto_view');
Route::post('/crear_producto_post', 'Productos\ProductosController@trx_producto')->name('Crear.Producto_Post');
Route::get('/buscar_producto', 'Productos\ProductosController@buscarProducto')->name('Buscar.Producto');
Route::get('/lista_producto', 'Productos\ProductosController@ListaProductos')->name('Lista.Productos');
 */


/**
 *  Facturacion
 */
Route::get('/crear_factura', 'Facturacion\FacturacionController@indexFactura')->name('Crear.Factura_view');
Route::get('/con_json_empresas_fact', 'Facturacion\FacturacionController@buscarEmpresa')->name('Consultar.Lista_Empresa_fact');
Route::get('/con_punto_emision', 'Facturacion\FacturacionController@buscarPuntoEmision')->name('Consultar.PuntoEmision');
Route::post('/crear_factura_post', 'Facturacion\FacturacionController@trx_factura')->name('Crear.Factura_Post');
Route::get('/revisar_facturas', 'Facturacion\FacturacionController@consultarFacturaView')->name('consultar.Facturas_view');
Route::get('/revisar_facturas_get', 'Facturacion\FacturacionController@consultarFacturaGet')->name('consultar.Facturas_get');
Route::get('/revisar_facturas_detalle', 'Facturacion\FacturacionController@consultarFacturaDetalle')->name('consultar.Facturas_Detalle');
//Route::get('/buscar_factura', 'Productos\ProductosController@buscarFactura')->name('Buscar.Factura');
// Route::get('/lista_producto', 'Productos\ProductosController@ListaProductos')->name('Lista.Productos');


/**
 * Login
 */
Route::get('/login', 'Admin\PantillaController@login')->middleware('guest')->name('login');
Route::post('/validacion-iniciar-sesion', 'Sesion\IniciarSesionController@iniciar_sesion')->name('IniciarSesion');

/**
 *
 * Cerrar sesion
 */
Route::get('/cerrar', 'Sesion\IniciarSesionController@Cerrar_Logout')->name('salir');


Route::middleware(['auth', 'administrativo'])->group(function () {

    Route::get('/', 'Admin\VistaAdministrativoController@index');


    /**
     * Lista Empresa
     */
    Route::get('/empresa', 'Configuracion\EmpresaController@indexEmpresa')->name('Empresa');
    Route::get('/con_json_empresas', 'Configuracion\EmpresaController@datatableEmpresasJson')->name('Consultar.Lista_Empresa');


    /**
     * Crear Empresas
     */
    Route::get('/crear_empresa', 'Configuracion\EmpresaController@crearEmpresa')->name('Crear.Empresa');
    Route::post('/crear_empresa_post', 'Configuracion\EmpresaController@graba_empresa')->name('Crear.Empresa_Post');
    Route::post('/modificar_empresa_post', 'Configuracion\EmpresaController@modificar_empresa')->name('Modificar.Empresa_Post');


    /**
     * Crear Producto
     */
    Route::get('/crear_producto', 'Productos\ProductosController@indexProducto')->name('Crear.Producto_view');
    Route::post('/crear_producto_post', 'Productos\ProductosController@trx_producto')->name('Crear.Producto_Post');
    Route::get('/buscar_producto', 'Productos\ProductosController@buscarProducto')->name('Buscar.Producto');
    Route::get('/lista_producto', 'Productos\ProductosController@ListaProductos')->name('Lista.Productos');

    /*
    * Configuracion Emsy Punto de Emsion
    */
    Route::get('/configuracion_empresa', 'Configuracion\EmpresaController@configuracionEmpresa')->name('Crear.Configuracion_Empresa');
    Route::get('/configuracion_empresa_con', 'Configuracion\EmpresaController@datatableConfiguracion')->name('Consultar.Configuracion_Empresa');
    Route::post('/configuracion_empresa_post', 'Configuracion\EmpresaController@graba_configuracion')->name('Configuracion.Empresa_Post');

    /**
     * Crear Cliente
     */
    Route::get('/crear_cliente', 'Generales\ClienteController@indexCliente')->name('Crear.Cliente_view');
    Route::post('/crear_cliente_post', 'Generales\ClienteController@trx_cliente')->name('Crear.Cliente_Post');
    Route::get('/buscar_cliente', 'Generales\ClienteController@buscarCliente')->name('Buscar.Cliente');
    Route::get('/lista_clientes', 'Generales\ClienteController@ListaCliente')->name('Lista.Cliente');
    Route::get('/buscar_cliente_modal', 'Generales\ClienteController@buscarCliente_modal')->name('Buscar.ClienteModal');

    Route::get('/panel-administrativo', 'Admin\VistaAdministrativoController@index')->name('PanelAdministrativo');
});


Route::middleware(['auth', 'estandar'])->group(function () {
    Route::get('/', 'Admin\VistaAdministrativoController@index');

    Route::get('/panel-estandar', 'Admin\VistaEstandarController@index')->name('PanelEstandar');
});
