$(document).ready(function() {

    $('#btn-enviar-form').click(function () {
        let formulario  = $('#form-inicar-sesion').serialize();

        $.ajax({
            type: 'POST',
            url:  $('#form-inicar-sesion').attr("action"),
            data: formulario,
           // dataType: "dataType",
            beforeSend : function (){
                $('#load-login').removeClass('d-none');
                $('#btn-login').addClass('d-none');
            },
            success: function ( response ) {
                location.reload();
            },
            error:  function ( error ) {
                let errores = error.responseJSON.errors;

                if( errores.hasOwnProperty('email')){
                    alert(errores['email'][0]);
                }else if ( errores.hasOwnProperty ('password')){
                    alert(errores['password'][0]);
                }else{
                    alert(errores['login'][0]);
                }
            },
            complete : function(){
                $('#load-login').addClass('d-none');
                $('#btn-login').removeClass('d-none');
            }
        });

    });


});



