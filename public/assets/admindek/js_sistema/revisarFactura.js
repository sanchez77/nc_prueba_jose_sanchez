$(document).ready(function () {

    $('#btn-buscar-cliente').click(function () {
        let dat_busq = $('#inp-buscar-cliente').val();
        let opc = dat_busq === '' ? 'AC' : 'AD';
        $.ajax({
            type: 'GET',//?opcion=AC&dato_busq=
            url: $('#rt-buscar-cliente').attr("action") + '?opcion=' + opc + '&dato_busq=' + dat_busq,
            // data: {
            //     opcion: dat_busq === '' ? 'AC' : 'AD',
            //     dato_busq: dat_busq
            // },
            // dataType: "dataType",
            beforeSend: function () {
                $('.loaders').removeClass('d-none');
                $('#load-grb').removeClass('d-none');
                $('#btn-grb-empresa-form').addClass('d-none');
            },
            success: function (response) {

                if (response === 0) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Alerta!',
                        text: 'No Exite Registro!'
                        //footer: '<a href>Why do I have this issue?</a>'
                    });
                } else {
                    tab_clinete = new FancyGrid.get('tb-personas');
                    tab_clinete.setData(response);
                    tab_clinete.update();
                    tab_clinete.show();
                }


            },
            // error:  function ( error ) {
            //     let errores = error.responseJSON.errors;

            //     if( errores.hasOwnProperty('email')){
            //         alert(errores['email'][0]);
            //     }else if ( errores.hasOwnProperty ('password')){
            //         alert(errores['password'][0]);
            //     }else{
            //         alert(errores['login'][0]);
            //     }
            // },
            complete: function () {
                $('.loaders').addClass('d-none');
                $('#load-grb').addClass('d-none');
                $('#btn-grb-empresa-form').removeClass('d-none');
            }
        });


    });


    $('#tb-personas').each(function (i, el) {
        var $tb_list_empresas = new FancyGrid({
            window: true,
            modal: true,
            selModel: 'row',
            emptyText: 'No tienes Personas',
            renderTo: 'tb-personas',
            theme: 'bootstrap',
            height: 'fit',
            width: 'fit',
            paging: {
                cls: 'grid-busqueda-pagin'
            },
            lang: {
                loadingText: 'Cargando ...',
                paging: {
                    of: 'de [0]',
                    info: 'mostranto [0] - [1] de [2]',
                    page: 'pagina'
                }
            },
            title: {
                text: 'Lista de Personas',
                cls: 'my-title',
                tools: [
                    {
                        type: 'button',
                        cls: 'btn-blue',
                        enableToggle: true,
                        text: '<a title="Cerrar"><i class="fa fa-window-close fa-lg" style="color:#6466b5; width:6; height:6;"></i></a>',
                        style: {
                            'width': 'auto',
                            'height': 'auto',
                            'padding': '13px 12px'
                        },
                        handler: function (grid, o) {

                            $tb_list_empresas.hide();

                        }
                    }
                ]
            },
            defaults: {
                type: 'string',
                sortable: true
            },
            clicksToEdit: 1,
            data: [],
            // {
            //     proxy: {
            //         //read: 'GET',
            //         url: $('#dt-list-empresas-get').attr("action")
            //         // url: certus.oregien() + '/CtrNominaCargo',
            //         // params: {
            //         //     opcion: 'COOpcConsultaNominaCargos',
            //         //     combodepartamento :''
            //         // }}
            //     }
            // },



            events: [],
            columns: [

                {
                    index: 'id_cliente',
                    title: 'Cod Cliente',
                    editable: true,
                    hidden: true,
                    width: 0,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'id_tip_doc',
                    title: 'Cod Doc',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'num_doc',
                    title: 'Num Doc',
                    editable: false,
                    width: 140,
                    hidden: false,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'nombres',
                    editable: false,
                    title: 'Nombres',
                    hidden: false,
                    width: 80,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'apellidos',
                    title: 'Apellidos',
                    editable: false,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'razon_comercial',
                    title: 'Razon Comercial',
                    editable: false,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'correo',
                    title: 'Correo',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'fecha_nacimiento',
                    title: 'Fech Nac',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'edad',
                    title: 'Edad',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'telefono',
                    title: 'Telf',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'celular',
                    title: 'Celular',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'estado',
                    title: 'Telf',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'direccion',
                    title: 'Direccion',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    title: '',
                    locked: true,
                    cellAlign: 'center',
                    align: 'center',
                    type: 'action',
                    width: 35,
                    items: [
                        {
                            text: '<a title="Seleccionar"><i class="fa fa-check-circle"></i></a>',
                            cls: 'action-column-opciones',
                            tip: 'Guardar',
                            handler: function (grid, o) {

                                //$('.loaders').removeClass('d-none');

                                $('#inp-buscar-cliente').attr('cod_cliente', o.data.id_cliente);
                                $('#inp-buscar-cliente').attr('num_documento', o.data.num_doc);
                                $('#inp-buscar-cliente').val(o.data.num_doc);
                                //$('#inp-buscar-cliente').val(o.data.nombres + ' ' + o.data.apellidos);



                                $tb_list_empresas.removeAll();
                                $tb_list_empresas.hide();

                                //$('.loaders').addClass('d-none');

                            }
                        }
                    ]
                }

            ]
        });
    });



    $('#btn-buscar-facturas-cab').click(function () {

        let inp_secuencia = $('#inp_num_doc').val();
        let inp_fecha_desde = $('#inp_fecha_desde').val();
        let inp_fecha_hasta = $('#inp_fecha_hasta').val();


        let inp_dato_cliente = $('#inp-buscar-cliente').val();
        let cod_cliente = $('#inp-buscar-cliente').attr('cod_cliente');


        $.ajax({
            type: 'GET',//?opcion=AC&dato_busq=
            url: $('#rt-buscar-facturas-cab').attr("action"),
            data: {
                opcion: 'AA',
                fecha_desde: inp_fecha_desde,
                fecha_hasta: inp_fecha_hasta,
                secuencia: inp_secuencia,
                dato_cliente: inp_dato_cliente //cod_cliente === 0 ? inp_dato_cliente:cod_cliente

            },
            // dataType: "dataType",
            beforeSend: function () {
                $('.loaders').removeClass('d-none');
                // $('#load-grb').removeClass('d-none');
                // $('#btn-grb-empresa-form').addClass('d-none');
            },
            success: function (response) {


                if (response === 0) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Alerta!',
                        text: 'No Exite Registro!'
                        //footer: '<a href>Why do I have this issue?</a>'
                    });
                } else {
                    // console.log(response);
                    tab_cab_fact = new FancyGrid.get('tb-cabecera-facturas');
                    tab_cab_fact.setData(response);
                    tab_cab_fact.update();
                    tab_cab_fact.show();
                }


            },
            // error:  function ( error ) {
            //     let errores = error.responseJSON.errors;

            //     if( errores.hasOwnProperty('email')){
            //         alert(errores['email'][0]);
            //     }else if ( errores.hasOwnProperty ('password')){
            //         alert(errores['password'][0]);
            //     }else{
            //         alert(errores['login'][0]);
            //     }
            // },
            complete: function () {
                $('.loaders').addClass('d-none');
                // $('#load-grb').addClass('d-none');
                // $('#btn-grb-empresa-form').removeClass('d-none');
            }
        });


    });





    $('#tb-cabecera-facturas').each(function (i, el) {
        var $tb_list_empresas = new FancyGrid({
            selModel: 'row',
            emptyText: 'No tienes Facturas',
            renderTo: 'tb-cabecera-facturas',
            theme: 'bootstrap',
            height: 'fit',
            trackOver: true,
            title: {
                text: 'Lista de Facturas',
                cls: 'my-title',
                tools: [
                ]
            },
            defaults: {
                type: 'string',
                sortable: true
            },
            clicksToEdit: 1,
            data: [],
            events: [],
            columns: [

                {
                    index: 'id_fact',
                    title: 'ID FACT',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'id_empresa',
                    title: 'ID empresa',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'id_establecimiento',
                    title: 'ID ESTABLE',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'id_punto_emsion',
                    title: 'ID PUNT EMISI',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'razon_social',
                    title: 'RAZON SOCIAL',
                    editable: false,
                    width: 140,
                    hidden: false

                }, {
                    index: 'd_establecimiento',
                    title: 'ESTABLECIMIENTO',
                    editable: false,
                    width: 140,
                    hidden: false
                }, {
                    index: 'd_punto_emsion',
                    title: 'PUNTO EMISION',
                    editable: false,
                    width: 140,
                    hidden: false
                }, {
                    index: 'secuencia',
                    title: 'SECUENCIAL',
                    editable: false,
                    width: 100,
                    hidden: false,
                    render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                        if (o.data.secuencia !== 0) {
                            o.style = {
                                'background': 'rgb(255, 182, 77)'
                            };
                        }
                        return o;
                    }
                }, {
                    index: 'fecha_emision',
                    title: 'FECHA EMISION',
                    editable: false,
                    hidden: false,
                    width: 120,
                    render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();

                        o.style = {
                            'background': 'rgb(255, 182, 77)'
                        }
                        return o;
                    }
                }, {
                    index: 'id_cliente',
                    title: 'id cliente',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'nombres',
                    editable: false,
                    title: 'Nombres',
                    hidden: false,
                    width: 80,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'apellidos',
                    title: 'Apellidos',
                    editable: false,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'num_doc',
                    title: 'DOCUMENTO',
                    editable: false,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'correo',
                    title: 'CORREO',
                    editable: false,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'comentario',
                    title: 'NOTA',
                    editable: false,
                    hidden: false,
                    width: 120
                }, {
                    index: 'forma_de_pago',
                    title: 'FORMA DE PAGO',
                    editable: false,
                    hidden: false,
                    width: 120
                }, {
                    index: 'subtotal',
                    title: 'SUBTOTAL',
                    editable: false,
                    hidden: false,
                    width: 120
                }, {
                    index: 'descuento',
                    title: 'DESCUENTO',
                    editable: false,
                    hidden: false,
                    width: 120
                }, {
                    index: 'iva',
                    title: 'IVA',
                    editable: false,
                    hidden: false,
                    width: 120
                }, {
                    index: 'total',
                    title: 'TOTAL',
                    editable: false,
                    hidden: false,
                    width: 120,
                    render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();

                        o.style = {
                            'background': 'rgb(255, 182, 77)'
                        }
                        return o;
                    }
                }, {
                    index: 'direccion',
                    title: 'direccion',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    title: '',
                    locked: true,
                    cellAlign: 'center',
                    align: 'center',
                    type: 'action',
                    width: 35,
                    items: [
                        {
                            text: '<a title="VER DETALLE"><i class="fa fa-eye"></i></a>',
                            cls: 'action-column-opciones',
                            tip: 'Guardar',
                            handler: function (grid, o) {


                                console.log(o.data);
                                $.ajax({
                                    type: 'GET',//?opcion=AC&dato_busq=
                                    url: $('#rt-buscar-facturas-detalle').attr("action"),
                                    data: {

                                        opcion: 'AA',
                                        id_factura: o.data.id_fact,
                                        secuencia: o.data.secuencia,
                                        id_establecimiento: o.data.id_establecimiento,
                                        id_punto_emsion: o.data.id_punto_emsion

                                    },
                                    // dataType: "dataType",
                                    beforeSend: function () {
                                        $('.loaders').removeClass('d-none');
                                        // $('#load-grb').removeClass('d-none');
                                        // $('#btn-grb-empresa-form').addClass('d-none');
                                    },
                                    success: function (response) {
                                       // console.log(response);

                                        if (response === 0) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Alerta!',
                                                text: 'No Exite Registro!'
                                                //footer: '<a href>Why do I have this issue?</a>'
                                            });
                                        } else {
                                           // console.log(response);
                                            tab_det_fact = new FancyGrid.get('tb-detalle-factura-view');
                                            tab_det_fact.setData(response);
                                            tab_det_fact.update();
                                            tab_det_fact.show();
                                        }


                                    },
                                    // error:  function ( error ) {
                                    //     let errores = error.responseJSON.errors;

                                    //     if( errores.hasOwnProperty('email')){
                                    //         alert(errores['email'][0]);
                                    //     }else if ( errores.hasOwnProperty ('password')){
                                    //         alert(errores['password'][0]);
                                    //     }else{
                                    //         alert(errores['login'][0]);
                                    //     }
                                    // },
                                    complete: function () {
                                        $('.loaders').addClass('d-none');
                                        // $('#load-grb').addClass('d-none');
                                        // $('#btn-grb-empresa-form').removeClass('d-none');
                                    }
                                });

                                // $tb_list_empresas.removeAll();
                                // $tb_list_empresas.hide();

                                // $('.loaders').addClass('d-none');

                            }
                        }
                    ]
                }, {
                    title: '',
                    locked: true,
                    cellAlign: 'center',
                    align: 'center',
                    type: 'action',
                    width: 35,
                    items: [
                        {
                            text: '<a title="REPORTE"><i class="fa fa-file-pdf-o"></i></a>',
                            cls: 'action-column-opciones',
                            tip: 'Guardar',
                            handler: function (grid, o) {

                                //$('.loaders').removeClass('d-none');

                                $('#inp-buscar-cliente').attr('cod_cliente', o.data.id_cliente);
                                $('#inp-buscar-cliente').attr('num_documento', o.data.num_doc);
                                $('#inp-buscar-cliente').val(o.data.num_doc);
                                //$('#inp-buscar-cliente').val(o.data.nombres + ' ' + o.data.apellidos);



                                // $tb_list_empresas.removeAll();
                                // $tb_list_empresas.hide();

                                //$('.loaders').addClass('d-none');

                            }
                        }
                    ]
                }

            ]
        });
    });



    $('#tb-detalle-factura-view').each(function (i, el) {
        var $tb_list_detalle = new FancyGrid({
            selModel: 'row',
            emptyText: 'No tienes detalle',
            renderTo: 'tb-detalle-factura-view',
            theme: 'bootstrap',
            height: 'fit',
            trackOver: true,
            title: {
                text: 'Detalle de factura seleccionada',
                cls: 'my-title',
                tools: [
                ]
            },
            defaults: {
                type: 'string',
                sortable: true
            },
            clicksToEdit: 1,
            data: [],
            events: [],
            columns: [

                {
                    index: 'id_det_fact',
                    title: 'ID FACT DET',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'cod_prod',
                    title: 'COD PRODUCTO',
                    editable: false,
                    width: 140,
                    hidden: false

                }, {
                    index: 'secuencia',
                    title: 'SECUENCIA',
                    editable: false,
                    width: 80,
                    hidden: false
                },{
                    index: 'cantidad',
                    title: 'CANTIDAD',
                    editable: false,
                    width: 140,
                    hidden: false
                }, {
                    index: 'precio_unitario',
                    title: 'PVP',
                    editable: false,
                    width: 140,
                    hidden: false
                }, {
                    index: 'subtotal_prod',
                    title: 'SUBTOTAL',
                    editable: false,
                    width: 100,
                    hidden: false,
                    render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();

                        o.style = {
                            'background': 'rgb(255, 182, 77)'
                        };

                        return o;
                    }
                }, {
                    index: 'desct_prod_porcent',
                    title: 'DESCT(%)',
                    editable: false,
                    hidden: false,
                    width: 120,
                    render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();

                        o.style = {
                            'background': 'rgb(255, 182, 77)'
                        }
                        return o;
                    }
                }, {
                    index: 'desct_prod_valor',
                    title: 'DESCUENTO',
                    editable: false,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    },
                    render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();

                        o.style = {
                            'background': 'rgb(255, 182, 77)'
                        }
                        return o;
                    }
                }, {
                    index: 'total_prod',
                    title: 'TOTAL',
                    editable: false,
                    hidden: false,
                    width: 120,
                    render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();

                        o.style = {
                            'background': 'rgb(255, 182, 77)'
                        }
                        return o;
                    }
                }

            ]
        });
    });



});
