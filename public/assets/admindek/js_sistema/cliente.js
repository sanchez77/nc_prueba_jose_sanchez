$(document).ready(function () {


    $('#btn-grb-cliente-form').click(function () {
        let formulario = $('#form-guadar-empresa').serialize();

        let inp_id_cliente = $('#inp_num_doc').attr('cod_cliente');
        let inp_id_tip_doc = $('#select_tipo_doc').val();
        let inp_num_doc = $('#inp_num_doc').val();
        let inp_nombres = $('#inp_nombre').val();
        let inp_apellidos = $('#inp_apellidos').val();
        let inp_razon_comercial = $('#inp_razon_comercial').val();
        let inp_correo = $('#inp_email').val();
        let inp_fecha_nacimiento = $('#inp_fecha_nacimiento').val();
        let inp_edad = $('#inp_edad').val();
        let inp_telefono = $('#inp_telefono').val();
        let inp_celular = $('#inp_celular').val();
        let inp_direccion = $('#inp_direccion').val();
        let inp_estado = $('#select_estado').val();


        let msj = '';


        parseInt(inp_id_cliente) === 0 ? msj = 'GUARDAR' : msj = 'ACTUALIZAR';

        Swal.fire({
            title: '<strong>' + msj + '</strong>',
            icon: 'info',
            html:
                'Confirmar...',
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText:
                '<i class="icofont icofont-check-circled"></i>',
            confirmButtonAriaLabel: 'Thumbs up, great!',
            cancelButtonText:
                '<i class="fa fa-times"></i>',
            cancelButtonAriaLabel: 'Thumbs down'
        }).then((result) => {
            if (result.value === true) {
                $.ajax({
                    type: 'POST',
                    url: $('#form-cliente-empresa').attr("action"),
                    data: {
                        _token: $('#token').val(),
                        opcion: parseInt(inp_id_cliente) === 0 ? 'AA' : 'AB',
                        id_cliente: parseInt(inp_id_cliente),
                        id_tip_doc: inp_id_tip_doc,
                        num_doc: inp_num_doc,
                        nombres: inp_nombres,
                        apellidos: inp_apellidos,
                        razon_comercial: inp_razon_comercial,
                        correo: inp_correo,
                        fecha_nacimiento: inp_fecha_nacimiento,
                        edad: inp_edad,
                        telefono: inp_telefono,
                        celular: inp_celular,
                        direccion: inp_direccion,
                        estado: inp_estado,
                    },
                    // dataType: "dataType",
                    beforeSend: function () {
                        $('.loaders').removeClass('d-none');
                        $('#load-grb').removeClass('d-none');
                        $('#btn-grb-cliente-form').addClass('d-none');
                    },
                    success: function (response) {


                        let cod_validacion = response[0].out_cod;
                        let cod_reg_auto = response[0].id_cliente;
                        let msj = response[0].out_msj;
                        let num_doc = response[0].out_num_doc;

                        if (cod_validacion === 7) {

                            Swal.fire({
                                icon: 'success',
                                title: msj,
                                text: 'Numero de Documento: ' + num_doc,
                                showConfirmButton: false,
                                allowOutsideClick: false,
                                closeOnClickOutside: false,
                                timer: 3000
                            });
                            setTimeout('document.location.reload()', 3000);


                            location.reload();
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: msj,
                                text: 'Numero de Documento: ' + num_doc,

                            });
                        }
                    },
                    // error:  function ( error ) {
                    //     let errores = error.responseJSON.errors;

                    //     if( errores.hasOwnProperty('email')){
                    //         alert(errores['email'][0]);
                    //     }else if ( errores.hasOwnProperty ('password')){
                    //         alert(errores['password'][0]);
                    //     }else{
                    //         alert(errores['login'][0]);
                    //     }
                    // },
                    complete: function () {
                        $('.loaders').addClass('d-none');
                        $('#load-grb').addClass('d-none');
                        $('#btn-grb-cliente-form').removeClass('d-none');
                    }
                });
            }
        })
    });


    $('#btn-buscar-cliente').click(function () {
        let dat_busq =  $('#inp-buscar-cliente').val();
        $.ajax({
            type: 'GET',
            url: $('#rt-buscar-cliente').attr("action"),
            data: {
                opcion: dat_busq === '' ? 'AA' : 'AB',
                num_doc: dat_busq
            },
            // dataType: "dataType",
            // beforeSend: function () {
            //     $('.loaders').removeClass('d-none');
            //     $('#load-grb').removeClass('d-none');
            //     $('#btn-grb-empresa-form').addClass('d-none');
            // },
            success: function (response) {

                if (response === 0) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Alerta!',
                        text: 'No Exite Registro!'
                        //footer: '<a href>Why do I have this issue?</a>'
                    });
                } else {
                    tab_clinete = new FancyGrid.get('tb-personas');
                    tab_clinete.setData(response);
                    tab_clinete.update();
                    tab_clinete.show();
                }


                // let cod_validacion = response[0].out_cod;
                // let cod_reg_auto = response[0].ID_REG;
                // let msj = response[0].out_id_reg;
                // let ruc = response[0].p_in_ruc;

                // if (cod_validacion === 7) {

                //     Swal.fire({
                //         icon: 'success',
                //         title: msj,
                //         text: 'RUC: ' + ruc,
                //         showConfirmButton: false,
                //         allowOutsideClick: false,
                //         closeOnClickOutside: false,
                //         timer: 6000
                //     });
                //     setTimeout('document.location.reload()', 5000);


                //     // location.reload();
                // } else {
                //     Swal.fire({
                //         icon: 'error',
                //         title: msj,
                //         text: 'RUC: ' + ruc,

                //     });
                // }
            },
            // error:  function ( error ) {
            //     let errores = error.responseJSON.errors;

            //     if( errores.hasOwnProperty('email')){
            //         alert(errores['email'][0]);
            //     }else if ( errores.hasOwnProperty ('password')){
            //         alert(errores['password'][0]);
            //     }else{
            //         alert(errores['login'][0]);
            //     }
            // },
            // complete: function () {
            //     $('.loaders').addClass('d-none');
            //     $('#load-grb').addClass('d-none');
            //     $('#btn-grb-empresa-form').removeClass('d-none');
            // }
        });
    });



    $('#tb-personas').each(function (i, el) {
        var $tb_list_empresas = new FancyGrid({
            window: true,
            modal: true,
            selModel: 'row',
            emptyText: 'No tienes Personas',
            renderTo: 'tb-personas',
            theme: 'bootstrap',
            height: 'fit',
            width: 'fit',
            paging: {
                cls: 'grid-busqueda-pagin'
            },
            lang: {
                loadingText: 'Cargando ...',
                paging: {
                    of: 'de [0]',
                    info: 'mostranto [0] - [1] de [2]',
                    page: 'pagina'
                }
            },
            title: {
                text: 'Lista de Personas',
                cls: 'my-title',
                tools: [
                    {
                        type: 'button',
                        cls: 'btn-blue',
                        enableToggle: true,
                        text: '<a title="Cerrar"><i class="fa fa-window-close fa-lg" style="color:#6466b5; width:6; height:6;"></i></a>',
                        style: {
                            'width': 'auto',
                            'height': 'auto',
                            'padding': '13px 12px'
                        },
                        handler: function (grid, o) {

                            $tb_list_empresas.hide();

                        }
                    }
                ]
            },
            defaults: {
                type: 'string',
                sortable: true
            },
            clicksToEdit: 1,
            data: [],
            // {
            //     proxy: {
            //         //read: 'GET',
            //         url: $('#dt-list-empresas-get').attr("action")
            //         // url: certus.oregien() + '/CtrNominaCargo',
            //         // params: {
            //         //     opcion: 'COOpcConsultaNominaCargos',
            //         //     combodepartamento :''
            //         // }}
            //     }
            // },



            events: [],
            columns: [

                {
                    index: 'id_cliente',
                    title: 'Cod Cliente',
                    editable: true,
                    hidden: true,
                    width: 0,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'id_tip_doc',
                    title: 'Cod Doc',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'num_doc',
                    title: 'Num Doc',
                    editable: false,
                    width:140,
                    hidden: false,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'nombres',
                    editable: false,
                    title: 'Nombres',
                    hidden: false,
                    width: 80,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'apellidos',
                    title: 'Apellidos',
                    editable: false,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'razon_comercial',
                    title: 'Razon Comercial',
                    editable: false,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'correo',
                    title: 'Correo',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'fecha_nacimiento',
                    title: 'Fech Nac',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'edad',
                    title: 'Edad',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'telefono',
                    title: 'Telf',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'celular',
                    title: 'Celular',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'estado',
                    title: 'Telf',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'direccion',
                    title: 'Direccion',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    title: '',
                    locked: true,
                    cellAlign: 'center',
                    align: 'center',
                    type: 'action',
                    width: 35,
                    items: [
                        {
                            text: '<a title="Seleccionar"><i class="fa fa-check-circle"></i></a>',
                            cls: 'action-column-opciones',
                            tip: 'Guardar',
                            handler: function (grid, o) {

                                //$('.loaders').removeClass('d-none');

                                $('#inp_num_doc').attr('cod_cliente', o.data.id_cliente);
                                $('#select_tipo_doc').val(o.data.id_tip_doc).change();
                                $('#inp_num_doc').val(o.data.num_doc);
                                $('#inp_nombre').val(o.data.nombres);
                                $('#inp_apellidos').val(o.data.apellidos);
                                $('#inp_razon_comercial').val(o.data.razon_comercial);
                                $('#inp_email').val(o.data.correo);
                                $('#inp_fecha_nacimiento').val(o.data.fecha_nacimiento);
                                $('#inp_edad').val(o.data.edad);
                                $('#inp_telefono').val(o.data.telefono);
                                $('#inp_celular').val(o.data.celular);
                                $('#inp_direccion').val(o.data.direccion);
                                $('#select_estado').val(o.data.estado).change();

                                $tb_list_empresas.removeAll();
                                $tb_list_empresas.hide();

                                //$('.loaders').addClass('d-none');

                            }
                        }
                    ]
                }

            ]
        });
    });


});
