
$(document).ready(function () {

    $('#tb-list-empresas').each(function (i, el) {
        var $tb_list_empresas = new FancyGrid({
            selModel: 'row',
            window: true,
            modal: true,
            width: 'fit',
            emptyText: 'No tienes Empresas',
            renderTo: 'tb-list-empresas',
            theme: 'bootstrap',
            height: 'fit',
            paging: {
                cls: 'grid-busqueda-pagin'
            },
            lang: {
                loadingText: 'Cargando ...',
                paging: {
                    of: 'de [0]',
                    info: 'mostranto [0] - [1] de [2]',
                    page: 'pagina'
                }
            },
            title: {
                text: 'Lista de Empresas',
                cls: 'my-title',
                tools: [
                    {
                        type: 'button',
                        cls: 'btn-blue',
                        enableToggle: true,
                        text: '<a title="Cerrar"><i class="fa fa-window-close fa-lg" style="color:#6466b5; width:6; height:6;"></i></a>',
                        style: {
                            'width': 'auto',
                            'height': 'auto',
                            'padding': '13px 12px'
                        },
                        handler: function (grid, o) {

                            $tb_list_empresas.hide();

                        }
                    }
                ]
            },
            defaults: {
                type: 'string',
                sortable: true
            },
            clicksToEdit: 1,
            data: [],
            //  {
            //     proxy: {
            //         //read: 'GET',
            //         url: $('#dt-list-empresas-get').attr("action")
            //         // url: certus.oregien() + '/CtrNominaCargo',
            //         // params: {
            //         //     opcion: 'COOpcConsultaNominaCargos',
            //         //     combodepartamento :''
            //         // }}
            //     }
            // },

            events: [],
            columns: [
                {
                    index: 'estado',
                    title: 'Estado',
                    editable: true,
                    hidden: true,
                    width: 0,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'id_rgt',
                    title: 'ID',
                    editable: false,
                    width: 0,
                    hidden: false,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'id_empresa',
                    editable: false,
                    title: 'Cod Empresa',
                    hidden: true,
                    width: 0,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'ruc',
                    title: 'RUC',
                    editable: false,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'razon_social',
                    title: 'Razon Social',
                    editable: false,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'razon_comercial',
                    title: 'Razon Comercial',
                    editable: false,
                    hidden: true,
                    width: 0,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'correo',
                    title: 'Correo',
                    editable: true,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'direccion',
                    title: 'Direccion',
                    editable: true,
                    hidden: true,
                    width: 0,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'iva',
                    title: 'IVA',
                    editable: true,
                    hidden: true,
                    width: 0,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    title: '',
                    locked: true,
                    cellAlign: 'center',
                    align: 'center',
                    type: 'action',
                    width: 35,
                    items: [
                        {
                            text: '<a title="Seleccionar"><i class="fa fa-edit"></i></a>',
                            cls: 'action-column-opciones',
                            tip: 'Guardar',
                            handler: function (grid, o) {

                                $('#inp_empresa').val(o.data.razon_social);
                                $('#inp_empresa').attr('cod_empresa', o.data.id_empresa);
                                $tb_list_empresas.hide();
                                let $codigo = o.data.id_empresa;

                                $.ajax({
                                    type: 'GET',
                                    url: $('#buscar-punto-emsion-get').attr("action") + '?opcion=AA&id_cod_empresa=' + $codigo + '&otro',
                                    success: function (response) {


                                        if (response === 0) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Alerta!',
                                                text: 'No Exite Configuracion para la empresa seleccionada!'

                                            });
                                        } else {
                                            //console.log(response);//[0].d_establecimiento);
                                            $('#inp_establecimiento').val(response[0].d_establecimiento);
                                            $('#inp_establecimiento').attr('id_estab', response[0].id_establecimiento);
                                            $('#inp_punto_emsion').val(response[0].d_punto_emsion);
                                            $('#inp_punto_emsion').attr('id_punto_emision', response[0].id_punto_emsion);
                                            $('#inpt_secuencial').val(response[0].secuencia);
                                            $('#inpt_secuencial').attr('id_rgt', response[0].id_rgt);//de tabla configura


                                            var fecha = new Date(); //Fecha actual
                                            var mes = fecha.getMonth() + 1; //obteniendo mes
                                            var dia = fecha.getDate(); //obteniendo dia
                                            var ano = fecha.getFullYear(); //obteniendo año
                                            if (dia < 10)
                                                dia = '0' + dia; //agrega cero si el menor de 10
                                            if (mes < 10)
                                                mes = '0' + mes //agrega cero si el menor de 10
                                            document.getElementById('inp_fecha').value = ano + "-" + mes + "-" + dia;



                                        }

                                    }
                                });


                            }
                        }
                    ]
                }

            ]
        });
    });





    $('#btn-buscar-empresas').click(function () {
        let dat_busq = $('#inp_empresa').val();
        $.ajax({
            type: 'GET',
            url: $('#dt-list-empresas-get').attr("action"),
            data: {
                opcion: dat_busq === '' ? 'AC' : 'AB',
                num_doc: dat_busq
            },
            // dataType: "dataType",
            // beforeSend: function () {
            //     $('.loaders').removeClass('d-none');
            //     $('#load-grb').removeClass('d-none');
            //     $('#btn-grb-empresa-form').addClass('d-none');
            // },
            success: function (response) {

                if (response === 0) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Alerta!',
                        text: 'No Exite Registro!'
                        //footer: '<a href>Why do I have this issue?</a>'
                    });
                } else {
                    tab_empresas = new FancyGrid.get('tb-list-empresas');
                    tab_empresas.setData(response);
                    tab_empresas.update();
                    tab_empresas.show();
                }

            },

        });
    });



    $('#btn-buscar-cliente').click(function () {
        let dat_busq = $('#inp-buscar-cliente').val();
        $.ajax({
            type: 'GET',
            url: $('#rt-buscar-cliente').attr("action"),
            data: {
                opcion: dat_busq === '' ? 'AA' : 'AB',
                num_doc: dat_busq
            },
            // dataType: "dataType",
            // beforeSend: function () {
            //     $('.loaders').removeClass('d-none');
            //     $('#load-grb').removeClass('d-none');
            //     $('#btn-grb-empresa-form').addClass('d-none');
            // },
            success: function (response) {

                if (response === 0) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Alerta!',
                        text: 'No Exite Registro!'
                        //footer: '<a href>Why do I have this issue?</a>'
                    });
                } else {
                    tab_clinete = new FancyGrid.get('tb-personas');
                    tab_clinete.setData(response);
                    tab_clinete.update();
                    tab_clinete.show();
                }


                // let cod_validacion = response[0].out_cod;
                // let cod_reg_auto = response[0].ID_REG;
                // let msj = response[0].out_id_reg;
                // let ruc = response[0].p_in_ruc;

                // if (cod_validacion === 7) {

                //     Swal.fire({
                //         icon: 'success',
                //         title: msj,
                //         text: 'RUC: ' + ruc,
                //         showConfirmButton: false,
                //         allowOutsideClick: false,
                //         closeOnClickOutside: false,
                //         timer: 6000
                //     });
                //     setTimeout('document.location.reload()', 5000);


                //     // location.reload();
                // } else {
                //     Swal.fire({
                //         icon: 'error',
                //         title: msj,
                //         text: 'RUC: ' + ruc,

                //     });
                // }
            },
            // error:  function ( error ) {
            //     let errores = error.responseJSON.errors;

            //     if( errores.hasOwnProperty('email')){
            //         alert(errores['email'][0]);
            //     }else if ( errores.hasOwnProperty ('password')){
            //         alert(errores['password'][0]);
            //     }else{
            //         alert(errores['login'][0]);
            //     }
            // },
            // complete: function () {
            //     $('.loaders').addClass('d-none');
            //     $('#load-grb').addClass('d-none');
            //     $('#btn-grb-empresa-form').removeClass('d-none');
            // }
        });
    });



    $('#tb-personas').each(function (i, el) {
        var $tb_list_empresas = new FancyGrid({
            window: true,
            modal: true,
            selModel: 'row',
            emptyText: 'No tienes Personas',
            renderTo: 'tb-personas',
            theme: 'bootstrap',
            height: 'fit',
            width: 'fit',
            paging: {
                cls: 'grid-busqueda-pagin'
            },
            lang: {
                loadingText: 'Cargando ...',
                paging: {
                    of: 'de [0]',
                    info: 'mostranto [0] - [1] de [2]',
                    page: 'pagina'
                }
            },
            title: {
                text: 'Lista de Personas',
                cls: 'my-title',
                tools: [
                    {
                        type: 'button',
                        cls: 'btn-blue',
                        enableToggle: true,
                        text: '<a title="Cerrar"><i class="fa fa-window-close fa-lg" style="color:#6466b5; width:6; height:6;"></i></a>',
                        style: {
                            'width': 'auto',
                            'height': 'auto',
                            'padding': '13px 12px'
                        },
                        handler: function (grid, o) {

                            $tb_list_empresas.hide();

                        }
                    }
                ]
            },
            defaults: {
                type: 'string',
                sortable: true
            },
            clicksToEdit: 1,
            data: [],
            // {
            //     proxy: {
            //         //read: 'GET',
            //         url: $('#dt-list-empresas-get').attr("action")
            //         // url: certus.oregien() + '/CtrNominaCargo',
            //         // params: {
            //         //     opcion: 'COOpcConsultaNominaCargos',
            //         //     combodepartamento :''
            //         // }}
            //     }
            // },



            events: [],
            columns: [

                {
                    index: 'id_cliente',
                    title: 'Cod Cliente',
                    editable: true,
                    hidden: true,
                    width: 0,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'id_tip_doc',
                    title: 'Cod Doc',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'num_doc',
                    title: 'Num Doc',
                    editable: false,
                    width: 140,
                    hidden: false,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'nombres',
                    editable: false,
                    title: 'Nombres',
                    hidden: false,
                    width: 80,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'apellidos',
                    title: 'Apellidos',
                    editable: false,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'razon_comercial',
                    title: 'Razon Comercial',
                    editable: false,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'correo',
                    title: 'Correo',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'fecha_nacimiento',
                    title: 'Fech Nac',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'edad',
                    title: 'Edad',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'telefono',
                    title: 'Telf',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'celular',
                    title: 'Celular',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'estado',
                    title: 'Telf',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'direccion',
                    title: 'Direccion',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    title: '',
                    locked: true,
                    cellAlign: 'center',
                    align: 'center',
                    type: 'action',
                    width: 35,
                    items: [
                        {
                            text: '<a title="Seleccionar"><i class="fa fa-check-circle"></i></a>',
                            cls: 'action-column-opciones',
                            tip: 'Guardar',
                            handler: function (grid, o) {

                                //$('.loaders').removeClass('d-none');

                                $('#inp-buscar-cliente').attr('cod_cliente', o.data.id_cliente);

                                $('#inp_num_doc').val(o.data.num_doc);
                                $('#inp-buscar-cliente').val(o.data.nombres + ' ' + o.data.apellidos);
                                $('#inp_razon_comercial').val(o.data.razon_comercial);
                                $('#inp_email').val(o.data.correo);
                                $('#inp_direccion').val(o.data.direccion);


                                $tb_list_empresas.removeAll();
                                $tb_list_empresas.hide();

                                //$('.loaders').addClass('d-none');

                            }
                        }
                    ]
                }

            ]
        });
    });


    $('#tb-producto-modal').each(function (i, el) {
        var $tb_list_productos = new FancyGrid({
            window: true,
            modal: true,
            selModel: 'row',
            emptyText: 'No tienes Productos',
            renderTo: 'tb-producto-modal',
            theme: 'bootstrap',
            height: 'fit',
            width: 'fit',
            paging: {
                cls: 'grid-busqueda-pagin'
            },
            lang: {
                loadingText: 'Cargando ...',
                paging: {
                    of: 'de [0]',
                    info: 'mostranto [0] - [1] de [2]',
                    page: 'pagina'
                }
            },
            title: {
                text: 'Lista de Productos',
                cls: 'my-title',
                tools: [
                    {
                        type: 'button',
                        cls: 'btn-blue',
                        enableToggle: true,
                        text: '<a title="Cerrar"><i class="fa fa-window-close fa-lg" style="color:#6466b5; width:6; height:6;"></i></a>',
                        style: {
                            'width': 'auto',
                            'height': 'auto',
                            'padding': '13px 12px'
                        },
                        handler: function (grid, o) {

                            $tb_list_productos.hide();

                        }
                    }
                ]
            },
            defaults: {
                type: 'string',
                sortable: true
            },
            clicksToEdit: 1,
            data: [],
            // {
            //     proxy: {
            //         //read: 'GET',
            //         url: $('#dt-list-empresas-get').attr("action")
            //         // url: certus.oregien() + '/CtrNominaCargo',
            //         // params: {
            //         //     opcion: 'COOpcConsultaNominaCargos',
            //         //     combodepartamento :''
            //         // }}
            //     }
            // },



            events: [],
            columns: [

                {
                    index: 'id_product',
                    title: 'Cod Prodc',
                    editable: false,
                    hidden: true,
                    width: 0,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'cod_prod',
                    title: 'Cod Producto',
                    editable: false,
                    hidden: false,
                    width: 200
                }, {
                    index: 'descripcion',
                    title: 'Description',
                    editable: false,
                    width: 140,
                    hidden: false,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'precio_unitario',
                    editable: false,
                    title: 'PVP Unit',
                    hidden: false,
                    width: 100,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'stock',
                    editable: false,
                    title: 'Stock',
                    hidden: false,
                    width: 100,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'estado',
                    title: 'Estado',
                    editable: false,
                    hidden: false,
                    width: 80,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    title: '',
                    locked: true,
                    cellAlign: 'center',
                    align: 'center',
                    type: 'action',
                    width: 35,
                    items: [
                        {
                            text: '<a title="Seleccionar"><i class="fa fa-check-circle" style="color:#6466b5; width:6; height:6;"></i></a>',
                            cls: 'action-column-opciones',
                            tip: 'Guardar',
                            handler: function (grid, o) {

                                //console.log(o.data);



                                tab_det_fact = new FancyGrid.get('tb-detalle-factura');//.getData();

                                tab_det_fact.insert( {
                                    // id_rgt_det : 0,
                                    id_rgt_det: 0,
                                    id_product: o.item.data.id_product,
                                    cod_prod: o.item.data.cod_prod,
                                    cantidad: 0,
                                    precio_unitario: o.item.data.precio_unitario,
                                    subtotal_prod: 0,
                                    desct_prod_porcent: 0,
                                    desct_prod_valor: 0,
                                    total_prod: 0
                                });
                                $tb_list_productos.hide();
                                $tb_list_productos.removeAll();

                                //$('.loaders').addClass('d-none');

                            }
                        }
                    ]
                }

            ]
        });
    });


    function buscar_modal_producto($tb_detalle_fact) {
        let dat_busq = $('#inp_codigo_producto').val();
        $.ajax({
            type: 'GET',
            url: $('#rt-buscar-producto').attr("action"),
            data: {
                opcion: 'AC',//dat_busq === '' ? 'AA' : 'AB',
                cod_prod: dat_busq
            },
            // dataType: "dataType",
            beforeSend: function () {
                $('.loaders').removeClass('d-none');
                // $('#load-grb').removeClass('d-none');
                // $('#btn-grb-empresa-form').addClass('d-none');
            },
            success: function (response) {

                if (response === 0) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Alerta!',
                        text: 'No Exite Registro!'
                        //footer: '<a href>Why do I have this issue?</a>'
                    });
                } else {
                    tab_producto = new FancyGrid.get('tb-producto-modal');
                    tab_producto.setData(response);
                    tab_producto.update();
                    tab_producto.show();
                }


            }, complete: function () {
                $('.loaders').addClass('d-none');
                // $('#load-grb').addClass('d-none');
                // $('#btn-grb-empresa-form').removeClass('d-none');
            }
        });
    };


    $('#tb-detalle-factura').each(function (i, el) {
        var $tb_detalle_fact = new FancyGrid({
            selModel: 'row',
            emptyText: 'No tienes Cargos',
            renderTo: 'tb-detalle-factura',
            theme: 'bootstrap',
            height: 'fit',

            title: {
                text: 'Configuracion Establecimineto-Punto de emision-Secuencial',
                cls: 'my-title',
                tools: [
                    {
                        type: 'button',
                            cls: 'btn-blue',
                            enableToggle: true,
                        text: '<a title="Buscar"><i class="fa fa-search "  style="color:#6466b5; width:6; height:6;"></i></a>',
                        cls: 'action-column-opciones',
                        tip: 'Buscar',
                        handler: function (grid, o) {

                            buscar_modal_producto($tb_detalle_fact);
                        }
                    }
                    // {
                    //     type: 'button',
                    //     cls: 'btn-blue',
                    //     enableToggle: true,
                    //     text: '<a title="Agregar"><i class="fa fa-plus-square fa-lg" style="color:#6466b5; width:6; height:6;"></i></a>',
                    //     style: {
                    //         'width': 'auto',
                    //         'height': 'auto',
                    //         'padding': '3px 12px'
                    //     },
                    //     handler: function (grid, o) {



                    //         $tb_detalle_fact.insert(0, {
                    //             id_rgt_det: 0,
                    //             estado: 'A'
                    //         });

                    //     }
                    // }
                ]
            },
            defaults: {
                type: 'string',
                sortable: true
            },
            clicksToEdit: 1,
            data: [],
            //  {
            //     proxy: {
            //         //read: 'GET',
            //         url: url_conf
            //         // url: certus.oregien() + '/CtrNominaCargo',
            //         // params: {
            //         //     opcion: 'COOpcConsultaNominaCargos',
            //         //     combodepartamento :''
            //         // }}
            //     }
            // },

            // cantidad: "12"
            // cod_prod: "cell001"
            // descuento_prod_porcent: 0
            // descuento_prod_valor: 0
            // id: 1116
            // id_product: 1
            // id_rgt_det: 0
            // precio_unitario: "152.36"
            // subtotal_prod: 0
            // total_prod: 0

            events: [{
                    set: function (grid, o) {
                       // console.log(o.item.data);
                        if (o.key === 'cantidad') {
                            var $cantidad = o.item.data.cantidad === 'undefined' ? 0 : o.item.data.cantidad ;
                            var $poscentaje_descuento = o.item.data.descuento_prod_valor === 'undefined' ? 0 : o.item.data.descuento_prod_valor;
                            var $pvp_unit = o.item.data.precio_unitario === 'undefined' ? 0 : o.item.data.precio_unitario ;


                            var  sub_total_items = $cantidad * $pvp_unit ;




                              //  console.log(grid.setById);
                                $tb_detalle_fact.setById(o.item.data.id_rgt_det, {
                                // id_rgt_det : 0,
                                id_rgt_det: 0,
                                id_product: o.item.data.id_product,
                                cod_prod: o.item.data.cod_prod,
                                cantidad: $cantidad,
                                precio_unitario: o.item.data.precio_unitario,
                                subtotal_prod: sub_total_items.toFixed(2),
                                descuento_prod_porcent: 0,
                                descuento_prod_valor: 0,
                                total_prod:sub_total_items
                            });
                            $tb_detalle_fact.update();


                            //sub_total_items = parseInt(cantidad)*parseFloat(pvp_unit);
                            // console.log($cantidad);
                            // console.log($poscentaje_descuento);
                            // console.log($pvp);

                        }
                    }
                }
            ],
            //   [{
            //     load: function(grid, o){

            //         if(grid.getTotal() === 0){
            //             grid.setHeight(400);
            //         }

            //     }
            // }],
            columns: [
                {
                    index: 'id_rgt_det',
                    title: 'ID',
                    editable: true,
                    width: 40,
                    hidden: false,
                    render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                        if (o.data.id_rgt_det === 0) {
                            o.style = {
                                'background': 'rgb(255, 182, 77)'
                            };
                        } else {
                            o.style = {
                                'background': ''
                            };
                        }
                        return o;
                    }
                }, {
                    index: 'id_product',
                    title: 'ID PROD',
                    editable: true,
                    width: 60,
                    hidden: false,
                    // render: function (o) {
                    //     // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                    //     if (o.data.id_rgt_det === 0) {
                    //         o.style = {
                    //             'background': 'rgb(255, 182, 77)'
                    //         };
                    //     } else {
                    //         o.style = {
                    //             'background': ''
                    //         };
                    //     }
                    //     return o;
                    // }
                }, {
                    index: 'cod_prod',
                    editable: false,
                    title: 'Cod Producto',
                    hidden: false,
                    width: 80,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                    // , render: function (o) {
                    //     // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                    //     if (o.data.id_rgt_det === 0) {
                    //         o.style = {
                    //             'background': 'rgb(255, 182, 77)'
                    //         };
                    //     } else {
                    //         o.style = {
                    //             'background': ''
                    //         };
                    //     }
                    //     return o;
                    // }
                }, {
                    index: 'cantidad',
                    title: 'Cantidad',
                    editable: true,
                    hidden: false,
                    width: 120
                    // ,
                    // render: function (o) {
                    //     // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                    //     if (o.data.id_rgt_det === 0) {
                    //         o.style = {
                    //             'background': 'rgb(255, 182, 77)'
                    //         };
                    //     } else {
                    //         o.style = {
                    //             'background': ''
                    //         };
                    //     }
                    //     return o;
                    // }
                }, {
                    index: 'precio_unitario',
                    title: 'PVP',
                    editable: true,
                    hidden: false,
                    width: 120
                    // ,
                    // render: function (o) {
                    //     // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                    //     if (o.data.id_rgt_det === 0) {
                    //         o.style = {
                    //             'background': 'rgb(255, 182, 77)'
                    //         };
                    //     } else {
                    //         o.style = {
                    //             'background': ''
                    //         };
                    //     }
                    //     return o;
                    // }
                }, {
                    index: 'subtotal_prod',
                    title: 'SUBTOTAL',
                    editable: true,
                    hidden: false,
                    width: 120
                    // ,
                    // render: function (o) {
                    //     // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                    //     if (o.data.id_rgt_det === 0) {
                    //         o.style = {
                    //             'background': 'rgb(255, 182, 77)'
                    //         };
                    //     } else {
                    //         o.style = {
                    //             'background': ''
                    //         };
                    //     }
                    //     return o;
                    // }
                }, {
                    index: 'desct_prod_porcent',
                    title: 'DESCT(%)',
                    editable: true,
                    hidden: false,
                    width: 120
                    // ,
                    // render: function (o) {
                    //     // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                    //     if (o.data.id_rgt_det === 0) {
                    //         o.style = {
                    //             'background': 'rgb(255, 182, 77)'
                    //         };
                    //     } else {
                    //         o.style = {
                    //             'background': ''
                    //         };
                    //     }
                    //     return o;
                    // }
                }, {
                    index: 'desct_prod_valor',
                    title: 'Descuento',
                    editable: true,
                    hidden: false,
                    width: 120
                    // ,
                    // render: function (o) {
                    //     // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                    //     if (o.data.id_rgt_det === 0) {
                    //         o.style = {
                    //             'background': 'rgb(255, 182, 77)'
                    //         };
                    //     } else {
                    //         o.style = {
                    //             'background': ''
                    //         };
                    //     }
                    //     return o;
                    // }
                }, {
                    index: 'total_prod',
                    title: 'Total',
                    editable: true,
                    hidden: false,
                    width: 120
                    // ,
                    // render: function (o) {
                    //     // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                    //     if (o.data.id_rgt_det === 0) {
                    //         o.style = {
                    //             'background': 'rgb(255, 182, 77)'
                    //         };
                    //     } else {
                    //         o.style = {
                    //             'background': ''
                    //         };
                    //     }
                    //     return o;
                    // }
                },
                {
                    title: '',
                    locked: true,
                    cellAlign: 'center',
                    align: 'center',
                    type: 'action',
                    width: 40,
                    items: [
                        // {
                        //     text: '<a title="Buscar"><i class="fa fa-search m-1"></i></a>',
                        //     cls: 'action-column-opciones',
                        //     tip: 'Buscar',
                        //     handler: function (grid, o) {

                        //         buscar_modal_producto($tb_detalle_fact);
                        //     }
                        // },
                         {
                            text: '<a title="Borrar"><i class="fa fa-times-circle"></i></a>',
                            cls: 'action-column-opciones',
                            tip: 'Borrar',
                            handler: function (grid, o) {

                                $tb_detalle_fact.removeRow(0);
                            }
                        }
                    ]
                }

            ]
        });
    });



    $('#btn-grb-factura-form').click(function () {
        //let formulario = $('#form-guadar-empresa').serialize();


        let inp_nombre_empresa = $('#inp_empresa').val();
        let cod_empresa = $('#inp_empresa').attr('cod_empresa');

        let descrip_establecimiento = $('#inp_establecimiento').val();
        let id_establecimiento = $('#inp_establecimiento').attr('id_estab');
        let descrip_punto_emsion = $('#inp_punto_emsion').val();
        let id_punto_emsion = $('#inp_punto_emsion').attr('id_punto_emision');
        let secuencia = $('#inpt_secuencial').val();
        let id_rgt_confg = $('#inpt_secuencial').attr('id_rgt');//de tabla configura
        let fecha = $('#inp_fecha').val();

        let id_cliente = $('#inp-buscar-cliente').attr('cod_cliente');
        let inp_num_doc = $('#inp_num_doc').val();

        let inp_nota = $('#inp_nota').val();
        let forma_pag = $('#select_tipo_doc').val();

        let inp_sub_total = $('#inp_sub_total').val();
        let inp_descuento = $('#inp_descuento').val();
        let inp_iva = $('#inp_iva').val();
        let inp_total = $('#inp_total').val();

        let  tab_det_fact = new FancyGrid.get('tb-detalle-factura').getData();



        let msj = 'Procesar Factura';



        Swal.fire({
            title: '<strong>' + msj + '</strong>',
            icon: 'info',
            html:
                'Confirmar...',
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText:
                '<i class="icofont icofont-check-circled"></i>',
            confirmButtonAriaLabel: 'Thumbs up, great!',
            cancelButtonText:
                '<i class="fa fa-times"></i>',
            cancelButtonAriaLabel: 'Thumbs down'
        }).then((result) => {
            if (result.value === true) {
                $.ajax({
                    type: 'POST',
                    url: $('#form-factura').attr("action"),
                    data: {
                        _token: $('#token').val(),
                        opcion: 'AA',
                        id_fact: 0,
                        id_empresa: cod_empresa,
                        tipo_doc: 'FACT',
                        id_rgt_conf: id_rgt_confg,
                        id_establecimiento: id_establecimiento,
                        id_punto_emsion: id_punto_emsion,
                        fecha_emision: fecha,
                        secuencia: secuencia,
                        id_cliente: id_cliente,
                        comentario: inp_nota,
                        forma_de_pago: forma_pag,
                        subtotal: inp_sub_total,
                        iva: inp_iva,
                        descuento: inp_descuento,
                        total: inp_total,
                        tabla_detalle : JSON.stringify(tab_det_fact),
                    },
                    // dataType: "dataType",
                    beforeSend: function () {
                        $('.loaders').removeClass('d-none');
                        $('#load-grb').removeClass('d-none');
                        $('#btn-grb-producto-form').addClass('d-none');
                    },
                    success: function (response) {



                        let cod_validacion = response[0].out_cod;
                        let cod_reg_auto = response[0].out_id_fact;
                        let msj = response[0].out_msj;
                        let cod_proct = response[0].out_secuencia;

                        if (cod_validacion === 7) {

                            Swal.fire({
                                icon: 'success',
                                title: msj,
                                text: 'FACTURA #: ' + cod_proct,
                                showConfirmButton: false,
                                allowOutsideClick: false,
                                closeOnClickOutside: false,
                                timer: 3000
                            });
                            setTimeout('document.location.reload()', 3000);


                            location.reload();
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: msj,
                                text: 'FACTURA #: ' + cod_proct,

                            });
                        }
                    },

                    complete: function () {
                        $('.loaders').addClass('d-none');
                        $('#load-grb').addClass('d-none');
                        $('#btn-grb-producto-form').removeClass('d-none');
                    }
                });
            }
        })
    });



});
