$(document).ready(function () {


    $('#btn-grb-empresa-form').click(function () {
        let formulario = $('#form-guadar-empresa').serialize();
        $.ajax({
            type: 'POST',
            url: $('#form-guadar-empresa').attr("action"),
            data: formulario,
            // dataType: "dataType",
            beforeSend: function () {
                $('.loaders').removeClass('d-none');
                $('#load-grb').removeClass('d-none');
                $('#btn-grb-empresa-form').addClass('d-none');
            },
            success: function (response) {


                let cod_validacion = response[0].out_cod;
                let cod_reg_auto = response[0].ID_REG;
                let msj = response[0].out_id_reg;
                let ruc = response[0].p_in_ruc;

                if (cod_validacion === 7) {

                    Swal.fire({
                        icon: 'success',
                        title: msj,
                        text: 'RUC: ' + ruc,
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        closeOnClickOutside: false,
                        timer: 6000
                    });
                    setTimeout('document.location.reload()', 5000);


                    // location.reload();
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: msj,
                        text: 'RUC: ' + ruc,

                    });
                }
            },
            // error:  function ( error ) {
            //     let errores = error.responseJSON.errors;

            //     if( errores.hasOwnProperty('email')){
            //         alert(errores['email'][0]);
            //     }else if ( errores.hasOwnProperty ('password')){
            //         alert(errores['password'][0]);
            //     }else{
            //         alert(errores['login'][0]);
            //     }
            // },
            complete: function () {
                $('.loaders').addClass('d-none');
                $('#load-grb').addClass('d-none');
                $('#btn-grb-empresa-form').removeClass('d-none');
            }
        });

    });


    /**
     * @autor: jose Sanchez
     * @descripbe: tabla para la visualizacion de las empresas
     */

    $('#tb-list-empresas').each(function (i, el) {
        var $tb_list_empresas = new FancyGrid({
            selModel: 'row',
            emptyText: 'No tienes Empresas',
            renderTo: 'tb-list-empresas',
            theme: 'bootstrap',
            height: 'fit',
            paging: {
                cls: 'grid-busqueda-pagin'
            },
            lang: {
                loadingText: 'Cargando ...',
                paging: {
                    of: 'de [0]',
                    info: 'mostranto [0] - [1] de [2]',
                    page: 'pagina'
                }
            },
            title: {
                text: 'Lista de Empresas',
                cls: 'my-title',
                tools: [
                    // {
                    //     type: 'button',
                    //     cls: 'btn-blue',
                    //     enableToggle: true,
                    //     text: '<a title="Agregar"><i class="fa fa-plus-square fa-lg" style="color:#6466b5; width:6; height:6;"></i></a>',
                    //     style: {
                    //         'width': 'auto',
                    //         'height': 'auto',
                    //         'padding': '3px 12px'
                    //     },
                    //     handler: function (grid, o) {

                    //         // $tb_confg.firstPage();
                    //         // $tb_confg.insert(0, {
                    //         //     id_rgt: 0,
                    //         //     estado: 'A'
                    //         // });


                    //     }
                    // }
                ]
            },
            defaults: {
                type: 'string',
                sortable: true
            },
            clicksToEdit: 1,
            data: {
                proxy: {
                    //read: 'GET',
                    url: $('#dt-list-empresas-get').attr("action")
                    // url: certus.oregien() + '/CtrNominaCargo',
                    // params: {
                    //     opcion: 'COOpcConsultaNominaCargos',
                    //     combodepartamento :''
                    // }}
                }
            },



            events: [],
            columns: [
                {
                    index: 'estado',
                    title: 'Estado',
                    editable: true,
                    hidden: false,
                    width: 100,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'id_rgt',
                    title: 'ID',
                    editable: false,
                    width: 40,
                    hidden: false,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'id_empresa',
                    editable: false,
                    title: 'Cod Empresa',
                    hidden: false,
                    width: 80,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'ruc',
                    title: 'RUC',
                    editable: false,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'razon_social',
                    title: 'Razon Social',
                    editable: false,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'razon_comercial',
                    title: 'Razon Comercial',
                    editable: false,
                    hidden: false,
                    width: 220,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'correo',
                    title: 'Correo',
                    editable: true,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'direccion',
                    title: 'Direccion',
                    editable: true,
                    hidden: false,
                    width: 220,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'iva',
                    title: 'IVA',
                    editable: true,
                    hidden: false,
                    width: 100,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    title: '',
                    locked: true,
                    cellAlign: 'center',
                    align: 'center',
                    type: 'action',
                    width: 35,
                    items: [
                        {
                            text: '<a title="Guardar"><i class="fa fa-edit"></i></a>',
                            cls: 'action-column-opciones',
                            tip: 'Guardar',
                            handler: function (grid, o) {

                                let msj = '';
                                let id_reg = o.data.id_rgt;

                                msj = 'Desea realizar el proceso?';

                                Swal.fire({
                                    title: '<strong>' + msj + '</strong>',
                                    icon: 'info',
                                    html:
                                        'Confirmar...',
                                    showCloseButton: true,
                                    showCancelButton: true,
                                    focusConfirm: false,
                                    confirmButtonText:
                                        '<i class="icofont icofont-check-circled"></i>',
                                    confirmButtonAriaLabel: 'Thumbs up, great!',
                                    cancelButtonText:
                                        '<i class="fa fa-times"></i>',
                                    cancelButtonAriaLabel: 'Thumbs down'
                                }).then((result) => {
                                    if (result.value === true) {

                                        //e.preventDefault();

                                        $.ajax({
                                            type: 'POST',
                                            url: $('#dt-empresa-post').attr("action"),
                                            data: {
                                                _token: $('#token').val(),
                                                opcion: 'AB',
                                                p_in_id_empresa: o.data.id_empresa,
                                                p_in_id_rgt: o.data.id_rgt,
                                                inp_direccion: o.data.direccion,
                                                inp_email: o.data.correo,
                                                inp_iva: o.data.iva,
                                                inp_razon_comercial: o.data.razon_comercial,
                                                inp_razon_social: o.data.razon_social,
                                                inp_ruc: o.data.ruc,
                                                select_estado: o.data.estado
                                            },
                                            beforeSend: function () {
                                                $('.loaders').removeClass('d-none');
                                            },
                                            success: function (response) {

                                                let cod_validacion = response[0].out_cod;
                                                let cod_reg_auto = response[0].ID_REG;
                                                let msj = response[0].out_id_reg;
                                                let ruc = response[0].p_in_ruc;

                                                if (cod_validacion === 7) {
                                                    $tb_list_empresas.load();
                                                    Swal.fire({
                                                        icon: 'success',
                                                        title: msj,
                                                        text: 'RUC: ' + ruc,
                                                        showConfirmButton: false,
                                                        allowOutsideClick: false,
                                                        closeOnClickOutside: false,
                                                        timer: 1000
                                                    });
                                                    //  setTimeout('document.location.reload()',5000);


                                                    // location.reload();
                                                } else {
                                                    Swal.fire({
                                                        icon: 'error',
                                                        title: msj,
                                                        text: 'RUC: ' + ruc,

                                                    });
                                                }
                                            },
                                            // error:  function ( error ) {
                                            //     let errores = error.responseJSON.errors;

                                            //     if( errores.hasOwnProperty('email')){
                                            //         alert(errores['email'][0]);
                                            //     }else if ( errores.hasOwnProperty ('password')){
                                            //         alert(errores['password'][0]);
                                            //     }else{
                                            //         alert(errores['login'][0]);
                                            //     }
                                            // },
                                            complete: function () {
                                                $('.loaders').addClass('d-none');
                                            }
                                        });

                                    }
                                })

                            }
                        }
                    ]
                }

            ]
        });
    });



});



