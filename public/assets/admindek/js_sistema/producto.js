$(document).ready(function () {


    $('#btn-grb-producto-form').click(function () {
        //let formulario = $('#form-guadar-empresa').serialize();

        let cod_prod = $('#inp_codigo_producto').attr('cod_prod');
        let inp_codigo_producto = $('#inp_codigo_producto').val();
        let inp_descripcion = $('#inp_descripcion').val();
        let inp_stock = $('#inp_stock').val();
        let inp_precio_unitario = $('#inp_precio_unitario').val();
        let select_estado = $('#select_estado').val();



        let msj = '';

        parseInt(cod_prod) === 0 ? msj = 'GUARDAR' : msj = 'ACTUALIZAR';

        Swal.fire({
            title: '<strong>' + msj + '</strong>',
            icon: 'info',
            html:
                'Confirmar...',
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText:
                '<i class="icofont icofont-check-circled"></i>',
            confirmButtonAriaLabel: 'Thumbs up, great!',
            cancelButtonText:
                '<i class="fa fa-times"></i>',
            cancelButtonAriaLabel: 'Thumbs down'
        }).then((result) => {
            if (result.value === true) {
                $.ajax({
                    type: 'POST',
                    url: $('#form-producto').attr("action"),
                    data: {
                        _token: $('#token').val(),
                        opcion: parseInt(cod_prod) === 0 ? 'AA' : 'AB',
                        id_product: cod_prod,
                        cod_prod: inp_codigo_producto,
                        descripcion: inp_descripcion,
                        stock: inp_stock,
                        precio_unitario: inp_precio_unitario,
                        id_empresa: '0',
                        estado: select_estado
                    },
                    // dataType: "dataType",
                    beforeSend: function () {
                        $('.loaders').removeClass('d-none');
                        $('#load-grb').removeClass('d-none');
                        $('#btn-grb-producto-form').addClass('d-none');
                    },
                    success: function (response) {



                        let cod_validacion = response[0].out_cod;
                        let cod_reg_auto = response[0].out_id_aut;
                        let msj = response[0].out_msj;
                        let cod_proct = response[0].out_cod_proct;

                        if (cod_validacion === 7) {

                            Swal.fire({
                                icon: 'success',
                                title: msj,
                                text: 'Codigo Producto: ' + cod_proct,
                                showConfirmButton: false,
                                allowOutsideClick: false,
                                closeOnClickOutside: false,
                                timer: 3000
                            });
                            setTimeout('document.location.reload()', 3000);


                            location.reload();
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: msj,
                                text: 'Codigo Producto: ' + cod_proct,

                            });
                        }
                    },
                    // error:  function ( error ) {
                    //     let errores = error.responseJSON.errors;

                    //     if( errores.hasOwnProperty('email')){
                    //         alert(errores['email'][0]);
                    //     }else if ( errores.hasOwnProperty ('password')){
                    //         alert(errores['password'][0]);
                    //     }else{
                    //         alert(errores['login'][0]);
                    //     }
                    // },
                    complete: function () {
                        $('.loaders').addClass('d-none');
                        $('#load-grb').addClass('d-none');
                        $('#btn-grb-producto-form').removeClass('d-none');
                    }
                });
            }
        })
    });


    $('#btn-buscar-producto').click(function () {

        let dat_busq = $('#inp_codigo_producto').val();
        $.ajax({
            type: 'GET',
            url: $('#rt-buscar-producto').attr("action"),
            data: {
                opcion: dat_busq === '' ? 'AA' : 'AB',
                cod_prod: dat_busq
            },
            // dataType: "dataType",
            beforeSend: function () {
                $('.loaders').removeClass('d-none');
                // $('#load-grb').removeClass('d-none');
                // $('#btn-grb-empresa-form').addClass('d-none');
            },
            success: function (response) {

                if (response === 0) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Alerta!',
                        text: 'No Exite Registro!'
                        //footer: '<a href>Why do I have this issue?</a>'
                    });
                } else {
                    tab_producto = new FancyGrid.get('tb-producto');
                    tab_producto.setData(response);
                    tab_producto.update();
                    tab_producto.show();
                }


            }, complete: function () {
                $('.loaders').addClass('d-none');
                // $('#load-grb').addClass('d-none');
                // $('#btn-grb-empresa-form').removeClass('d-none');
            }

        });
    });




    $('#tb-producto').each(function (i, el) {
        var $tb_list_productos = new FancyGrid({
            window: true,
            modal: true,
            selModel: 'row',
            emptyText: 'No tienes Productos',
            renderTo: 'tb-producto',
            theme: 'bootstrap',
            height: 'fit',
            width: 'fit',
            paging: {
                cls: 'grid-busqueda-pagin'
            },
            lang: {
                loadingText: 'Cargando ...',
                paging: {
                    of: 'de [0]',
                    info: 'mostranto [0] - [1] de [2]',
                    page: 'pagina'
                }
            },
            title: {
                text: 'Lista de Productos',
                cls: 'my-title',
                tools: [
                    {
                        type: 'button',
                        cls: 'btn-blue',
                        enableToggle: true,
                        text: '<a title="Cerrar"><i class="fa fa-window-close fa-lg" style="color:#6466b5; width:6; height:6;"></i></a>',
                        style: {
                            'width': 'auto',
                            'height': 'auto',
                            'padding': '13px 12px'
                        },
                        handler: function (grid, o) {

                            $tb_list_productos.hide();

                        }
                    }
                ]
            },
            defaults: {
                type: 'string',
                sortable: true
            },
            clicksToEdit: 1,
            data: [],
            // {
            //     proxy: {
            //         //read: 'GET',
            //         url: $('#dt-list-empresas-get').attr("action")
            //         // url: certus.oregien() + '/CtrNominaCargo',
            //         // params: {
            //         //     opcion: 'COOpcConsultaNominaCargos',
            //         //     combodepartamento :''
            //         // }}
            //     }
            // },



            events: [],
            columns: [

                {
                    index: 'id_product',
                    title: 'Cod Prodc',
                    editable: false,
                    hidden: true,
                    width: 0,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'cod_prod',
                    title: 'Cod Producto',
                    editable: false,
                    hidden: false,
                    width: 200
                }, {
                    index: 'descripcion',
                    title: 'Description',
                    editable: false,
                    width: 140,
                    hidden: false,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'precio_unitario',
                    editable: false,
                    title: 'PVP Unit',
                    hidden: false,
                    width: 100,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'stock',
                    editable: false,
                    title: 'Stock',
                    hidden: false,
                    width: 100,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'estado',
                    title: 'Estado',
                    editable: false,
                    hidden: false,
                    width: 80,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    title: '',
                    locked: true,
                    cellAlign: 'center',
                    align: 'center',
                    type: 'action',
                    width: 35,
                    items: [
                        {
                            text: '<a title="Seleccionar"><i class="fa fa-check-circle"></i></a>',
                            cls: 'action-column-opciones',
                            tip: 'Guardar',
                            handler: function (grid, o) {

                                //$('.loaders').removeClass('d-none');

                                $('#inp_codigo_producto').attr('cod_prod', o.data.id_product);
                                $('#inp_codigo_producto').val(o.data.cod_prod);
                                $('#inp_descripcion').val(o.data.descripcion);
                                $('#inp_stock').val(o.data.stock);
                                $('#inp_precio_unitario').val(o.data.precio_unitario);
                                $('#select_estado').val(o.data.estado).change();

                                $tb_list_productos.removeAll();
                                $tb_list_productos.hide();

                                //$('.loaders').addClass('d-none');

                            }
                        }
                    ]
                }

            ]
        });
    });


});
