var editor;
$(document).ready(function () {



    let url_conf = $('#dt-configuracion-get').attr("action");
    $('#tb-configuracion').each(function (i, el) {
        var $tb_confg = new FancyGrid({
            selModel: 'row',
            emptyText: 'No tienes Cargos',
            renderTo: 'tb-configuracion',
            theme: 'bootstrap',
            height: 'fit',
            paging: {
                cls: 'grid-busqueda-pagin'
            },
            lang: {
                loadingText: 'Cargando ...',
                paging: {
                    of: 'de [0]',
                    info: 'mostranto [0] - [1] de [2]',
                    page: 'pagina'
                }
            },
            title: {
                text: 'Configuracion Establecimineto-Punto de emision-Secuencial',
                cls: 'my-title',
                tools: [
                    {
                        type: 'button',
                        cls: 'btn-blue',
                        enableToggle: true,
                        text: '<a title="Agregar"><i class="fa fa-plus-square fa-lg" style="color:#6466b5; width:6; height:6;"></i></a>',
                        style: {
                            'width': 'auto',
                            'height': 'auto',
                            'padding': '3px 12px'
                        },
                        handler: function (grid, o) {

                            $tb_confg.firstPage();
                            $tb_confg.insert(0, {
                                id_rgt: 0,
                                estado: 'A'
                            });

                            // var $id_departamento = $('#combo_nm_departamento').val();

                            // var tam_table_cargos = $mant_cargos.getData().length;
                            // var valor = true;
                            // var mensaje = "";
                            // if (tam_table_cargos > 0) {
                            //     $mant_cargos.each(function (casillero) {
                            //         if (casillero.data.descripcion === "") {
                            //             mensaje = 'Campo Vacio en Cargo';
                            //             valor = false;
                            //             return false;
                            //         }
                            //         if (casillero.data.id_catalogo_estado === "") {
                            //             mensaje = 'Campo Vacio en Columna Estado';
                            //             valor = false;
                            //             return false;
                            //         }
                            //         if (casillero.data.id_cargo === 0) {
                            //             mensaje = 'Debe Guardar Primer Fila';
                            //             valor = false;
                            //             return true;
                            //         }
                            //     });
                            // }
                            // if (valor) {
                            //     $mant_cargos.firstPage();
                            //     $mant_cargos.insert(0, {
                            //         id_cargo: 0,
                            //         id_departamento: $id_departamento,
                            //         descripcion: '',
                            //         id_catalogo_estado: ''
                            //     });
                            // } else {
                            //     certus.alertas_sr(5, '', mensaje, '');
                            // }
                        }
                    }
                ]
            },
            defaults: {
                type: 'string',
                sortable: true
            },
            clicksToEdit: 1,
            data: {
                proxy: {
                    //read: 'GET',
                    url: url_conf
                    // url: certus.oregien() + '/CtrNominaCargo',
                    // params: {
                    //     opcion: 'COOpcConsultaNominaCargos',
                    //     combodepartamento :''
                    // }}
                }
            },



            events: [],
            //   [{
            //     load: function(grid, o){

            //         if(grid.getTotal() === 0){
            //             grid.setHeight(400);
            //         }

            //     }
            // }],
            columns: [
                {
                    index: 'id_rgt',
                    title: 'ID',
                    editable: false,
                    width: 40,
                    hidden: false,
                    filter: {
                        header: false,
                        headerNote: true
                    }, render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                        if (o.data.id_rgt === 0) {
                            o.style = {
                                'background': 'rgb(255, 182, 77)'
                            };
                        } else {
                            o.style = {
                                'background': ''
                            };
                        }
                        return o;
                    }
                }, {
                    index: 'id_empresa',
                    editable: true,
                    title: 'Cod Empresa',
                    hidden: false,
                    width: 80,
                    filter: {
                        header: false,
                        headerNote: true
                    }, render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                        if (o.data.id_rgt === 0) {
                            o.style = {
                                'background': 'rgb(255, 182, 77)'
                            };
                        } else {
                            o.style = {
                                'background': ''
                            };
                        }
                        return o;
                    }
                }, {
                    index: 'id_establecimiento',
                    title: 'Cod Establecimiento',
                    editable: true,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }, render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                        if (o.data.id_rgt === 0) {
                            o.style = {
                                'background': 'rgb(255, 182, 77)'
                            };
                        } else {
                            o.style = {
                                'background': ''
                            };
                        }
                        return o;
                    }
                }, {
                    index: 'd_establecimiento',
                    title: 'Despc Establecimiento',
                    editable: true,
                    hidden: false,
                    width: 220,
                    filter: {
                        header: false,
                        headerNote: true
                    }, render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                        if (o.data.id_rgt === 0) {
                            o.style = {
                                'background': 'rgb(255, 182, 77)'
                            };
                        } else {
                            o.style = {
                                'background': ''
                            };
                        }
                        return o;
                    }
                }, {
                    index: 'id_punto_emsion',
                    title: 'Cod Punt Emision',
                    editable: true,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }, render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                        if (o.data.id_rgt === 0) {
                            o.style = {
                                'background': 'rgb(255, 182, 77)'
                            };
                        } else {
                            o.style = {
                                'background': ''
                            };
                        }
                        return o;
                    }
                }, {
                    index: 'd_punto_emsion',
                    title: 'Despc Punt Emision',
                    editable: true,
                    hidden: false,
                    width: 220,
                    filter: {
                        header: false,
                        headerNote: true
                    }, render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                        if (o.data.id_rgt === 0) {
                            o.style = {
                                'background': 'rgb(255, 182, 77)'
                            };
                        } else {
                            o.style = {
                                'background': ''
                            };
                        }
                        return o;
                    }
                }, {
                    index: 'secuencia',
                    title: 'Secuencia',
                    editable: true,
                    hidden: false,
                    width: 100,
                    filter: {
                        header: false,
                        headerNote: true
                    }, render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                        if (o.data.id_rgt === 0) {
                            o.style = {
                                'background': 'rgb(255, 182, 77)'
                            };
                        } else {
                            o.style = {
                                'background': ''
                            };
                        }
                        return o;
                    }
                }, {
                    index: 'estado',
                    title: 'Estado',
                    editable: true,
                    hidden: false,
                    width: 100,
                    filter: {
                        header: false,
                        headerNote: true
                    }, render: function (o) {
                        // o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                        if (o.data.id_rgt === 0) {
                            o.style = {
                                'background': 'rgb(255, 182, 77)'
                            };
                        } else {
                            o.style = {
                                'background': ''
                            };
                        }
                        return o;
                    }
                },

                // {
                //     index: 'descripcion',
                //     title: 'CARGO',
                //     flex: 2,
                //     //width: 450,
                //     editable: true,
                //     filter: {
                //         header: true,
                //         headerNote: true
                //     }, render: function (o) {
                //         o.value = typeof o.value === 'undefined' ? '' : o.value.toUpperCase();
                //         if (o.data.id_cargo === 0) {
                //             o.style = {
                //                 'background': 'rgb(255, 156, 156)'
                //             };
                //         } else {
                //             o.style = {
                //                 'background': ''
                //             };
                //         }
                //         return o;
                //     }
                // }, {
                //     index: 'id_catalogo_estado',
                //     editable: true,
                //     title: 'ESTADO',
                //     type: 'combo',
                //     flex: 1,
                //     displayKey: 'codigo_descripcion',
                //     valueKey: 'codigo_catalogo',
                //     multiSelect: true,
                //     itemCheckBox: true
                //     // data: {
                //     //     proxy: {
                //     //         method: 'POST',
                //     //         url: certus.oregien() + '/CtrGeneralNomina?opcion=COOpcComboCatalogoGeneralComboFancy&codigocatalgo=ESTADO',
                //     //         reader: {
                //     //             root: 'data'
                //     //         }
                //     //     }
                //     // }, render: function (o) {
                //     //     if (o.data.id_cargo === 0) {
                //     //         o.style = {
                //     //             'background': 'rgb(255, 156, 156)'
                //     //         };
                //     //     } else {
                //     //         o.style = {
                //     //             'background': ''
                //     //         };
                //     //     }
                //     //     return o;
                //     // }
                // },
                {
                    title: '',
                    locked: true,
                    cellAlign: 'center',
                    align: 'center',
                    type: 'action',
                    width: 35,
                    items: [
                        {
                            text: '<a title="Guardar"><i class="fa fa-edit"></i></a>',
                            cls: 'action-column-opciones',
                            tip: 'Guardar',
                            handler: function (grid, o) {

                                let msj = '';
                                let id_reg = o.data.id_rgt;

                                id_reg === 0 ? msj = 'GUARDAR' : msj = 'ACTUALIZAR';

                                Swal.fire({
                                    title: '<strong>' + msj + '</strong>',
                                    icon: 'info',
                                    html:
                                        'Confirmar...',
                                    showCloseButton: true,
                                    showCancelButton: true,
                                    focusConfirm: false,
                                    confirmButtonText:
                                        '<i class="icofont icofont-check-circled"></i>',
                                    confirmButtonAriaLabel: 'Thumbs up, great!',
                                    cancelButtonText:
                                        '<i class="fa fa-times"></i>',
                                    cancelButtonAriaLabel: 'Thumbs down'
                                }).then((result) => {
                                    if (result.value === true) {

                                        //e.preventDefault();

                                        // let form_conf = $('#form-guadar-configuracion').serialize();
                                        // let id_reg = $('#id_reg').val();
                                        // let id_empr = $('#id_empresa').val();

                                        $.ajax({
                                            type: 'POST',
                                            url: $('#dt-configuracion-post').attr("action"),
                                            data: {
                                                _token:  $('#token').val() ,
                                                opcion : id_reg === 0 ? 'AA' : 'AB',
                                                id_empr: o.data.id_empresa,
                                                id_reg: o.data.id_rgt,
                                                inp_cod_establecimiento: o.data.id_establecimiento,
                                                inp_cod_put_ems: o.data.id_punto_emsion,
                                                inp_establecimiento: o.data.d_establecimiento,
                                                inp_punto_emision: o.data.d_punto_emsion,
                                                inp_secuencia: o.data.secuencia,
                                                select_estado: o.data.estado
                                            },
                                            beforeSend: function () {
                                                $('.loaders').removeClass('d-none');
                                            },
                                            success: function (response) {

                                                let cod_reg_auto = response[0].ID_REG;
                                                let cod_validacion = response[0].out_cod;
                                                let msj = response[0].out_msj;
                                                let cod_establecimineto = response[0].out_id_establecimiento;
                                                let cod_punt_emis = response[0].out_id_punto_emsion;

                                                if(cod_validacion === 7){
                                                    $tb_confg.load();
                                                      Swal.fire({
                                                        icon: 'success',
                                                        title: msj,
                                                        text: 'Cod Establecimiento: '+cod_establecimineto,
                                                        showConfirmButton: false,
                                                        allowOutsideClick: false,
                                                        closeOnClickOutside: false,
                                                        timer: 2000
                                                        });
                                                      //  setTimeout('document.location.reload()',5000);


                                                     // location.reload();
                                                }else{
                                                    Swal.fire({
                                                        icon: 'error',
                                                        title: msj,
                                                        text: 'Cod Establecimiento: '+cod_establecimineto,

                                                      });
                                                }
                                            },
                                            // error:  function ( error ) {
                                            //     let errores = error.responseJSON.errors;

                                            //     if( errores.hasOwnProperty('email')){
                                            //         alert(errores['email'][0]);
                                            //     }else if ( errores.hasOwnProperty ('password')){
                                            //         alert(errores['password'][0]);
                                            //     }else{
                                            //         alert(errores['login'][0]);
                                            //     }
                                            // },
                                            complete: function () {
                                                $('.loaders').addClass('d-none');
                                            }
                                        });

                                        // $('.loaders').removeClass('d-none');
                                        // $.post( $('#dt-configuracion-post').attr("action"), {
                                        //     _token: $('meta[name="csrf_token"]').attr('content'), // obteniendo el valor
                                        //     id_empr: o.data.id_empresa,
                                        //     id_reg : o.data.id_rgt,
                                        //     inp_cod_establecimiento: o.data.id_establecimiento,
                                        //     inp_cod_put_ems: o.data.id_punto_emsion,
                                        //     inp_establecimiento: o.data.d_establecimiento,
                                        //     inp_punto_emision: o.data.d_punto_emsion,
                                        //     inp_secuencia: o.data.secuencia,
                                        //     select_estado: o.data.estado

                                        // }, function (data) {
                                        //     console.log(data);
                                        //     $('.loaders').addClass('d-none');

                                        // }).fail(function () { });

                                    }
                                })

                                // var $descripcion = o.data.descripcion;
                                // var $id_catalogo_estado = o.data.id_catalogo_estado;

                                // var $id_cargo = typeof o.data.id_cargo === 'undefined' ? 0 : o.data.id_cargo ;
                                // var $id_departamento = $('#combo_nm_departamento').val();
                                // var mensaje = "";
                                // if ($descripcion === "") {
                                //     $.confirm({
                                //         title: 'Alerta',
                                //         content: 'Debe ingresar descripcion',
                                //         type: 'blue',
                                //         typeAnimated: true,
                                //         theme: 'modern',
                                //         autoClose: 'cerrar|8000',
                                //         icon: 'fa fa-check-square-o',
                                //         buttons: {
                                //             cerrar: function () {
                                //             }
                                //         }
                                //     });
                                //     return;
                                // }
                                // if ($id_catalogo_estado === "") {
                                //     $.confirm({
                                //         title: 'Alerta',
                                //         content: 'Debe selecionar estado',
                                //         type: 'blue',
                                //         typeAnimated: true,
                                //         theme: 'modern',
                                //         autoClose: 'cerrar|8000',
                                //         icon: 'fa fa-check-square-o',
                                //         buttons: {
                                //             cerrar: function () {
                                //             }
                                //         }
                                //     });
                                //     return;
                                // }

                                // $.confirm({
                                //     title: ($id_cargo === 0) ? 'Guardar' : 'Actualizar',
                                //     content: 'Desea confirmar la transacción',
                                //     type: 'blue',
                                //     typeAnimated: true,
                                //     theme: 'modern',
                                //     icon: 'fa fa-exclamation-circle',
                                //     buttons: {
                                //         SI: function () {
                                //             $('body').loading();

                                //             $.post(certus.oregien() + '/CtrNominaCargo', {
                                //                 opcion: "COOpcGrbNmCargo",
                                //                 id_cargo: $id_cargo,
                                //                 id_departamento: $id_departamento,
                                //                 descripcion: o.data.descripcion,
                                //                 id_catalogo_estado: o.data.id_catalogo_estado

                                //             }, function (data) {

                                //                 if (data.cod === 7) {
                                //                     $mant_cargos.load();
                                //                     certus.alertas_sr(data.cod, '', data.desc, '');
                                //                     $('body').loading('stop');
                                //                 } else {
                                //                     certus.alertas_sr(data.cod, '', data.desc, '');
                                //                     $('body').loading('stop');
                                //                 }
                                //                 $('body').loading('stop');
                                //             }).fail(function () {

                                //                 $('body').loading('stop');
                                //                 if (navigator.onLine) {
                                //                     certus.alertas_sr(9, '', 'Verifique la información que envia a procesar e intentelo nuevamente.', '');
                                //                 } else {
                                //                     certus.alertas_sr(9, '', 'Por favor verifique su conexión a internet y vuelva a intentarlo', '');
                                //                 }

                                //             });

                                //         },
                                //         NO: function () {
                                //             //$mant_areas.load();
                                //         }
                                //     }
                                // });
                            }
                        }
                    ]
                }

            ]
        });
    });

    // $('#dt-configuracion tfoot th').each(function () {
    //     var title = $(this).text();
    //     $(this).html('<input type="text" class="form-control" placeholder="Buscar ' + title + '" />');
    // });
    var table = $('#dt-configuracion').DataTable();
    table.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        });
    });



    let tb_conf = $('#dt-configuracion').DataTable({
        // responsive: true,
        searching: true,
        //    //  'language':{
        //    //      'url':'{{asset("assets/$theme/vendor/jquery-datatables/datatablespanish.json")}}'
        //    //  },
        //  lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, 'Todos'] ],
        order: [[1, 'asc']],
        info: true,
        processing: true,
        serverSide: true,
        deferRender: true,
        destroy: true,
        autoWidth: true,
        ajax: {
            // 'method':'GET',
            url: $('#dt-configuracion').attr("action"),
            //'url':'conexion.php'
        },
        columns: [
            { data: 'id_rgt' },
            { data: 'id_empresa' },
            { data: 'id_establecimiento' },
            { data: 'd_establecimiento' },
            { data: 'id_punto_emsion' },
            { data: 'd_punto_emsion' },
            { data: 'secuencia' },
            { data: 'estado' },
            {
                "className": '',
                "orderable": false,
                "data": null,
                "defaultContent": '<div class="btn-group " role="group" data-toggle="tooltip" data-placement="top" title="" data-original-title=".btn-xlg"><button type="button" data-modal="modal-2" class="btn btn-info btn-mini waves-effect waves-light btn_editar"><i class="fa fa-edit"></i></button><button type="button" class="btn btn-danger  btn-mini waves-effect waves-light"><i class="fa fa-archive"></i></button></div>'
            }

        ]


    });

    let fila;

    $(document).on('click', '.btn_editar', function () {
        fila = $(this).closest('tr');
        id = parseInt(fila.find('td:eq(0)').text());
        id_empresa = parseInt(fila.find('td:eq(1)').text());
        id_establecimiento = parseInt(fila.find('td:eq(2)').text());
        d_establecimiento = fila.find('td:eq(3)').text();
        id_punto_emsion = fila.find('td:eq(4)').text();
        d_punto_emsion = fila.find('td:eq(5)').text();
        secuencia = parseInt(fila.find('td:eq(6)').text());
        estado = fila.find('td:eq(7)').text();

        $('#id_reg').val(id);
        $('#id_empresa').val(id_empresa);
        $("#cod_establecimiento").val(id_establecimiento);
        $("#establecimiento").val(d_establecimiento);
        $("#cod_put_ems").val(id_punto_emsion);
        $("#punto_emision").val(d_punto_emsion);
        $("#secuencia").val(secuencia);
        $("#select_estado").val(estado).change();;


        $('#modal-2').modal('show')
        // $('#modal-2').removeClass('md-hiden');
        $('#modal-2').addClass('md-show');


    });


    $('#btn-grb-configuracion-form').click(function (e) {
        e.preventDefault();

        let form_conf = $('#form-guadar-configuracion').serialize();
        let id_reg = $('#id_reg').val();
        let id_empr = $('#id_empresa').val();

        $.ajax({
            type: 'POST',
            url: $('#form-guadar-configuracion').attr("action"),
            data: form_conf + '&id_reg=' + id_reg + '&id_empr=' + id_empr,
            beforeSend: function () {
                $('.loaders').removeClass('d-none');
                $('#load-grb').removeClass('d-none');
                $('#btn-grb-configuracion-form').addClass('d-none');
            },
            success: function (response) {

                console.log(response);
                // let cod_validacion = response[0].out_cod;
                // let cod_reg_auto = response[0].ID_REG;
                // let msj = response[0].out_id_reg;
                // let ruc = response[0].p_in_ruc;

                // if(cod_validacion === 7){

                //       Swal.fire({
                //         icon: 'success',
                //         title: msj,
                //         text: 'RUC: '+ruc,
                //         showConfirmButton: false,
                //         allowOutsideClick: false,
                //         closeOnClickOutside: false,
                //         timer: 20000
                //         });
                //         setTimeout('document.location.reload()',5000);


                //      // location.reload();
                // }else{
                //     Swal.fire({
                //         icon: 'error',
                //         title: msj,
                //         text: 'RUC: '+ruc,

                //       });
                // }
            },
            // error:  function ( error ) {
            //     let errores = error.responseJSON.errors;

            //     if( errores.hasOwnProperty('email')){
            //         alert(errores['email'][0]);
            //     }else if ( errores.hasOwnProperty ('password')){
            //         alert(errores['password'][0]);
            //     }else{
            //         alert(errores['login'][0]);
            //     }
            // },
            complete: function () {
                $('.loaders').addClass('d-none');
                $('#load-grb').addClass('d-none');
                $('#btn-grb-configuracion-form').removeClass('d-none');
            }
        });
    });


    // $('#dt-configuracion').DataTable( {
    //     dom: "Bfrtip",
    //     ajax: "/configuracion_empresa_con",
    //     columns: [
    //         {data: 'id_empresa'},
    //         {data: 'id_establecimiento'},
    //         {data: 'd_establecimiento'},
    //          {data: 'id_punto_emsion'},
    //         {data: 'd_punto_emsion'},
    //         {data: 'secuencia'},
    //         {data: 'estado'}
    //     ],
    //     select: true,
    //     // buttons: [
    //     //     { extend: "create", editor: editor },
    //     //     { extend: "edit",   editor: editor },
    //     //     { extend: "remove", editor: editor }
    //     // ]
    // } );

    // var listar = function(){
    // $.noConflict();
    //var table =


    //  $('#dt-configuracion').DataTable({
    //      responsive: true,
    //      searching: true,
    //     //  'language':{
    //     //      'url':'{{asset("assets/$theme/vendor/jquery-datatables/datatablespanish.json")}}'
    //     //  },
    //      lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, 'Todos'] ],
    //      order: [[ 2, 'desc' ], [ 3, 'desc' ]],
    //      info: true,
    //      processing: true,
    //      serverSide: true,
    //      deferRender: true,
    //      destroy: true,
    //      autoWidth: true,
    //      ajax:{
    //          //'method':'GET',
    //          url: '{{url(/configuracion_empresa_con)}}',
    //          //'url':'conexion.php'
    //      },
    //      columns:[
    //          {data: 'id_empresa'},
    //          {data: 'id_establecimiento'},
    //          {data: 'd_establecimiento'},
    //          {data: 'id_punto_emsion'},
    //          {data: 'd_punto_emsion'},
    //          {data: 'secuencia'},
    //          {data: 'estado'},
    //      ]

    //  });



    //          setInterval( function () {
    //              table.ajax.reload();//reinicio de paginacion
    //     // table.ajax.reload(null, false);//cada 30 segundos (paginación retenida):
    //  }, 30000 );
    //      }//.ajax.reload();

    //listar();

});
