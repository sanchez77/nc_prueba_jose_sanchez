$(document).ready(function () {
    $('#tb-lista-clientes').each(function (i, el) {
        var $tb_list_empresas = new FancyGrid({
            selModel: 'row',
            emptyText: 'No tienes Personas',
            renderTo: 'tb-lista-clientes',
            theme: 'bootstrap',
            height: 'fit',
            //width: 'fit',
            paging: {
                cls: 'grid-busqueda-pagin'
            },
            lang: {
                loadingText: 'Cargando ...',
                paging: {
                    of: 'de [0]',
                    info: 'mostranto [0] - [1] de [2]',
                    page: 'pagina'
                }
            },
            title: {
                text: 'Lista de Personas',
                cls: 'my-title',
                tools: [
                    // {
                    //     type: 'button',
                    //     cls: 'btn-blue',
                    //     enableToggle: true,
                    //     text: '<a title="Cerrar"><i class="fa fa-window-close fa-lg" style="color:#6466b5; width:6; height:6;"></i></a>',
                    //     style: {
                    //         'width': 'auto',
                    //         'height': 'auto',
                    //         'padding': '13px 12px'
                    //     },
                    //     handler: function (grid, o) {

                    //         $tb_list_empresas.hide();

                    //     }
                    // }
                ]
            },
            defaults: {
                type: 'string',
                sortable: true
            },
            clicksToEdit: 1,
            data:
            {
                proxy: {
                    //read: 'GET',
                    url: $('#dt-list-clientes-get').attr("action")+'?opcion=AA&num_doc=1',
                    // extraParams: {
                    //     opcion: 'AA',
                    //     num_doc :''
                    // }
                }
                , reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            events: [],
            columns: [

                {
                    index: 'id_cliente',
                    title: 'Cod Cliente',
                    editable: true,
                    hidden: true,
                    width: 0,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'estado',
                    title: 'Estado',
                    editable: true,
                    hidden: false,
                    width: 70
                },{
                    index: 'id_tip_doc',
                    title: 'Cod Doc',
                    editable: true,
                    hidden: true,
                    width: 0
                }, {
                    index: 'num_doc',
                    title: 'Num Doc',
                    editable: false,
                    width:140,
                    hidden: false,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'nombres',
                    editable: false,
                    title: 'Nombres',
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'apellidos',
                    title: 'Apellidos',
                    editable: false,
                    hidden: false,
                    width: 120,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'razon_comercial',
                    title: 'Razon Comercial',
                    editable: false,
                    hidden: false,
                    width: 150,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'correo',
                    title: 'Correo',
                    editable: true,
                    hidden: false,
                    width: 200
                }, {
                    index: 'fecha_nacimiento',
                    title: 'Fech Nac',
                    editable: true,
                    hidden: false,
                    width: 100
                }, {
                    index: 'edad',
                    title: 'Edad',
                    editable: true,
                    hidden: false,
                    width: 50
                }, {
                    index: 'telefono',
                    title: 'Telf',
                    editable: true,
                    hidden: false,
                    width: 100
                }, {
                    index: 'celular',
                    title: 'Celular',
                    editable: true,
                    hidden: false,
                    width: 150
                },  {
                    index: 'direccion',
                    title: 'Direccion',
                    editable: true,
                    hidden: false,
                    width: 200
                }
            ]
        });
    });






    $('#tb-lista-productos').each(function (i, el) {
        var $tb_list_productos = new FancyGrid({

            selModel: 'row',
            emptyText: 'No tienes Productos',
            renderTo: 'tb-lista-productos',
            theme: 'bootstrap',
            height: 'fit',

            paging: {
                cls: 'grid-busqueda-pagin'
            },
            lang: {
                loadingText: 'Cargando ...',
                paging: {
                    of: 'de [0]',
                    info: 'mostranto [0] - [1] de [2]',
                    page: 'pagina'
                }
            },
            title: {
                text: 'Lista de Productos',
                cls: 'my-title',
                tools: [
                    // {
                    //     type: 'button',
                    //     cls: 'btn-blue',
                    //     enableToggle: true,
                    //     text: '<a title="Cerrar"><i class="fa fa-window-close fa-lg" style="color:#6466b5; width:6; height:6;"></i></a>',
                    //     style: {
                    //         'width': 'auto',
                    //         'height': 'auto',
                    //         'padding': '13px 12px'
                    //     },
                    //     handler: function (grid, o) {

                    //         $tb_list_productos.hide();

                    //     }
                    // }
                ]
            },
            defaults: {
                type: 'string',
                sortable: true
            },
            clicksToEdit: 1,
            data:
            {
                proxy: {
                    //read: 'GET',
                    url: $('#dt-list-productos-get').attr("action")+'?opcion=AA&cod_prod=1',
                    // extraParams: {
                    //     opcion: 'AA',
                    //     num_doc :''
                    // }
                }, reader: {
                    type: 'json',
                    root: 'data'
                }
            },



            events: [],
            columns: [

                {
                    index: 'id_product',
                    title: 'Cod Prodc',
                    editable: false,
                    hidden: true,
                    width: 0,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'cod_prod',
                    title: 'Cod Producto',
                    editable: false,
                    hidden: false,
                    width: 250
                }, {
                    index: 'descripcion',
                    title: 'Description',
                    editable: false,
                    width:300,
                    hidden: false,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'precio_unitario',
                    editable: false,
                    title: 'PVP Unit',
                    hidden: false,
                    width: 100,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'stock',
                    editable: false,
                    title: 'Stock',
                    hidden: false,
                    width: 150,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }, {
                    index: 'estado',
                    title: 'Estado',
                    editable: false,
                    hidden: false,
                    flex: 2,
                    filter: {
                        header: false,
                        headerNote: true
                    }
                }

            ]
        });
    });



});
