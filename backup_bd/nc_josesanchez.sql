/*
Navicat MySQL Data Transfer

Source Server         : laragonE
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : nc_josesanchez

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2020-08-12 11:06:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for clientes_nc
-- ----------------------------
DROP TABLE IF EXISTS `clientes_nc`;
CREATE TABLE `clientes_nc` (
  `id_cliente` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_tip_doc` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_doc` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombres` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `razon_comercial` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `edad` int(10) unsigned NOT NULL,
  `telefono` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` char(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `id_empresa` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for configuracion_nc
-- ----------------------------
DROP TABLE IF EXISTS `configuracion_nc`;
CREATE TABLE `configuracion_nc` (
  `id_rgt` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) unsigned NOT NULL,
  `id_establecimiento` int(10) unsigned NOT NULL,
  `d_establecimiento` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_punto_emsion` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `d_punto_emsion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secuencia` int(10) unsigned NOT NULL,
  `estado` char(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_rgt`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for empresas_nc
-- ----------------------------
DROP TABLE IF EXISTS `empresas_nc`;
CREATE TABLE `empresas_nc` (
  `id_rgt` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) unsigned NOT NULL,
  `razon_social` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `razon_comercial` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruc` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iva` int(11) NOT NULL,
  `estado` char(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_rgt`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for productos_nc
-- ----------------------------
DROP TABLE IF EXISTS `productos_nc`;
CREATE TABLE `productos_nc` (
  `id_product` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cod_prod` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock` int(10) unsigned NOT NULL,
  `precio_unitario` decimal(8,2) NOT NULL,
  `id_empresa` int(10) unsigned NOT NULL,
  `estado` char(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `roles_id` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Procedure structure for sp_con_cliente_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_con_cliente_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_con_cliente_nc`(IN p_in_opcion text, 
IN p_in_num_doc varchar(50))
BEGIN
case p_in_opcion
 when 'AA' THEN  
		SELECT 
			 id_cliente ,
			id_tip_doc ,
			num_doc ,
			nombres ,
			apellidos ,
			razon_comercial, 
			correo ,
			fecha_nacimiento , 
			edad ,
			telefono ,
			celular,
			direccion ,
			estado 
		FROM clientes_nc;
	
	 when 'AB' THEN  
		SELECT 
			 id_cliente ,
			id_tip_doc ,
			num_doc ,
			nombres ,
			apellidos ,
			razon_comercial, 
			correo ,
			fecha_nacimiento , 
			edad ,
			telefono ,
			celular,
			direccion ,
			estado 
		FROM clientes_nc
        where (num_doc LIKE concat('%',p_in_num_doc,'%')) 
        or (nombres LIKE concat('%',p_in_num_doc,'%')) 
        or (apellidos LIKE concat('%',p_in_num_doc,'%')) ;
       
        #'%'+p_in_num_doc+'%';
		#ORDER BY id_rgt DESC;
		#DESCRIBE EMPRESAS_NC
	   
	end case;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_con_configuracion_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_con_configuracion_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_con_configuracion_nc`(IN p_in_opcion VARCHAR(2), IN p_in_id_empresa INT)
BEGIN
IF p_in_opcion = 'AA' THEN
	SELECT id_rgt, id_empresa,id_establecimiento,d_establecimiento,id_punto_emsion,d_punto_emsion,secuencia,estado 
	FROM configuracion_nc ORDER BY id_rgt DESC;
	##WHERE p_in_id_empresa = id_empresa;

end if;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_con_empresas_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_con_empresas_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_con_empresas_nc`(IN p_in_opcion text, IN p_id_empresa int)
BEGIN
case p_in_opcion
	 when 'AA' THEN  
		SELECT 
			id_rgt,
			id_empresa,
			razon_social,
			razon_comercial,
			ruc,
			correo,
			direccion,
			iva,
			estado
		FROM EMPRESAS_NC
		ORDER BY id_rgt DESC;
		#DESCRIBE EMPRESAS_NC
	   
	end case;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_con_productos_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_con_productos_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_con_productos_nc`(
IN p_in_opcion text, 
IN p_in_cod_prod varchar(50))
BEGIN
case p_in_opcion
 when 'AA' THEN  
		SELECT 
			id_product ,
			cod_prod ,
			descripcion ,
			stock,
			precio_unitario,
			id_empresa ,
			estado
		FROM productos_nc;
	
	 when 'AB' THEN  
		SELECT 
			id_product ,
			cod_prod ,
			descripcion ,
			stock,
			precio_unitario,
			id_empresa ,
			estado
		FROM productos_nc
        where (cod_prod LIKE concat('%',p_in_cod_prod,'%'));
       
        #'%'+p_in_num_doc+'%';
		#ORDER BY id_rgt DESC;
		#DESCRIBE EMPRESAS_NC
	   
	end case;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trx_cliente_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trx_cliente_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trx_cliente_nc`(
						IN opcion text,
						IN p_in_id_cliente int,                        
						IN p_in_id_tip_doc VARCHAR(5),
						IN p_in_num_doc VARCHAR(50),
						IN p_in_nombres VARCHAR(255),
						IN p_in_apellidos VARCHAR(255),
						IN p_in_razon_comercial VARCHAR(100),
						IN p_in_correo VARCHAR(250),
						IN p_in_fecha_nacimiento VARCHAR(250),
						IN p_in_edad int,
						IN p_in_telefono VARCHAR(15),
						IN p_in_celular VARCHAR(15),
						IN p_in_direccion VARCHAR(100),
						IN p_in_estado char(10),
                        IN p_in_id_empresa int,
						OUT out_cod INT,
                        OUT out_msj VARCHAR(500),
						OUT out_id_cliente INT,
                        OUT out_num_doc VARCHAR(500))
BEGIN
declare _v_cod_cliente int;
case opcion  
  when 'AA' then 
	set _v_cod_cliente = (SELECT count(*) FROM clientes_nc where  num_doc = p_in_num_doc );
	IF _v_cod_cliente >= 1 THEN
			SET out_cod = 6;
			SET out_num_doc = p_in_num_doc;
			SET out_msj = 'YA EXISTE UN CLIENTE CON ESE NUMERO DE DOCUMENTO. POR FAVOR REVISE!';
			SET  out_id_cliente = (SELECT id_cliente FROM clientes_nc where  num_doc = p_in_num_doc);
SELECT out_id_cliente AS id_cliente, out_cod, out_msj, out_num_doc;
	ELSE 
		INSERT INTO clientes_nc(
								id_tip_doc, 
								num_doc,
								nombres,
								apellidos ,
								razon_comercial,
								correo,
								fecha_nacimiento, 
								edad,
								telefono,
								celular, 
								direccion, 
								estado,
								id_empresa, 
								created_at, 
								updated_at)
		VALUES(		       
							p_in_id_tip_doc,
							p_in_num_doc,
							p_in_nombres,
							p_in_apellidos,
							p_in_razon_comercial,
							p_in_correo,
							p_in_fecha_nacimiento,
							p_in_edad,
							p_in_telefono,
							p_in_celular,
							p_in_direccion,
							p_in_estado,
							p_in_id_empresa,
							now(),
							now());
							
				SET out_cod = 7;
				SET out_num_doc = p_in_num_doc;
				SET out_msj = 'SE HA CREADO CLIENTE CORRECTAMENTE';
				SELECT 
    LAST_INSERT_ID() AS id_cliente,
    out_cod,
    out_msj,
    out_num_doc;
END IF;
when 'AB' then
UPDATE clientes_nc set 
		id_tip_doc =	p_in_id_tip_doc,
		nombres =	p_in_nombres,
		apellidos =	p_in_apellidos,
		razon_comercial =	p_in_razon_comercial,
		correo =	p_in_correo,
		fecha_nacimiento =	p_in_fecha_nacimiento,
		edad =	p_in_edad,
		telefono =	p_in_telefono,
		celular =	p_in_celular,
		direccion =	p_in_direccion,
		estado =	p_in_estado
	where id_cliente = p_in_id_cliente and num_doc =	p_in_num_doc;
	 SET out_cod = 7;
				SET out_num_doc = p_in_num_doc;
				SET out_msj = 'SE HA MODIFICADO EL CLIENTE CORRECTAMENTE';
				SELECT 
    p_in_id_cliente as id_cliente,
    out_cod,
    out_msj,
    out_num_doc;
  
ELSE BEGIN END;  
end case;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trx_configuracion_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trx_configuracion_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trx_configuracion_nc`(
						IN opcion text,
	IN p_in_id_rgt int,
	IN p_id_empresa int,
	IN p_id_establecimiento int,
	IN p_d_establecimiento VARCHAR(100),
	IN p_id_punto_emsion VARCHAR(5),
	IN p_d_punto_emsion VARCHAR(100),
	IN p_in_secuencia int,
	IN p_estado char(10),
	OUT out_cod INT,
	OUT out_id_rgt INT,
	OUT out_msj VARCHAR(500),
	OUT out_id_establecimiento INT,
	OUT out_id_punto_emsion VARCHAR(50))
BEGIN
declare _v_rgt int;
case opcion  
  when 'AA' then 
	set _v_rgt = (SELECT count(*) FROM configuracion_nc where id_empresa = p_id_empresa and  id_establecimiento = p_id_establecimiento and  id_punto_emsion = p_id_punto_emsion);
	IF _v_rgt >= 1 THEN
			SET out_cod = 6;
			SET out_id_rgt = p_in_id_rgt;
			SET out_msj = 'YA EXISTE UN ESTABLECIMIENTO O PUNTO DE EMISION CON ESA NUMERACION. POR FAVOR REVISE!';
			SET  out_id_establecimiento = p_id_establecimiento;
			SET  out_id_punto_emsion = p_id_punto_emsion;
			SELECT 
    out_id_rgt AS ID_REG,
    out_cod,
    out_msj,
    out_id_establecimiento,
    out_id_punto_emsion;
		ELSE
			set _v_rgt = 0;
		END IF;
	
IF _v_rgt = 0 then

	
	INSERT INTO configuracion_nc(id_empresa,
												id_establecimiento,
												d_establecimiento,
												id_punto_emsion,
												d_punto_emsion,
												secuencia,
												estado,
												created_at,updated_at)
	VALUES(									p_id_empresa,
													p_id_establecimiento,
													p_d_establecimiento,
													p_id_punto_emsion,
													p_d_punto_emsion,
													p_in_secuencia,
													p_estado,
													now(),
													now());
	SET out_cod = 7;
	SET out_msj = 'SE HA CREADO LA CONFIGURACION DE EMPRESA CORRECTAMENTE';
	SET  out_id_establecimiento = p_id_establecimiento;
	SET  out_id_punto_emsion = p_id_punto_emsion;
	SELECT 
    LAST_INSERT_ID() AS ID_REG,
    out_cod,
    out_msj,
    out_id_establecimiento,
    out_id_punto_emsion;

END IF;



  when 'AB' then 
	UPDATE configuracion_nc set 
		secuencia= p_in_secuencia,
		estado = p_estado,
		updated_at = now()
	where id_rgt = p_in_id_rgt and id_empresa = p_id_empresa;
	
    SET out_cod = 7;
	SET out_msj = 'SE HA MODIFICADO LA CONFIGURACION DE EMPRESA CORRECTAMENTE';
	SET  out_id_establecimiento = p_id_establecimiento;
	SET  out_id_punto_emsion = p_id_punto_emsion;
SELECT 
    p_in_id_rgt AS ID_REG,
    out_cod,
    out_msj,
    out_id_establecimiento,
    out_id_punto_emsion;
ELSE BEGIN END;  
end case;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trx_empresas_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trx_empresas_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trx_empresas_nc`(
	IN `opcion` VARCHAR(2),
	IN `p_in_id_rgt` int,
	IN `p_in_id_empresa` int,
	IN `p_in_razon_social` VARCHAR(100),
	IN `p_in_razon_comercial` VARCHAR(100),
	IN `p_in_ruc` VARCHAR(13),
	IN `p_in_correo` VARCHAR(100),
	IN `p_in_direccion` VARCHAR(100),
	IN `p_in_iva` int,
	IN `p_in_estado` char(2)
,
	OUT `out_cod` INT,
	OUT `out_id_reg` VARCHAR(50)
)
BEGIN
declare v_id_reg varchar(20) ;
declare v_rgt int;
declare v_regt int;
declare _count_emp int;
declare _id_emp int;

IF opcion = 'AB' THEN
UPDATE empresas_nc set 

												correo =p_in_correo,
												direccion = p_in_direccion,
												iva = p_in_iva,
												estado = p_in_estado,
												updated_at = now()							
	where id_rgt=p_in_id_rgt and id_empresa = p_in_id_empresa;
	
	SET out_cod = 7;
	SET out_id_reg = 'SE HA MOFICADO LA EMPRESA CORRECTAMENTE';
	
	SELECT p_in_id_rgt AS ID_REG, out_cod, out_id_reg, p_in_ruc;
	

END IF;

set v_rgt = (SELECT count(*) FROM empresas_nc where id_empresa = p_in_id_empresa and ruc = p_in_ruc);

IF v_rgt >= 1 THEN
	SET out_cod = 6;
	SET out_id_reg = 'EMPRESA YA EXITE';
	SET  v_regt = (SELECT id_rgt FROM empresas_nc where id_empresa = p_in_id_empresa and ruc = p_in_ruc);
	
	SELECT v_regt AS ID_REG, out_cod, out_id_reg, p_in_ruc;

	
ELSE
	set v_rgt = 0;
END IF;

IF opcion = 'AA' AND v_rgt = 0 then
	
    set _count_emp = ( SELECT count(*) FROM empresas_nc ) ;
    IF( _count_emp = 0) then
		set _id_emp = 1;
    else
		set _id_emp = (SELECT (MAX(id_empresa)+1) as id_empresa FROM empresas_nc order by id_rgt desc limit 1);
	END IF;
	INSERT INTO empresas_nc(id_empresa,
												razon_social,
												razon_comercial,
												ruc,
												correo,
												direccion,
												iva,
												estado,
												created_at,updated_at)
	VALUES(									_id_emp,#p_in_id_empresa,
													p_in_razon_social,
													p_in_razon_comercial,
													p_in_ruc,
													p_in_correo,
													p_in_direccion,
													p_in_iva,
													p_in_estado,
													now(),
													now());
													
													
													
	
	
	
	SET out_cod = 7;
	SET out_id_reg = 'SE HA CREADO LA EMPRESA CORRECTAMENTE';
	
	SELECT LAST_INSERT_ID() AS ID_REG, out_cod, out_id_reg, p_in_ruc;
	

ELSE
	SELECT LAST_INSERT_ID() AS ID_REG;
END IF;



END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trx_productos_nc
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trx_productos_nc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trx_productos_nc`(
						IN opcion text,
						IN p_in_id_product int,                        
						IN p_in_cod_prod VARCHAR(255),
                        IN p_in_descripcion VARCHAR(255),
                        IN p_in_stock  int, 
                        IN p_in_precio_unitario  decimal(8,2), 
                        IN p_in_id_empresa  int,
						IN p_in_estado char(10),
                   
						OUT out_cod INT,
                        OUT out_msj VARCHAR(500),
						OUT out_id_aut INT,
                        OUT out_cod_proct VARCHAR(500))
BEGIN
declare _v_cod_prod int;
case opcion  
  when 'AA' then 
	set _v_cod_prod = (SELECT count(*) FROM productos_nc where  cod_prod = p_in_cod_prod );
	IF _v_cod_prod >= 1 THEN
			SET out_cod = 6;
			SET out_cod_proct = p_in_cod_prod;
			SET out_msj = 'YA EXISTE CODIGO PRODUCTO. POR FAVOR REVISE!';
			SET  out_id_aut = (SELECT id_product FROM productos_nc where  cod_prod = p_in_cod_prod);
SELECT 
    out_id_aut,
    out_cod,
    out_msj,
    out_cod_proct;
	ELSE 
		INSERT INTO productos_nc(
								cod_prod, 
                                descripcion,
                                stock,
                                precio_unitario,
                                id_empresa,
                                estado,
								created_at, 
								updated_at)
		VALUES(		       
							p_in_cod_prod,
                            p_in_descripcion,
                            p_in_stock,
                            p_in_precio_unitario,
                            p_in_id_empresa,
                            p_in_estado,
							now(),
							now());
							
				SET out_cod = 7;
				SET out_cod_proct = p_in_cod_prod;
				SET out_msj = 'SE HA CREADO PRODUCTO CORRECTAMENTE';
				SELECT 
    LAST_INSERT_ID() AS out_id_aut,
    out_cod,
    out_msj,
    out_cod_proct;
END IF;
when 'AB' then
UPDATE productos_nc 
SET 
    descripcion = p_in_descripcion,
    stock = p_in_stock,
    precio_unitario = p_in_precio_unitario,
    id_empresa = p_in_id_empresa,
    estado = p_in_estado,
    updated_at = NOW()
WHERE
    cod_prod = p_in_cod_prod;
				SET out_cod = 7;
				SET out_cod_proct = p_in_cod_prod;				
                SET out_msj = 'SE HA MODIFICADO PRODUCTO CORRECTAMENTE';
SELECT 
    p_in_id_product as out_id_aut,
    out_cod,
    out_msj,
    out_cod_proct;
  
ELSE BEGIN END;  
end case;

END
;;
DELIMITER ;
