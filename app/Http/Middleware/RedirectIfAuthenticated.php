<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // if (Auth::guard($guard)->check()) {
        //     return redirect(RouteServiceProvider::HOME);
        // }
        //    dd(Auth::logout());//null
        //si existe sesion
        if( Auth::check()){

            $rol = Auth::user()->roles_id;
           // dd($rol);
            if( $rol == 1){
                //Panel Administrativo
                return redirect()->route('PanelAdministrativo');
            }else if($rol == 2){
                //Pagina de usuario estandar
                return redirect()->route('PanelEstandar');
            }

        }






        return $next($request);
    }
}
