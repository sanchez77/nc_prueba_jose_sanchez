<?php

namespace App\Http\Controllers\Generales;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Events\StatementPrepared;
use App\Models\Cliente;
use PDO;



use Doctrine\DBAL\Driver\PDOConnection;


class ClienteController extends Controller
{
    public function indexCliente()
    {
        return view('Generales.crearCliente');
    }


    public function trx_cliente(Request $request)
    { //Valdacionde formualrio [FormRequest]
        $opcion = $request->opcion;
        $p_in_id_cliente = $request->id_cliente;
        $p_in_id_tip_doc = $request->id_tip_doc;
        $p_in_num_doc = $request->num_doc;
        $p_in_nombres = $request->nombres;
        $p_in_apellidos = $request->apellidos;
        $p_in_razon_comercial = $request->razon_comercial;
        $p_in_correo = $request->correo;
        $p_in_fecha_nacimiento = $request->fecha_nacimiento;
        $p_in_edad = $request->edad;
        $p_in_telefono = $request->telefono;
        $p_in_celular = $request->celular;
        $p_in_direccion = $request->direccion;
        $p_in_estado = $request->estado;
        $p_in_id_empresa = 0;




        $p_in_id_cliente = empty(($p_in_id_cliente)) ? "0" : $p_in_id_cliente;
        $p_in_id_tip_doc = empty(($p_in_id_tip_doc)) ? "NULL" : $p_in_id_tip_doc;
        $p_in_num_doc = empty(($p_in_num_doc)) ? "NULL" : $p_in_num_doc;
        $p_in_nombres = empty(($p_in_nombres)) ? "NULL" : $p_in_nombres;
        $p_in_apellidos = empty(($p_in_apellidos)) ? "NULL" : $p_in_apellidos;
        $p_in_razon_comercial = empty(($p_in_razon_comercial)) ? "NULL" : $p_in_razon_comercial;
        $p_in_correo = empty(($p_in_correo)) ? "NULL" : $p_in_correo;
        $p_in_fecha_nacimiento = empty(($p_in_fecha_nacimiento)) ? "1999-01-01" : $p_in_fecha_nacimiento;
        $p_in_edad = empty(($p_in_edad)) ? "0" : $p_in_edad;
        $p_in_telefono = empty(($p_in_telefono)) ? "NULL" : $p_in_telefono;
        $p_in_celular = empty(($p_in_celular)) ? "NULL" : $p_in_celular;
        $p_in_direccion = empty(($p_in_direccion)) ? "NULL" : $p_in_direccion;
        $p_in_estado = empty(($p_in_estado)) ? "NULL" : $p_in_estado;

        $data = DB::select(
            'call sp_trx_cliente_nc(?,?,?,?,?,  ?,?,?,?,?,  ?,?,?,?,?,  ?,?,?,?)',
            array($opcion, $p_in_id_cliente, $p_in_id_tip_doc, $p_in_num_doc, $p_in_nombres,  $p_in_apellidos, $p_in_razon_comercial, $p_in_correo, $p_in_fecha_nacimiento, $p_in_edad, $p_in_telefono, $p_in_celular, $p_in_direccion,  $p_in_estado, $p_in_id_empresa, "@val", "@val2", "@val3", "@val4")
        );
        return response()->json($data, 200);
    }

    public function buscarCliente(Request $request)
    { //Valdacionde formualrio [FormRequest]
        $opcion = $request->opcion;
        $p_in_opcion = $request->opcion;
        $p_in_num_doc = $request->num_doc;


        $data = DB::select(
            'call sp_con_cliente_nc(?,?)',
            array($p_in_opcion, $p_in_num_doc)
        );
        if (empty($data)) {
            $data = 0;
        } else {
            $data = $data;
        }
        if ($p_in_opcion == "AA" && $p_in_num_doc == 1) {
            return response()->json(['data' => $data], 200);
        } else {
            return response()->json($data, 200);
        }
    }

    public function ListaCliente()
    {
        return view('Generales.listaClientes');
    }




    public function buscarCliente_modal(Request $request)
    { //Valdacionde formualrio [FormRequest]

        $p_in_opcion = "AC";
        $p_in_dato_busqueda = $request->dato_busq;


        $p_in_dato_busqueda = empty(($p_in_dato_busqueda)) ? "0" : $p_in_dato_busqueda;
        //  $parametros = array($p_in_opcion, $p_in_dato_busqueda);
        // $query = 'call sp_con_cliente_nc_busq( "' . $p_in_opcion . '" ,  "' . $p_in_dato_busqueda . '"  )';
        // $query = $query+","+$parametros;

        $data = DB::select(
            'call sp_con_cliente_nc_busq(?,?)',
            array($p_in_opcion, $p_in_dato_busqueda)
        );



        empty($data) ? 0 : $data;

        return response()->json($data, 200);
        // if(empty($data)){
        //     $data = 0;
        // }else{
        //     $data=$data;
        // }
        // if($p_in_opcion == "AA" && $p_in_num_doc == 1){
        //     return response()->json(['data' => $data], 200);
        // }else{
        //     return response()->json($data, 200);

        // }

    }
}
