<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VistaEstandarController extends Controller
{
    //
    public function index(){
        Auth::logout();
        return 'vistadel usuario estandar';
    }
}
