<?php

namespace App\Http\Controllers\Sesion;

use App\Http\Controllers\Controller;
use App\Http\Requests\Login\IniciarSesionFormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use  Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Artisan;


class IniciarSesionController extends Controller
{
    //
    public function iniciar_sesion( IniciarSesionFormRequest $request){//Valdacionde formualrio [FormRequest]

        $credentials = ['username' => $request->email,'password' => $request->password];

        $remember = false;
        //valide si existe el usuario
         if( Auth::attempt($credentials, $remember)){
             //return "ok";
             return response()->json("Has iniciado sesion", 200);
             //return response()->json($request->all(), 200);
        }else{
            return response()->json( [ 'errors' => ['login' => ['El usuarios ingresado no se encuentra registrado']]], 422);
         }


        //si existe que inice sesion

    }



    public function Cerrar_Logout(Request $request)
    {
        //Desconctamos al usuario
        Auth::logout();
        Artisan::call('cache:clear');
        $request->session()->regenerate(true); 
        $request->session()->flush(); 

        Session::flush();

        //Redireccionamos al inicio de la app con un mensaje
        return redirect()->route('PanelAdministrativo');
       
    }
}
