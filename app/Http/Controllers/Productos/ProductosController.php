<?php

namespace App\Http\Controllers\Productos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Empresa_NC;
use Illuminate\Support\Facades\DB;

class ProductosController extends Controller
{
    //

    public function indexProducto()
    {
        $empresas = Empresa_NC::all();
        return view('Productos.crearProducto', compact("empresas"));
    }



    public function trx_producto(Request $request)
    { //Valdacionde formualrio [FormRequest]
        $opcion = $request->opcion;
        $p_in_id_product = $request->id_product;
        $p_in_cod_prod = $request->cod_prod;
        $p_in_descripcion = $request->descripcion;
        $p_in_stock = $request->stock;
        $p_in_precio_unitario = $request->precio_unitario;
        $p_in_id_empresa = $request->id_empresa;
        $p_in_estado = $request->estado;


        $p_in_id_product = empty(($p_in_id_product)) ? "0" : $p_in_id_product;
        $p_in_cod_prod = empty(($p_in_cod_prod)) ? "NULL" : $p_in_cod_prod;
        $p_in_descripcion = empty(($p_in_descripcion)) ? "NULL" : $p_in_descripcion;
        $p_in_stock = empty(($p_in_stock)) ? "0" : $p_in_stock;
        $p_in_precio_unitario = empty(($p_in_precio_unitario)) ? "0" : $p_in_precio_unitario;
        $p_in_id_empresa = empty(($p_in_id_empresa)) ? "0" : $p_in_id_empresa;
        $p_in_estado = empty(($p_in_estado)) ? "NULL" : $p_in_estado;

        $data = DB::select(
            'call sp_trx_productos_nc(?,?,?,?,?,  ?,?,?,?,?,  ?,?)',
            array($opcion, $p_in_id_product,  $p_in_cod_prod, $p_in_descripcion, $p_in_stock, $p_in_precio_unitario, $p_in_id_empresa, $p_in_estado,   "@val", "@val2", "@val3", "@val4")
        );
        return response()->json($data, 200);
    }






    public function buscarProducto(Request $request)
    { //Valdacionde formualrio [FormRequest]

        $p_in_opcion = $request->opcion;
        $p_in_cod_prod = $request->cod_prod;


        $data = DB::select(
            'call sp_con_productos_nc(?,?)',
            array($p_in_opcion, $p_in_cod_prod)
        );
        if(empty($data)){
            $data = 0;
        }else{
            $data=$data;
        }
        if($p_in_opcion == "AA" && $p_in_cod_prod == 1){
            return response()->json(['data' => $data], 200);
        }else{
            return response()->json($data, 200);

        }


    }


    public function ListaProductos()
    {
        return view('Productos.listaProductos');
    }

}
