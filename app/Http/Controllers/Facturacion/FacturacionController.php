<?php

namespace App\Http\Controllers\Facturacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FacturacionController extends Controller
{
    public function indexFactura()
    {

        return view('Facturacion.crearFactura');
    }


    public function buscarEmpresa(Request $request)
    {

        $p_in_opcion = $request->opcion;
        $p_in_num_doc = $request->num_doc;


        $data = DB::select('call sp_con_empresas_nc(?,?)',  array($p_in_opcion, $p_in_num_doc));


        //return response()->json(['data' => $data], 200);
        return response()->json($data, 200);
    }

    public function buscarPuntoEmision(Request $request)
    {

        $p_in_opcion = $request->opcion;
        $p_in_cod_empresa = $request->id_cod_empresa;
        $p_in_otro = $request->otro;


        $data = DB::select('call sp_con_puntos_emision_establecimineto_nc(?,?,?)',  array($p_in_opcion, $p_in_cod_empresa, $p_in_otro));

        if (empty($data)) {
            $data = 0;
        } else {
            $data = $data;
        }
        //return response()->json(['data' => $data], 200);
        return response()->json($data, 200);
    }

    public function trx_factura(Request $request)
    { //Valdacionde formualrio [FormRequest]


        $opcion = $request->opcion;
        $p_in_id_fact = $request->id_fact;
        $p_in_id_empresa = $request->id_empresa;
        $p_in_tipo_doc = $request->tipo_doc;
        $p_in_id_rgt_conf = $request->id_rgt_conf;
        $p_in_id_establecimiento = $request->id_establecimiento;
        $p_in_id_punto_emsion = $request->id_punto_emsion;
        $p_in_fecha_emision  = $request->fecha_emision;
        $p_in_secuencia = $request->secuencia;
        $p_in_id_cliente = $request->id_cliente;
        $p_in_comentario = $request->comentario;
        $p_in_forma_de_pago = $request->forma_de_pago;
        $p_in_subtotal = $request->subtotal;
        $p_in_iva = $request->iva;
        $p_in_descuento = $request->descuento;
        $p_in_total = $request->total;


        $p_in_id_fact  = empty(($p_in_id_fact)) ? "0" : $p_in_id_fact;
        $p_in_id_empresa = empty(($p_in_id_empresa)) ? "0" : $p_in_id_empresa;
        $p_in_tipo_doc = empty(($p_in_tipo_doc)) ? "0" : $p_in_tipo_doc;
        $p_in_id_rgt_conf = empty(($p_in_id_rgt_conf)) ? "0" : $p_in_id_rgt_conf;
        $p_in_id_establecimiento = empty(($p_in_id_establecimiento)) ? "0" : $p_in_id_establecimiento;
        $p_in_id_punto_emsion = empty(($p_in_id_punto_emsion)) ? "0" : $p_in_id_punto_emsion;
        $p_in_fecha_emision = empty(($p_in_fecha_emision)) ? "1999-01-01" : $p_in_fecha_emision;
        $p_in_secuencia = empty(($p_in_secuencia)) ? "0" : $p_in_secuencia;
        $p_in_id_cliente = empty(($p_in_id_cliente)) ? "0" : $p_in_id_cliente;
        $p_in_comentario = empty(($p_in_comentario)) ? "0" : $p_in_comentario;
        $p_in_forma_de_pago = empty(($p_in_forma_de_pago)) ? "0" : $p_in_forma_de_pago;
        $p_in_subtotal = empty(($p_in_subtotal)) ? "0" : $p_in_subtotal;
        $p_in_iva = empty(($p_in_iva)) ? "0" : $p_in_iva;
        $p_in_descuento = empty(($p_in_descuento)) ? "0" : $p_in_descuento;
        $p_in_total =  empty(($p_in_total)) ? "0" : $p_in_total;




        $data = DB::select(
            'call sp_trx_factura_nc(?,?,?,?,?,  ?,?,?,?,?,   ?,?,?,?,?,  ?,?,?,?,?)',
            array(
                $opcion,
                $p_in_id_fact,
                $p_in_id_empresa,
                $p_in_tipo_doc,
                $p_in_id_rgt_conf,
                $p_in_id_establecimiento,
                $p_in_id_punto_emsion,
                $p_in_fecha_emision,
                $p_in_secuencia,
                $p_in_id_cliente,
                $p_in_comentario,
                $p_in_forma_de_pago,
                $p_in_subtotal,
                $p_in_iva,
                $p_in_descuento,
                $p_in_total,
                "@val", "@val2", "@val3", "@val4"
            )
        );


        // let cod_validacion = response[0].out_cod;
        // let cod_reg_auto = response[0].out_id_fact;
        // let msj = response[0].out_msj;
        // let cod_proct = response[0].out_secuencia;

        // $cab_rpt = json_decode($data, false);
        $cod_confir = "";
        $id_fact = "";
        //$p_cod =  $data[0].["out_cod"];
        foreach ($data as $key => $object) {
            $cod_confir = $object->out_cod;
            $id_fact = $object->out_id_fact;
        }
        //return  view('Facturacion.crearFactura', dd($prueba));

        $p_in_tabla_detalle = $request->tabla_detalle;
        $json_tb_detalle_fact = json_decode($p_in_tabla_detalle, true);

        if ($cod_confir == "7") {
            foreach ($json_tb_detalle_fact as $value) {

                $p_in_id_det_fact = 0;
                // $p_in_id_empresa
                $p_in_id_product =  $value['id_product'];
                $p_in_cod_prod  =  $value['cod_prod'];
                $p_in_cantidad  =  $value['cantidad'];
                $p_in_precio_unitario =  $value['precio_unitario'];
                $p_in_subtotal_prod =  $value['subtotal_prod'];
                $p_in_desct_prod_porcent =  $value['desct_prod_porcent'];
                $p_in_desct_prod_valor =  $value['desct_prod_valor'];
                $p_in_total_prod =  $value['total_prod'];
                // $p_in_tipo_doc
                // $p_in_fecha_emision
                // $p_in_id_fact
                // $p_in_id_establecimiento
                //  $p_in_id_punto_emsion


                $data2 = DB::select(
                    'call sp_trx_factura_detalle_nc(?,?,?,?,?,  ?,?,?,?,?,   ?,?,?,?,?,  ?,?,?,?,?,  ?)',
                    array(
                        $opcion,
                        $p_in_id_det_fact,
                        $p_in_id_empresa,
                        $p_in_id_product,
                        $p_in_cod_prod,
                        $p_in_cantidad,
                        $p_in_precio_unitario,
                        $p_in_subtotal_prod,
                        $p_in_desct_prod_porcent,
                        $p_in_desct_prod_valor,
                        $p_in_total_prod,
                        $p_in_tipo_doc,
                        $p_in_fecha_emision,
                        $id_fact, // $p_in_id_fact, auto
                        $p_in_secuencia,
                        $p_in_id_establecimiento,
                        $p_in_id_punto_emsion,
                        "@val", "@val2", "@val3", "@val4"
                    )
                );
            }
        }

        return response()->json($data2, 200);
    }


    public function consultarFacturaView()
    {

        return view('Facturacion.revisarFactura');
    }

    public function consultarFacturaGet(Request $request)
    {

        $p_in_opcion = $request->opcion;
        $p_in_fecha_desde = $request->fecha_desde;
        $p_in_fecha_hasta = $request->fecha_hasta;
        $p_in_secuencia = $request->secuencia;
        $p_in_dato_cliente = $request->dato_cliente;

        $data = DB::select('call sp_con_buscar_factura(?,?,?,?,?)',  array($p_in_opcion, $p_in_fecha_desde,  $p_in_fecha_hasta,  $p_in_secuencia,  $p_in_dato_cliente));
        if (empty($data)) {
            $data = 0;
        } else {
            $data =  $data ;
        }

        //return response()->json(['data' => $data], 200);
        return response()->json($data, 200);
    }



    public function consultarFacturaDetalle(Request $request)
    {

        $p_in_opcion = $request->opcion;
        $p_in_id_factura = $request->id_factura;
        $p_in_secuencia = $request->secuencia;
        $p_in_id_establecimiento = $request->id_establecimiento;
        $p_in_id_punto_emsion = $request->id_punto_emsion;


        $data = DB::select('call sp_con_buscar_factura_detalle(?,?,?,?,?)',  array($p_in_opcion, $p_in_id_factura,  $p_in_secuencia,  $p_in_id_establecimiento,  $p_in_id_punto_emsion));
        if (empty($data)) {
            $data = 0;
        } else {
            $data =  $data ;
        }

        //return response()->json(['data' => $data], 200);
        return response()->json($data, 200);
    }
}
