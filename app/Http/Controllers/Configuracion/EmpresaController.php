<?php

namespace App\Http\Controllers\Configuracion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Empresa_NC;
use App\Http\Controllers\Configuracion\View;

class EmpresaController extends Controller
{
    public function indexEmpresa()
    {
        return view('Configuracion.ver_empresas');
    }

    public function datatableEmpresasJson()
    {
        $data = DB::select('call sp_con_empresas_nc(?,?)', array('AA', 0));
        return response()->json(['data' => $data], 200);
    }


    public function crearEmpresa()
    {
        return view('Configuracion.crear');
    }


    public function graba_empresa(Request $request)
    { //Valdacionde formualrio [FormRequest]



        $p_in_id_empresa = 1;
        $p_in_direccion = $request->inp_direccion;
        $p_in_email = $request->inp_email;
        $p_in_iva = $request->inp_iva;
        $p_in_razon_comercial = $request->inp_razon_comercial;
        $p_in_razon_social = $request->inp_razon_social;
        $p_in_ruc = $request->inp_ruc;
        $p_in_est = "A";
        //call sp_trx_empresas_nc(AA,0,1,j,y, 3,sanchezbaquejoseluis@gmail.com,A,,,  ?,? ))
        //CALL sp_trx_empresas_nc('AA',0,1,'ra','co','123','j@gmail.com','dire',12,'A',@val,@VL)
        $data = DB::select(
            'call sp_trx_empresas_nc(?,?,?,?,?, ?,?,?,?,?,  ?,? )',
            array('AA', 0, $p_in_id_empresa,  $p_in_razon_social, $p_in_razon_comercial, $p_in_ruc, $p_in_email, $p_in_direccion, $p_in_iva, $p_in_est,  "@val", "@val2")
        );




        return response()->json($data, 200);

        // $credentials = ['username' => $request->email,'password' => $request->password];

        // $remember = false;
        // //valide si existe el usuario
        //  if( Auth::attempt($credentials, $remember)){
        //      //return "ok";
        //      return response()->json("Has iniciado sesion", 200);
        //      //return response()->json($request->all(), 200);
        // }else{
        //     return response()->json( [ 'errors' => ['login' => ['El usuarios ingresado no se encuentra registrado']]], 422);
        //  }


        //si existe que inice sesion

    }

    public function modificar_empresa(Request $request)
    { //Valdacionde formualrio [FormRequest]



        $p_in_id_empresa = $request->p_in_id_empresa;
        $p_in_id_rgt = $request->p_in_id_rgt;
        $opcion = $request->opcion;
        $p_in_direccion = $request->inp_direccion;
        $p_in_email = $request->inp_email;
        $p_in_iva = $request->inp_iva;
        $p_in_razon_comercial = $request->inp_razon_comercial;
        $p_in_razon_social = $request->inp_razon_social;
        $p_in_ruc = $request->inp_ruc;
        $p_in_est = $request->select_estado;
        $data = DB::select(
            'call sp_trx_empresas_nc(?,?,?,?,?, ?,?,?,?,?,  ?,? )',
            array($opcion, $p_in_id_rgt, $p_in_id_empresa,  $p_in_razon_social, $p_in_razon_comercial, $p_in_ruc, $p_in_email, $p_in_direccion, $p_in_iva, $p_in_est,  "@val", "@val2")
        );
        return response()->json($data, 200);
    }




    public function configuracionEmpresa()
    {
        return view('Configuracion.configuracion');
    }

    public function datatableConfiguracion(Request $request)
    {

        ///$temperatura = Temperatura::orderBy('fecha','DESC')->orderBy('hora','DESC')->get();
        //$temperatura = Temperatura::select(['temperatura','humedad','fecha','hora']);
        // $temperatura = \DB::table('temperaturas')->get();
        //dd($temperatura);
        //return view ('arduino.temperatura',compact('temperatura'));
        //$temperatura = Temperatura::select(['temperatura','humedad','fecha','hora']);

        //$temperatura = \DB::table('temperaturas')->select(['temperatura','humedad','fecha','hora']);

        
        $data = DB::select('call sp_con_configuracion_nc(?,?)', array('AA', 0));

        return response()->json(['data' => $data], 200);
        // return response()->json($data, 200);



    }


    public function graba_configuracion(Request $request)
    { //Valdacionde formualrio [FormRequest]

        $p_in_id_empresa = $request->id_empr;
        $p_in_id_rgt = $request->id_reg;
        $p_in_id_establecimiento = $request->inp_cod_establecimiento;
        $p_in_d_establecimiento = $request->inp_establecimiento;
        $p_in_id_punto_emsion = $request->inp_cod_put_ems;
        $p_in_d_punto_emsion = $request->inp_punto_emision;
        $p_in_secuencia = $request->inp_secuencia;
        $p_in_estado = $request->select_estado;
        $p_in_opcion = $request->opcion;

        $data = DB::select(
            'call sp_trx_configuracion_nc(?,?,?,?,?,?,?,?,?,?,  ?,?,?,? )',
            array(
                $p_in_opcion,
                $p_in_id_rgt,
                $p_in_id_empresa,
                $p_in_id_establecimiento,
                $p_in_d_establecimiento,
                $p_in_id_punto_emsion,
                $p_in_d_punto_emsion,
                $p_in_secuencia,
                $p_in_estado,
                "@val", "@val2", "@val3", "@val4", "@val5"
            )
        );

        // $data = DB::select('exec  sp_trx_configuracion_nc('AB',2,1,2,'002', 'SUR','NORTE',3,'A',@val,  @val2,@val3,@val4,@val5 )');
        return response()->json($data, 200);
    }



    // public function comboEmpresas()
    // {
    //     $empresas = Empresa_NC::all();
    //     return view('Productos.crearProducto', compact("empresas"));
    // }
}
