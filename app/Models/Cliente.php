<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes_nc';

    private $_opcion;
    private $_dato_busqueda;



    /**
     * Get the value of _opcion
     */
    public function get_opcion()
    {
        return $this->_opcion;
    }

    /**
     * Set the value of _opcion
     *
     * @return  self
     */
    public function set_opcion($_opcion)
    {
        $this->_opcion = $_opcion;

        return $this;
    }

    /**
     * Get the value of _dato_busqueda
     */ 
    public function get_dato_busqueda()
    {
        return $this->_dato_busqueda;
    }

    /**
     * Set the value of _dato_busqueda
     *
     * @return  self
     */
    public function set_dato_busqueda($_dato_busqueda)
    {
        $this->_dato_busqueda = $_dato_busqueda;

        return $this;
    }
}
