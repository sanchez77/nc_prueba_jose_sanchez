<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class Usuario_Md  extends Authenticatable
{
    //$usuario = App\Models\Usuario_Md::find(1)

    use Notifiable;

    protected $table = 'usuarios';

    protected $hidden = [
        'password', 'remember_token'
    ];

}
