<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empresa_NC extends Model
{
    protected $table = 'empresas_nc';
    protected $fillable = [
        'razon_social', 'razon_comercial','ruc','correo','direccion','iva','estado'
    ];

}
